package com.ws.accounting;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelReader;
import com.alibaba.excel.read.metadata.ReadSheet;
import com.ws.accounting.entity.*;
import com.ws.accounting.repository.*;
import com.ws.accounting.sever.AccAccDetailServer;
import com.ws.accounting.utils.*;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Comparator;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;

@SpringBootTest
class AccountingApplicationTests {

    @Test
    void contextLoads() {
    }
    private static Logger logger = LoggerFactory.getLogger(AccountingApplicationTests.class);
    @Autowired
    private SubjectInfoRepository repository;

    @Autowired
    private BusinessCodeRepository codeRepository;

    @Autowired
    private AccAccDetailServer accAccDetailServer;

    @Autowired
    private AcTrnaccBookRepository acTrnaccBookRepository;

    @Autowired
    private ACFM16Repository acfm16Repository;

    @Autowired
    private ACFB06Repository acfb06Repository;

    @Autowired
    private ACFD05Repository acfd05Repository;

    @Autowired
    private ACFE17Repository acfe17Repository;

    @Autowired
    private ACFM15Repository acfm15Repository;

    @Autowired
    private ACFM08Repository acfm08Repository;

    @Autowired
    private ACFB07Repository acfb07Repository;

    @Autowired
    private ACFE20Repository acfe20Repository;

    @Test
    public void read(){
        String fileName = "/Users/xiaocao/Downloads/subject.xlsx";
        ExcelReader excelReader = null;
        try {
            excelReader = EasyExcel.read(fileName, SubjectInfo.class, new SubjectImportUtils(repository)).build();
            ReadSheet readSheet = EasyExcel.readSheet(0).build();
            excelReader.read(readSheet);
        } finally {
            if (excelReader != null) {
                // 这里千万别忘记关闭，读的时候会创建临时文件，到时磁盘会崩的
                excelReader.finish();
            }
        }
    }


    @Test
    public void read2(){
        String fileName = "/Users/xiaocao/Downloads/ywdh.xls";
        ExcelReader excelReader = null;
        try {
            excelReader = EasyExcel.read(fileName, BusinessCode.class, new BusinessImportUtils(codeRepository)).build();
            ReadSheet readSheet = EasyExcel.readSheet(0).build();
            excelReader.read(readSheet);
        } finally {
            if (excelReader != null) {
                // 这里千万别忘记关闭，读的时候会创建临时文件，到时磁盘会崩的
                excelReader.finish();
            }
        }
    }

    @Test
    public void chuanpiao(){
        AcTrnaccBook acTrnaccBook = acTrnaccBookRepository.findById("402880e4773e929301773eb324f1004b").get();
        System.out.println(acTrnaccBook.getPrdcode());
        System.out.println(acTrnaccBook.getSerseqn());
        accAccDetailServer.analysis(acTrnaccBook);
    }

    @Test
    public void jiexi(){

        ACFM15 acfm15 = acfm15Repository.findById("402880e677d3cf5f0177d75d1a800000").get();
        List<ACFM16> acfm16List = (List<ACFM16>) acfm16Repository.findAll();
        //循环解析子流水
        // 传票序号
        int bookseq = 0;
        for (ACFM16 acfe16 : acfm16List
        ) {
            //核算产品号
            String accprdno = "";

            // 根据交易时间码查询规则表
            List<ACFB06> acfb06List = acfb06Repository.findAllByTranevntno(acfe16.getTranevntno());
            //循环解析规则，单笔子流水拆解传票


            for (ACFB06 acfb06 : acfb06List
            ) {
                //外部产品码映射至核算产品码
                if (StringUtils.equals(acfb06.getSubsrctyp(), AccConstant.SUB_NO_SOURCE_1)) {

                    ACFD05 acfd05 = acfd05Repository.findByExtprdno(acfe16.getPrdno());
                    if (acfd05 == null) {
                        //如果映射表中不存在映射关系 则默认核算产品码等于外部产品码
                        accprdno = acfe16.getPrdno();

                    } else {
                        //如果映射表存在，则赋值核算产品码
                        accprdno = acfd05.getAccprdno();
                    }
                }



                //根据规则表中金额表达式判断上送报文中金额是否有效，交易金额等于0不解析

                //取出规则表对应的金额表达式
                String amtexpform = acfb06.getAmtexpform();

                //金额表达式处理路径

                //1，根据子流水序号查询子流水辅表，取出金额属性集合

                List<ACFE17> acfe17List = acfe17Repository.findAllByKltrnseqnoAndKlregseq(acfe16.getKltrnseqno(),acfe16.getKlregseq());

                //1，以子流水辅表中的金额属性对应的金额值替换规则表中表达式的金额属性
                amtexpform = replaceAmtexpform(amtexpform, acfe17List);


                //2，根据金额表达式四则运算得到待解析的此笔流水对应的解析金额
                BigDecimal amount = AcCalculator.conversion(amtexpform);

                //3，判断金额表达式是否为零,如果为零，则程序跳过，此笔不做处理
                if (amount.compareTo(SystemConstant.ZERO2) != 0) {
                    logger.info("解析公式:{}",amtexpform);
                    logger.info("解析金额为:{}",amount);
                    //4，开始传票基本信息赋值
                    ACFE20 acfe20Where = new ACFE20();
                    //交易金额
                    acfe20Where.setAmttran(amount);
                    // 传票基本信息赋值
                    bookseq++;
                    setBookFields(acfm15, acfb06, acfe16, acfe20Where,bookseq);
                    //5，传票核算机构信息赋值，根据规则配置表中机构来源标志进行判断
                    String brc = getBrc(acfe16, acfb06);

                    acfe20Where.setBrcacc(brc);

                    //6, 传票科目(业务代号)信息赋值 根据解析规则表中科目来源进行判断

                    logger.info("科目来源,{}",acfb06.getSubsrctyp());
                    // 按固定科目
                    if (StringUtils.equals(acfb06.getSubsrctyp(), AccConstant.SUB_NO_SOURCE_2)) {


                            acfe20Where.setBusno(acfb06.getBusno());

                    }
                    //按产品
                    if (StringUtils.equals(acfb06.getSubsrctyp(), AccConstant.SUB_NO_SOURCE_1)) {

                        //查询核算产品属性

                        List<ACFM08> acfm08List = acfm08Repository.findAllByAccprdno(accprdno);
                        //查询核算产品业务代号映射表

                        List<ACFB07> acfb07List = acfb07Repository.findAllByAccprdno(accprdno);

                        String busno = getSubNoByPrdCode(acfb06, acfm08List, acfe17List, acfb07List);

                        acfe20Where.setBusno(busno);
                    }
                    //按内部账户
                    if (StringUtils.equals(acfb06.getSubsrctyp(), AccConstant.SUB_NO_SOURCE_3)) {

//                        //内部账户获取科目 查询内部账户索引表
//                        IAFE06 iafe06Where = new IAFE06();
//                        iafe06Where.setBankno(acfe16.getBankno());
//                        iafe06Where.setAccbooktyp(acfe16.getAccbooktyp());
//                        iafe06Where.setAccno(acfe16.getAccno());
//                        IAFE06 iafe06 = daoService.selectOne(iafe06Where, pubcom);
//                        // 业务代号赋值
//                        acfe20Where.setBusno(iafe06.getBusno());

                    }
                    //更新传票记账状态 //未入账
                    acfe20Where.setFlgaccent(AccConstant.ACC_ENT_FLAG_1);
                    //新增传票入库

                    acfe20Repository.save(acfe20Where);


                }
            }
        }
    }


    /**
     * @return String 替换后的金额表达式字符串
     * @Author
     * @Description 替换传票解析规则中的金额表达式中的金额要素，得到具体要计算的四则运算表达式
     * @Date 2021/2/19
     * @Param 待替换金额表达式(amtexpForm), 子流水辅表集合(acfe17List)
     * @module 会计核算
     **/
    private  String replaceAmtexpform(String amtexpForm, List<ACFE17> acfe17List) {
        String str = amtexpForm;
        for (ACFE17 ac : acfe17List
        ) {
            str = StringUtils.replace(str, ac.getPropdesc(), ac.getPropval());
        }
        return str;
    }

    /**
     * @return 传票机构号
     * @Author 曹国鹏
     * @Description 根据传票解析规则/会计子流水表 查询传票机构号
     * @Date 2021/2/20
     * @Param
     * @module 会计核算-引擎
     **/
    private  String getBrc(ACFM16 acfe16, ACFB06 acfb06) {


        String brc = "";

        //根据传票解析规则表Brcsrcflg来判断
        //1，交易中获取
        if (StringUtils.equals(acfb06.getFlgbrcsrc(), AccConstant.BRC_NO_SOURCE_1)) {
            //取子流水账务机构号
            brc = acfe16.getBrcadsc();
        }

        //2，固定机构号
        if (StringUtils.equals(acfb06.getFlgbrcsrc(), AccConstant.BRC_NO_SOURCE_2)) {
            //取解析规则表中配置的机构号
            brc = acfb06.getBrcacct();
        }

        return brc;
    }

    /**
     * @return 业务代号
     * @Author 曹国鹏
     * @Description 根据传票解析规则/会计子流水表/核算子流水辅表/核算产品属性表/核算产品与业务代号映射表 查询业务代号
     * @Date 2021/2/20
     * @Param
     * @module 会计核算-引擎
     **/
    private  String getSubNoByPrdCode(ACFB06 acfb06, List<ACFM08> acfm08List, List<ACFE17> acfe17List, List<ACFB07> acfb07List) {

        String busno = "";
        StringJoiner attributes = new StringJoiner("_");
        //核算产品属性集合排序，按照配置顺序进行排序
        acfm08List = acfm08List.stream().sorted(Comparator.comparing(ACFM08::getRegseq)).collect(Collectors.toList());

        //循环遍历子流水辅助表，取出产品属性 拼接串 形如:CNY_3_01
        for (ACFM08 acm08 : acfm08List
        ) {
            for (ACFE17 acfe17 : acfe17List
            ) {
                if (StringUtils.equals(acfe17.getFlgprdamt(), AccConstant.PRD_PROP_TYPE_1) && StringUtils.equals(acfe17.getPropdesc(), acm08.getPrdpropcod())) {
                    attributes.add(acfe17.getPropval());
                }
            }
        }

        logger.info("产品属性串{}",attributes);

        //遍历核算产品业务代号映射表，根据核算产品码，属性代码，记账要素 获取业务代号
        for (ACFB07 acfb07 : acfb07List
        ) {
            if (StringUtils.equals(acfb07.getAccprdno(), acfb06.getAccprdno())
                    && StringUtils.equals(acfb07.getAccfaccode(), acfb06.getAccfaccode())
                    && StringUtils.equals(acfb07.getAccpropno(), attributes.toString())) {
                busno = acfb07.getBusno();
            }
        }

        return busno;
    }

    /**
     * @return
     * @Author 曹国鹏
     * @Description 根据传票解析规则，会计主流水，会计子流水表 为传票赋值
     * @Date 2021/2/20
     * @Param acfm15，acfe16，acfe20
     * @module 会计核算-引擎
     **/
    private static void   setBookFields(ACFM15 acfm15, ACFB06 acfb06, ACFM16 acfe16, ACFE20 acfe20,int bookseq) {


        //传票序号
        acfe20.setBookseq(bookseq);

        // 核心交易代码
        acfe20.setKltrncode(acfm15.getKltrncode());

        // 被冲销标志
        acfe20.setFlgbrevcls(acfm15.getFlgbrevcls());

        // 交易接口类型
        //acfe20.setTrnintftyp(acfm15.getTrnintftyp);

        // 流水账类型 (正常，冲销，补正)
        acfe20.setJouracctyp(acfm15.getJouracctyp());

        // 发起方交易日期
        acfe20.setDtfrnt(acfm15.getDtfrnt());

        // 发起方流水号
        acfe20.setFrntseqno(acfm15.getFrntseqno());

        // 会计交易日期
        acfe20.setDtacctrn(acfm15.getDtacctrn());

        // 交易柜员
        acfe20.setTeltran(acfm15.getTeltran());

        // 复核柜员
        acfe20.setTelchk(acfm15.getTelchk());

        // 授权柜员
        acfe20.setTelauth(acfm15.getTelauth());

        // 银行号
        acfe20.setBankno(acfe16.getBankno());

        // 账薄类别
        acfe20.setAccbooktyp(acfe16.getAccbooktyp());

        // 适用系统
        acfe20.setSubsys(acfe16.getSubsys());

        //会计日期
        acfe20.setDtacc(acfe16.getDtacc());

        //会计交易流水号
        acfe20.setActrnseqno(acfe16.getActrnseqno());

        //会计子流水序号
        acfe20.setKlregseq(acfe16.getKlregseq());

        //分户帐记账标识(避免重复记账属性)
        acfe20.setFlgentsub(acfe16.getFlgentsub());

        // 交易事件代码
        acfe20.setTranevntno(acfe16.getTranevntno());

        //账号顺序号
        acfe20.setAccno(acfe16.getAccno());

        //客户号
        acfe20.setCustno(acfe16.getCustno());

        //产品编号
        acfe20.setPrdno(acfe16.getPrdno());

        //产品账号
        acfe20.setPrdaccno(acfe16.getPrdaccno());

        //产品账户名称
        acfe20.setPrdaccname(acfe16.getPrdaccname());

        // 钞汇标志
        acfe16.setFlgcr(acfe16.getFlgcr());

        // 借贷标志
        acfe20.setFlgdc(acfb06.getFlgdc());

        // 币种
        acfe20.setCcy(acfe16.getCcy());

        // 凭证种类
        acfe20.setVoutyp(acfe16.getVoutyp());

        // 凭证号
        acfe20.setVouno(acfe16.getVouno());

        // 借据编号
        acfe20.setDebitno(acfe16.getDebitno());

        // 对方账号
        acfe20.setOppaccno(acfe16.getOppaccno());

        // 对方户名
        acfe20.setOppaccnm(acfe16.getOppaccnm());

        // 对方行号
        acfe20.setOppbankno(acfe16.getOppbankno());

        // 对方行名
        acfe20.setOppbanknm(acfe16.getOppbanknm());

        //对方币种
        acfe20.setOppccy(acfe16.getOppccy());

        //核算记账方式
        acfe20.setAccentmod(acfb06.getAccentmod());

        // 核算入账标识 此处默认 "未入账"
        acfe20.setFlgaccent(AccConstant.ACC_ENT_FLAG_1);

        // 特殊清算标志 此处默认为 "正常"
        acfe20.setFlgspecls(AccConstant.FLGCLRS_NORMAL_0);

        // 清算传票标志  此处默认为 "记账传票"
        acfe20.setFlgclsbook(AccConstant.FLGCLRS_TYPE_0);

        //特殊处理标志  此处默认为 "未特殊处理"
        acfe20.setFlgspecls(AccConstant.FLGDEALS_UNTREATED_0);

        //摘要
        acfe20.setMemo(acfe16.getMemo());

        // 摘要码
        acfe20.setMemono(acfe16.getMemono());

        // 备注说明
        acfe20.setRemark(acfe16.getRemark());

        // 核心交易日期
        acfe20.setDtkltrn(acfe16.getDtkltrn());

        // 核心交易流水号
        acfe20.setKltrnseqno(acfe16.getKltrnseqno());

        // 记录状态 默认"正常"
        acfe20.setStsrcd(AccConstant.RECORD_STATUS_YES);

    }

}
