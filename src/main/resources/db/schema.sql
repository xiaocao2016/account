

CREATE TABLE if not exists  `ac_trn_book` (
  `id` varchar(50)   NOT NULL,
  `bank_id` varchar(50)   ,
  `corporate` varchar(50)   ,
  `create_date` datetime ,
  `create_user` varchar(50)   ,
  `create_user_id` varchar(50)   ,
  `is_disable` bit(1) NOT NULL,
  `update_date` datetime ,
  `update_user` varchar(50)   ,
  `update_user_id` varchar(50)   ,
  `acbnod` varchar(255)   ,
  `accnama` varchar(255)   ,
  `accno` varchar(255)   ,
  `accnoa` varchar(255)   ,
  `accsub` varchar(255)   ,
  `brcadsc` varchar(255)   ,
  `brcbcdb` varchar(255)   ,
  `brcoter` varchar(255)   ,
  `ccy` varchar(255)   ,
  `ccyopp` varchar(255)   ,
  `custid` varchar(255)   ,
  `datetrn` varchar(255)   ,
  `debtno` varchar(255)   ,
  `flgclrs` varchar(255)   ,
  `flgcr` varchar(255)   ,
  `flgcrt` varchar(255)   ,
  `flgdeals` varchar(255)   ,
  `isclsal` varchar(255)   ,
  `memo` varchar(255)   ,
  `memono` varchar(255)   ,
  `prdcode` varchar(255)   ,
  `scene` varchar(255)   ,
  `serseqn` varchar(255)   ,
  `teller` varchar(255)   ,
  `vouno` varchar(255)   ,
  `voutyp` varchar(255)   ,
  PRIMARY KEY (`id`)
) ;

-- ----------------------------
--  Table structure for `acc_ac_trn_info`
-- ----------------------------

CREATE TABLE if not exists `acc_ac_trn_info` (
`id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
`bankid` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`corporate` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`create_date` datetime(0) NULL DEFAULT NULL,
`create_user` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`create_user_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`is_disable`  bit(1) NOT NULL,
`update_date` datetime(0) NULL DEFAULT NULL,
`update_user` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`update_user_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`subbnkn` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`datetrn` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`datacc`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`serseqn` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`brctran` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`trancode` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`chnelno`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`frntdat`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`frntseq` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`datoltr` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`seqoltr` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`trandate` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`transeq` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`module` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`flgdeal` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`flgtim` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`isclsal` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`stantyp` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`flgcanl` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`teltran` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`telchk` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`brcmod` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`datemod` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`moddatetime` datetime(0) NULL DEFAULT NULL,
`fldbak1` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`fldbak2` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`fldbak3` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`fldbak4` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`fldbak5` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`fldbak6` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
`updtime` datetime(0) NULL DEFAULT NULL,
 PRIMARY KEY (`id`) USING BTREE
) ;

-- ----------------------------
--  Table structure for `acc_book_amount`
-- ----------------------------

CREATE TABLE if not exists `acc_book_amount` (
  `id` varchar(50)   NOT NULL,
  `bank_id` varchar(50)   ,
  `corporate` varchar(50)   ,
  `create_date` datetime ,
  `create_user` varchar(50)   ,
  `create_user_id` varchar(50)   ,
  `is_disable` bit(1) NOT NULL,
  `update_date` datetime ,
  `update_user` varchar(50)   ,
  `update_user_id` varchar(50)   ,
  `book_id` varchar(255)   ,
  `book_tag` varchar(255)   ,
  `book_value` varchar(255)   ,
  PRIMARY KEY (`id`)
) ;

-- ----------------------------
--  Table structure for `acc_book_attributes`
-- ----------------------------

CREATE TABLE if not exists `acc_book_attributes` (
  `id` varchar(50)   NOT NULL,
  `bank_id` varchar(50)   ,
  `corporate` varchar(50)   ,
  `create_date` datetime ,
  `create_user` varchar(50)   ,
  `create_user_id` varchar(50)   ,
  `is_disable` bit(1) NOT NULL,
  `update_date` datetime ,
  `update_user` varchar(50)   ,
  `update_user_id` varchar(50)   ,
  `book_id` varchar(255)   ,
  `book_tag` varchar(255)   ,
  `book_value` varchar(255)   ,
  PRIMARY KEY (`id`)
) ;

-- ----------------------------
--  Table structure for `acc_prdrule_def`
-- ----------------------------

CREATE TABLE if not exists `acc_prdrule_def` (
  `id` varchar(50)   NOT NULL,
  `bank_id` varchar(50)   ,
  `corporate` varchar(50)   ,
  `create_date` datetime ,
  `create_user` varchar(50)   ,
  `create_user_id` varchar(50)   ,
  `is_disable` bit(1) NOT NULL,
  `update_date` datetime ,
  `update_user` varchar(50)   ,
  `update_user_id` varchar(50)   ,
  `prdrule_code` varchar(255)   ,
  `prdrule_name` varchar(255)   ,
  `product_code` varchar(255)   ,
  `product_name` varchar(255)   ,
  PRIMARY KEY (`id`)
) ;

-- ----------------------------
--  Table structure for `acc_product_attributes`
-- ----------------------------

CREATE TABLE if not exists `acc_product_attributes` (
  `id` varchar(50)   NOT NULL,
  `bank_id` varchar(50)   ,
  `corporate` varchar(50)   ,
  `create_date` datetime ,
  `create_user` varchar(50)   ,
  `create_user_id` varchar(50)   ,
  `is_disable` bit(1) NOT NULL,
  `update_date` datetime ,
  `update_user` varchar(50)   ,
  `update_user_id` varchar(50)   ,
  `product_code` varchar(255)   ,
  `product_desc` varchar(255)   ,
  `product_label` varchar(255)   ,
  `product_number` varchar(255)   ,
  PRIMARY KEY (`id`)
) ;

-- ----------------------------
--  Table structure for `acc_product_subject`
-- ----------------------------

CREATE TABLE if not exists `acc_product_subject` (
  `id` varchar(50)   NOT NULL,
  `bank_id` varchar(50)   ,
  `corporate` varchar(50)   ,
  `create_date` datetime ,
  `create_user` varchar(50)   ,
  `create_user_id` varchar(50)   ,
  `is_disable` bit(1) NOT NULL,
  `update_date` datetime ,
  `update_user` varchar(50)   ,
  `update_user_id` varchar(50)   ,
  `product_desc` varchar(255)   ,
  `book_element_name` varchar(255)   ,
  `book_element_tag` varchar(255)   ,
  `busnoa` varchar(255)   ,
  `element_code` varchar(255)   ,
  `product_code` varchar(255)   ,
  `serial` varchar(255)   ,
  PRIMARY KEY (`id`)
) ;

-- ----------------------------
--  Table structure for `acc_rules`
-- ----------------------------

CREATE TABLE if not exists `acc_rules` (
  `id` varchar(50)   NOT NULL,
  `bank_id` varchar(50)   ,
  `corporate` varchar(50)   ,
  `create_date` datetime ,
  `create_user` varchar(50)   ,
  `create_user_id` varchar(50)   ,
  `is_disable` bit(1) NOT NULL,
  `update_date` datetime ,
  `update_user` varchar(50)   ,
  `update_user_id` varchar(50)   ,
  `accenmd` varchar(255)   ,
  `amount_expression` varchar(255)   ,
  `busnoa` varchar(255)   ,
  `busse` varchar(255)   ,
  `elements` varchar(255)   ,
  `flgdc` varchar(255)   ,
  `org_flag` varchar(255)   ,
  `org_id` varchar(255)   ,
  `org_source` varchar(255)   ,
  `scenseq` varchar(255)   ,
  `sernob` varchar(255)   ,
  `subsrc` varchar(255)   ,
  PRIMARY KEY (`id`)
) ;

-- ----------------------------
--  Table structure for `account_scene`
-- ----------------------------

CREATE TABLE if not exists `account_scene` (
  `id` varchar(50)   NOT NULL,
  `bank_id` varchar(50)   ,
  `corporate` varchar(50)   ,
  `create_date` datetime ,
  `create_user` varchar(50)   ,
  `create_user_id` varchar(50)   ,
  `is_disable` bit(1) NOT NULL,
  `update_date` datetime ,
  `update_user` varchar(50)   ,
  `update_user_id` varchar(50)   ,
  `scene_code` varchar(255)   ,
  `scene_name` varchar(255)   ,
  PRIMARY KEY (`id`)
) ;

-- ----------------------------
--  Table structure for `human_dict`
-- ----------------------------

CREATE TABLE if not exists `human_dict` (
  `id` varchar(50)   NOT NULL,
  `company_code` varchar(50)   ,
  `company_name` varchar(255)   ,
  `create_date` datetime ,
  `create_user` varchar(50)   ,
  `create_user_id` varchar(50)   ,
  `department_code` varchar(50)   ,
  `department_name` varchar(255)   ,
  `is_disable` bit(1) NOT NULL,
  `update_date` datetime ,
  `update_user` varchar(50)   ,
  `update_user_id` varchar(50)   ,
  `attributes` varchar(255)   ,
  `full_name` varchar(500)   ,
  `is_parent` bit(1) NOT NULL,
  `name` varchar(255)   ,
  `parent_id` varchar(50)   ,
  `_rank` int(11) ,
  `top_name` varchar(255)   ,
  `level` int(11) NOT NULL,
  `bank_id` varchar(50)   ,
  `corporate` varchar(50)   
) ;

-- ----------------------------
--  Table structure for `system_dictionary`
-- ----------------------------

CREATE TABLE if not exists `system_dictionary` (
  `id` varchar(50)   NOT NULL,
  `create_date` datetime ,
  `create_user` varchar(50)   ,
  `create_user_id` varchar(50)   ,
  `department_code` varchar(50)   ,
  `is_disable` bit(1) NOT NULL,
  `update_date` datetime ,
  `update_user` varchar(50)   ,
  `update_user_id` varchar(50)   ,
  `attributes` varchar(255)   ,
  `full_name` varchar(255)   ,
  `is_parent` bit(1) NOT NULL,
  `name` varchar(255)   ,
  `parent_id` varchar(50)   ,
  `_rank` int(11) ,
  `company_code` varchar(50)   ,
  `company_name` varchar(255)   ,
  `department_name` varchar(255)   ,
  `top_name` varchar(255)   ,
  `bank_id` varchar(50)   ,
  `corporate` varchar(50)   
) ;

create table acc_subject_info
(
    id                 varchar(50)  not null primary key,
    bank_id            varchar(50)  null,
    corporate          varchar(50)  null,
    create_date        datetime     null,
    create_user        varchar(50)  null,
    create_user_id     varchar(50)  null,
    is_disable         bit          not null,
    update_date        datetime     null,
    update_user        varchar(50)  null,
    update_user_id     varchar(50)  null,
    business_code      varchar(255) null,
    gl_subject         varchar(255) null,
    gl_subject_info    varchar(255) null,
    level1subject      varchar(255) null,
    level1subject_info varchar(255) null,
    level2subject      varchar(255) null,
    level2subject_info varchar(255) null
);



