package com.ws.accounting.base;

import com.ws.accounting.dto.BrcInfo;
import lombok.Data;

@Data
public class AjaxResult {
    private int code;
    private String message;
    private Object data;
    private String tranCode;
    private String subSys;
    private String tranDate;
    private String tranDate8;
    private String accDate;
    private String preDate;
    private String sysStatus;
    private String tranTime;
    private String ptDate;
    private String ptseq;
    private String microTranCode;
    private String prcNode;
    private String muid;
    private String muname;
    private String subid;
    private String trantype;
    private String tranmode;
    private String tranlevel;
    private String sysmode;
    private String commpmode;
    private String msgmode;
    private String macchk;
    private String tellerchk;
    private String branchchk;
    private String fluxflg;
    private String inchkflg;
    private String repeatflg;
    private String rulesflg;
    private String formmaxin;
    private String formmaxout;
    private String mulogflg;
    private String cancelflg;
    private String crtseqflg;
    private String timeoutflg;
    private String clsoffflg;
    private String enableflg;
    private String filemaxout;
    private String timeout;
    private String rsvflag1;
    private String rsvflag2;
    private String rsvflag3;
    private String regseqflg;
    private String conflg;
    private String revflg;
    private String brc;
    private BrcInfo brcinfo;
    private String teller;
    private String ccy;
    private String multFlag;
    private String fileFlag;
    private String fileName;
    private Integer fileIRecNum;
    private Integer fileORecNum;
    private Integer fileOMaxNum = 100;
    private Integer formSRecSeq;
    private Integer formIRecNum;
    private Integer formORecNum;
    private Integer formOMaxNum = 100;
    private String isBat;
    private String isOnlineBat;
    private String repeatFlg;
    private String exFlg;
    private String reserved;
    private Integer evtNum;
    private String isPbEvent;
    private String isDpEvent;
    private String isLnEvent;
    private String tranSeqNo;
    private String rspcode = "AAAAAAA";
    private String dspMsg;
    private String rspMSG = "";
    private String addMsg1 = "";
    private String addMsg2 = "";
    private String addMsg3 = "";
    private String addMsg4 = "";
    private String addMsg5 = "";
    private String reqTranDate;
    private String reqTranSeqNo;
    private String reqTranChnlId;
    private String reqTranCode;
    private String reqTranDesc;
    private String orgDate;
    private String orgSeqNo;
    private String orgFrntNo;
    private String orgChnlId;
    private String orgTranCode;
    private String orgTranDesc;
    private String thrdTranDate;
    private String thrdSeqNo;
    private String areaCode;
    private String busiKind;
    private String authFlag;
    private String aBrc;
    private String aTeller;
    private String authPasswd;
    private String authCode;
    private String feeFlag;
    private String feePaySrc1;
    private String feeCashCode;
    private String custAcctNo;
    private String feePayAcctNo;
    private String feePayAccSub;
    private String feeCcy;
    private String feeRmtcFlag;


    public static AjaxResult getInstance() {
        AjaxResult instance = new AjaxResult();
        instance.code=200;
        instance.message=null;
        instance.data=null;
        return instance;
    }

    public static AjaxResult getInstance(String message) {
        AjaxResult instance = new AjaxResult();
        instance.code=200;
        instance.message=message;
        instance.data=null;
        return instance;
    }

    public static AjaxResult getInstance(Object data) {
        AjaxResult instance = new AjaxResult();
        instance.code=200;
        instance.message=null;
        instance.data=data;
        return instance;
    }

    public static AjaxResult getInstance(int code,String message) {
        AjaxResult instance = new AjaxResult();
        instance.code=code;
        instance.message=message;
        instance.data=null;
        return instance;
    }

    public static AjaxResult getInstance(String message,Object data) {
        AjaxResult instance = new AjaxResult();
        instance.code=200;
        instance.message=message;
        instance.data=data;
        return instance;
    }

    public static AjaxResult getInstance(int code,String message,Object data) {
        AjaxResult instance = new AjaxResult();
        instance.code=code;
        instance.message=message;
        instance.data=data;
        return instance;
    }

}
