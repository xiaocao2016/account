package com.ws.accounting.base;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@MappedSuperclass
/**
 * 基础类
 */
@Data
public abstract class EntityBase implements Serializable {

    /**
     * 编号
     */
    @Id
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @GeneratedValue(generator = "system-uuid")
    @Column(length = 50)
    private String id;



    /**
     * 法人代码
     */
    @Column(length = 50)
    private String corporate;



    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    /**
     * 创建时间
     */
    private Date createDate;

    /**
     * 创建人编号
     */
    @Column(length = 50)
    private String createUserId;

    /**
     * 创建人姓名
     */
    @Column(length = 50)
    private String createUser;

    /**
     * 是否禁用
     */
    private boolean isDisable;

    /**
     * 修改时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date updateDate;

    /**
     * 修改人编号
     */
    @Column(length = 50)
    private String updateUserId;

    /**
     * 修改人姓名
     */
    @Column(length = 50)
    private String updateUser;

}