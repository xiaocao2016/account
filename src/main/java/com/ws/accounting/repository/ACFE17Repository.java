package com.ws.accounting.repository;




import com.ws.accounting.entity.ACFE16;
import com.ws.accounting.entity.ACFE17;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ACFE17Repository extends PagingAndSortingRepository<ACFE17,String> {

    List<ACFE17> findAllByKlregseq(String klregseq);

    List<ACFE17> findAllByKltrnseqnoAndKlregseq(String kltrnseqno,String klreqseq);
}
