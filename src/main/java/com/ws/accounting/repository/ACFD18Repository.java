package com.ws.accounting.repository;




import com.ws.accounting.entity.ACFD05;
import com.ws.accounting.entity.ACFD18;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ACFD18Repository extends PagingAndSortingRepository<ACFD18,String> {
}
