package com.ws.accounting.repository;

import com.ws.accounting.entity.MeCrBrfd;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface MeCrBrfdRepository extends PagingAndSortingRepository<MeCrBrfd,String> {
}
