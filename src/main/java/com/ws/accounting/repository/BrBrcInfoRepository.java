package com.ws.accounting.repository;

import com.ws.accounting.entity.AcAccDetail;
import com.ws.accounting.entity.BrBrcInfo;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface BrBrcInfoRepository extends PagingAndSortingRepository<BrBrcInfo,String> {
}
