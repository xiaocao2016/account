package com.ws.accounting.repository;




import com.ws.accounting.entity.ACFM18;
import com.ws.accounting.entity.ACFM22;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ACFM22Repository extends PagingAndSortingRepository<ACFM22,String> {
}
