package com.ws.accounting.repository;


import com.ws.accounting.entity.ACFB07;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ACFB07Repository extends PagingAndSortingRepository<ACFB07,String> {

    List<ACFB07> findAllByAccprdno(String accprdno);
}
