package com.ws.accounting.repository;

import com.ws.accounting.dto.AcClearPathDto;
import com.ws.accounting.entity.AcClearPath;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface AcClearPathRepository extends PagingAndSortingRepository<AcClearPath,String> {

}
