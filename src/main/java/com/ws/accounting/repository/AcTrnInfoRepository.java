package com.ws.accounting.repository;

import com.ws.accounting.entity.AcTrnInfo;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface AcTrnInfoRepository extends PagingAndSortingRepository<AcTrnInfo,String> {
}
