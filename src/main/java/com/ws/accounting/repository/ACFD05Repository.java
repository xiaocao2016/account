package com.ws.accounting.repository;




import com.ws.accounting.entity.ACFD03;
import com.ws.accounting.entity.ACFD05;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ACFD05Repository extends PagingAndSortingRepository<ACFD05,String> {
    ACFD05 findByExtprdno(String extprdno);
}
