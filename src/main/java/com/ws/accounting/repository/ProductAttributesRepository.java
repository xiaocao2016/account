package com.ws.accounting.repository;

import com.ws.accounting.entity.ProductAttributes;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface ProductAttributesRepository extends PagingAndSortingRepository<ProductAttributes,String> {

    List<ProductAttributes> findAllByProductCode(String productCode);
}
