package com.ws.accounting.repository;

import com.ws.accounting.entity.ACFB06;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ACFB06Repository extends PagingAndSortingRepository<ACFB06,String> {

    List<ACFB06> findAllByTranevntno(String tranevntno);
}
