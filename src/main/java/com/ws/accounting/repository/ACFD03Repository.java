package com.ws.accounting.repository;




import com.ws.accounting.entity.ACFD02;
import com.ws.accounting.entity.ACFD03;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ACFD03Repository extends PagingAndSortingRepository<ACFD03,String>{
}
