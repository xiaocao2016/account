package com.ws.accounting.repository;

import com.ws.accounting.entity.Scene;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface SceneRepository extends PagingAndSortingRepository<Scene,String> {
}
