package com.ws.accounting.repository;

import com.ws.accounting.entity.Dict;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DictRepository extends PagingAndSortingRepository<Dict,String> {
    List<Dict> findByParentIdOrderByRankAsc(String parentId);
    List<Dict> findByNameAndParentId(String value, String parentId);
    List<Dict> findByTopName(String topName);
    Dict findByNameAndTopName(String name,String topName);
    List<Dict> findByTopNameAndIsParentOrderByRankAsc(String topName,boolean isParent);
    int countByNameAndParentId(String value, String parentId);
    int countByParentId(String parentId);
    int countByNameAndParentIdAndIdNot(String value, String parentId, String id);
    Dict findOneByNameAndParentId(String value, String parentId);

    List<Dict> findAllByLevel(int level);
}
