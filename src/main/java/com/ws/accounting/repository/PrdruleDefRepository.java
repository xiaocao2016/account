package com.ws.accounting.repository;

import com.ws.accounting.entity.PrdruleDef;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PrdruleDefRepository extends PagingAndSortingRepository<PrdruleDef,String> {

    PrdruleDef findByProductCode(String productCode);
}
