package com.ws.accounting.repository;




import com.ws.accounting.entity.ACFD02;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ACFD02Repository extends PagingAndSortingRepository<ACFD02,String>{
}
