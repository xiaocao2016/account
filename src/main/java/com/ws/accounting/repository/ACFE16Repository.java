package com.ws.accounting.repository;




import com.ws.accounting.entity.ACFE09;
import com.ws.accounting.entity.ACFE16;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ACFE16Repository extends PagingAndSortingRepository<ACFE16,String> {
}
