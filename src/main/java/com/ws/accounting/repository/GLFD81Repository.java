package com.ws.accounting.repository;




import com.ws.accounting.entity.ACFM22;
import com.ws.accounting.entity.GLFD81;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GLFD81Repository extends PagingAndSortingRepository<GLFD81,String>{
}
