package com.ws.accounting.repository;

import com.ws.accounting.entity.AcAccDetail;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface AcAccDetailRepository extends PagingAndSortingRepository<AcAccDetail,String> {


}
