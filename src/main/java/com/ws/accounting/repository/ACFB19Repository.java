package com.ws.accounting.repository;




import com.ws.accounting.entity.ACFB19;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ACFB19Repository extends PagingAndSortingRepository<ACFB19,String> {
}
