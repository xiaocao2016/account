package com.ws.accounting.repository;


import com.ws.accounting.entity.BrcCtrlShip;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * ======================================================
 *
 * @System:[核算系统]
 * @Desc: [ss类/组件描述]
 * @author: [weibin.zhu]
 * @date: 2021-02-11 10:08
 * @copyright: [Pactera]
 * ========================================================
 */
public interface BrcCtrlShipRepository  extends PagingAndSortingRepository<BrcCtrlShip,String> {
}



