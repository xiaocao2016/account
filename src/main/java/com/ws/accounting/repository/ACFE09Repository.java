package com.ws.accounting.repository;




import com.ws.accounting.entity.ACFD18;
import com.ws.accounting.entity.ACFE09;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ACFE09Repository extends PagingAndSortingRepository<ACFE09,String> {
}
