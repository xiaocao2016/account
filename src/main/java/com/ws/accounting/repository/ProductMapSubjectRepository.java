package com.ws.accounting.repository;

import com.ws.accounting.entity.ProductMapSubject;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface ProductMapSubjectRepository extends PagingAndSortingRepository<ProductMapSubject,String> {

    List<ProductMapSubject> findAllByProductCodeAndElementCode(String productCode,String elementCode);
    ProductMapSubject findByProductCodeAndBookElementNameAndElementCode(String productCode,String bookElementName,String elementCode);
}
