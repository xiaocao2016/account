package com.ws.accounting.repository;




import com.ws.accounting.entity.ACFM15;
import com.ws.accounting.entity.ACFM18;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ACFM18Repository extends PagingAndSortingRepository<ACFM18,String> {
}
