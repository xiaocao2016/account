package com.ws.accounting.repository;



import com.ws.accounting.entity.ACFB08;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ACFB08Repository extends PagingAndSortingRepository<ACFB08,String>{
}
