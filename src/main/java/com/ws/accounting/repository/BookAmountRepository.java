package com.ws.accounting.repository;

import com.ws.accounting.entity.BookAmount;
import com.ws.accounting.entity.BookAttributes;
import org.springframework.data.repository.PagingAndSortingRepository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface BookAmountRepository extends PagingAndSortingRepository<BookAmount,String> {

   

}
