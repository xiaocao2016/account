package com.ws.accounting.repository;




import com.ws.accounting.entity.ACFM08;
import com.ws.accounting.entity.ACFM15;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ACFM15Repository extends PagingAndSortingRepository<ACFM15,String> {

    ACFM15 findByActrnseqno(String acseqno);
}
