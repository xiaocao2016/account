package com.ws.accounting.repository;





import com.ws.accounting.entity.ACFE20;
import com.ws.accounting.entity.ACFM08;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ACFE20Repository extends PagingAndSortingRepository<ACFE20,String> {


}
