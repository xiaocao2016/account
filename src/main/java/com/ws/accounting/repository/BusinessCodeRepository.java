package com.ws.accounting.repository;

import com.ws.accounting.entity.BusinessCode;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface BusinessCodeRepository extends PagingAndSortingRepository<BusinessCode,String> {

    BusinessCode findByYEWUDH(String yewudh);
}
