package com.ws.accounting.repository;





import com.ws.accounting.entity.ACFM16;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ACFM16Repository extends PagingAndSortingRepository<ACFM16,String> {

    List<ACFM16> findAllByKltrnseqno(String kltrnseqno);
}
