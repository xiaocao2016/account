package com.ws.accounting.repository;

import com.ws.accounting.entity.AcAccDetail;
import com.ws.accounting.entity.IaAccInfo;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IaAccInfoRepository extends PagingAndSortingRepository<IaAccInfo,String> {
}
