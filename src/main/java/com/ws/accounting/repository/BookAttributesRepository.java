package com.ws.accounting.repository;

import com.ws.accounting.entity.BookAttributes;
import org.springframework.data.repository.PagingAndSortingRepository;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface BookAttributesRepository extends PagingAndSortingRepository<BookAttributes,String> {

        List<BookAttributes> findAllByBookId(String bookId);

        BookAttributes findByBookIdAndBookTag(String bookId,String bookTag);

        void deleteAllByBookId(String bookId);
}
