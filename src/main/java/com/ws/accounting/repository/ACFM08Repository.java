package com.ws.accounting.repository;





import com.ws.accounting.entity.ACFM08;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ACFM08Repository extends PagingAndSortingRepository<ACFM08,String> {

    List<ACFM08> findAllByAccprdno(String accprdno);
}
