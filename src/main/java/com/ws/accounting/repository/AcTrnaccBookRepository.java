package com.ws.accounting.repository;

import com.ws.accounting.entity.AcTrnaccBook;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface AcTrnaccBookRepository extends PagingAndSortingRepository<AcTrnaccBook,String> {
    List<AcTrnaccBook> findAllBySerseqn(String serseqn);
}
