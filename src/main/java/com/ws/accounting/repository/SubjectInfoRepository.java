package com.ws.accounting.repository;

import com.ws.accounting.entity.SubjectInfo;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SubjectInfoRepository extends PagingAndSortingRepository<SubjectInfo,String> {
}
