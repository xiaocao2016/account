package com.ws.accounting.repository;




import com.ws.accounting.entity.ACFM22;
import com.ws.accounting.entity.CAFB02;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CAFB02Repository extends PagingAndSortingRepository<CAFB02,String> {
}
