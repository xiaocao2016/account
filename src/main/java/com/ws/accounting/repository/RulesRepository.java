package com.ws.accounting.repository;

import com.ws.accounting.entity.Rules;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface RulesRepository extends PagingAndSortingRepository<Rules,String>{

    //根据场景查询解析规则表
    Rules findByScenseq(String scenseq);
    Rules findByScenseqAndElements(String scense,String elements);

    List<Rules> findAllByScenseqAndProductCode(String scenseq,String productCode);

    List<Rules> findAllByBusse(String busse);
}
