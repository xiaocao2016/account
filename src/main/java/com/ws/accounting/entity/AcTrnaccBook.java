package com.ws.accounting.entity;

import com.ws.accounting.base.EntityBase;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.math.BigDecimal;
import java.util.Map;

/*
* 子流水表
* */

@Data
@Entity
@Table(name = "ac_trn_book")
public class AcTrnaccBook extends EntityBase {
    private String subbnkn;  // 法人行号
    private String datacc;   // 会计日期
    private String serseqn;  // 流水号
    private String seq;      // 序号
    private String datetrn;  // 交易日期
    private String prdcode;  // 产品代码
    private String ccy;      // 币种
    private String sublcod;  // 核算产品编号
    private String custid;   // 客户号
    private String accno;    // 内部账号
    private String accsub;   // 产品账号
    private String debtno;   // 借据编号
    //private String ;         // 产品账号户名 TODO 字段名待确认
    private String acbnod;   // 账簿项编号
    private String flgcr;    // 钞汇标志
    private String brcadsc;  // 归属机构
    private String scene;    // 业务场景
    private String vouno;    // 凭证号
    private String voutyp;   // 凭证种类
    private String memono;   // 摘要码
    private String memo;     // 摘要
    private String accnoa;   // 对方账号
    private String accnama;  // 对方户名
    private String brcoter;  // 对方行号
    private String brcbcdb;  // 对方行名
    private String ccyopp;   // 对方币种
    private String flgcrt;   // 对方钞汇标识
    private String flgclrs;  // 特殊清算标志
    private String flgdeals; // 特殊处理标志
    private String flgsmmi;  // 小微企业标志
    private String flgplan;  // 小额贷款标志
    private String ratecur;  // 执行利率
    private String rateint;  // 利率

    @Transient
    private Map<String, String> attribute;  //产品记账要素

    @Transient
    private Map<String, String> amount;   //金额要素

}
