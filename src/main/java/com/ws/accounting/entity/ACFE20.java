package com.ws.accounting.entity;


    import com.ws.accounting.base.EntityBase;
    import lombok.Data;

    import javax.persistence.Entity;
    import javax.persistence.Table;
    import java.math.BigDecimal;

/**
* @Desc 传票表 数据库对象<br>
* @author www.pactera-fintech.com
* @date 2021-02-23 10:02:05
*/
@Data
@Entity
@Table(name = "acfe20")
public class ACFE20 extends EntityBase {



    /** 银行号 */
    private String bankno;
    /** 账薄类别 */
    private String accbooktyp;
    /** 适用系统 */
    private String subsys;
    /** 会计日期 */
    private String dtacc;
    /** 会计交易流水号 */
    private String actrnseqno;
    /** 核心登记序号 */
    private String klregseq;
    /** 传票序号 */
    private Integer bookseq;
    /** 核心交易代码 */
    private String kltrncode;
    /** 交易名称 */
    private String tranname;
    /** 账务机构/核算机构 */
    private String brcacc;
    /** 交易事件代码 */
    private String tranevntno;
    /** 科目类别 */
    private String subnotyp;
    /** 科目号 */
    private String subno;
    /** 业务代号 */
    private String busno;
    /** 表内或表外标志 */
    private String flgsiso;
    /** 核算记分户标志 */
    private String flgentsub;
    /** 系统序号 */
    private String seqno;
    /** 账号 */
    private String accno;
    /** 客户号 */
    private String custno;
    /** 产品编号 */
    private String prdno;
    /** 产品账号 */
    private String prdaccno;
    /** 产品账号户名 */
    private String prdaccname;
    /** 核算现转标志 */
    private String flgct;
    /** 借贷标志 */
    private String flgdc;
    /** 币种 */
    private String ccy;
    /** 钞汇标志 */
    private String flgcr;
    /** 交易金额 */
    private BigDecimal amttran;
    /** 凭证种类 */
    private String voutyp;
    /** 凭证号 */
    private String vouno;
    /** 借据编号 */
    private String debitno;
    /** 现金项目代码A */
    private String cashid;
    /** 对方账号 */
    private String oppaccno;
    /** 对方户名 */
    private String oppaccnm;
    /** 对手行行号 */
    private String oppbankno;
    /** 对方行名 */
    private String oppbanknm;
    /** 对方币种 */
    private String oppccy;
    /** 对方钞汇标志 */
    private String flgoppcr;
    /** 被冲销标志1 */
    private String flgbrevcls;
    /** 冲销流水号1 */
    private String revclsseq;
    /** 交易接口类型 */
    private String trnintftyp;
    /** 交易流水类型 */
    private String jouracctyp;
    /** 核算记账方式 */
    private String accentmod;
    /** 核算入账标志 */
    private String flgaccent;
    /** 特殊清算标志1 */
    private String flgspecls;
    /** 清算传票标志 */
    private String flgclsbook;
    /** 特殊处理标志 */
    private String flgspedeal;
    /** 摘要 */
    private String memo;
    /** 摘要码 */
    private String memono;
    /** 备注 */
    private String remark;
    /** 核心交易日期 */
    private String dtkltrn;
    /** 核心交易流水号 */
    private String kltrnseqno;
    /** 发起方交易日期2 */
    private String dtfrnt;
    /** 发起方流水号A */
    private String frntseqno;
    /** 会计交易日期 */
    private String dtacctrn;
    /** 交易时间 */
    private String trantime;
    /** 交易机构 */
    private String brc;
    /** 交易柜员1 */
    private String teltran;
    /** 复核员 */
    private String telchk;
    /** 授权柜员 */
    private String telauth;
    /** 记录状态 */
    private String stsrcd;
    /** 保留字段1 */
    private String fldrsv1;
    /** 保留字段2 */
    private String fldrsv2;
    /** 保留字段3 */
    private String fldrsv3;
    /** 保留字段4 */
    private String fldrsv4;
    /** 保留字段5 */
    private String fldrsv5;
    /** 保留字段6 */
    private String fldrsv6;


}
