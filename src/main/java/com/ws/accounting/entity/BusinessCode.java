package com.ws.accounting.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.ws.accounting.base.EntityBase;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 业务代号定义
 */
@Data
@Entity
@Table(name = "acc_business_code")
public class BusinessCode extends EntityBase {
    @ExcelProperty(index = 0)
    private String BIEMNG; //业务代号别名
    @ExcelProperty(index = 1)
    private String YEWUDH; //业务代号
    @ExcelProperty(index = 2)
    private String YWUDHM; // 业务代号名
    @ExcelProperty(index = 3)
    private String KMUHAO; // 科目号
    @ExcelProperty(index = 4)
    private String KEMUCC; //科目存储
    @ExcelProperty(index = 5)
    private String YUEEXZ; // 余额性质
    @ExcelProperty(index = 6)
    private String NFKHBZ; // 能否联机开户
    @ExcelProperty(index = 7)
    private String XIOZLX; // 销帐类型
    @ExcelProperty(index = 8)
    private String TOUZBZ; //透支标志
    @ExcelProperty(index = 9)
    private String JIXIYF; // 计息与否
    @ExcelProperty(index = 10)
    private String LILVBH; // 利率编号
    @ExcelProperty(index = 11)
    private String JSCLBZ; // 入帐标志
    @ExcelProperty(index = 12)
    private String FENLSY; // 分理处适用
    @ExcelProperty(index = 13)
    private String ZHIHSY; // 支行适用
    @ExcelProperty(index = 14)
    private String FENHSY; // 分行适用
    @ExcelProperty(index = 15)
    private String ZNGHSY; // 总行适用
    @ExcelProperty(index = 16)
    private String ZCYWDH; // 支出业务代号
    @ExcelProperty(index = 17)
    private String ZCZHXH; // 支出帐号序号
    @ExcelProperty(index = 18)
    private String SRYWDH; // 收入业务代号
    @ExcelProperty(index = 19)
    private String SRZHXH; // 收入帐号序号
    @ExcelProperty(index = 20)
    private String ZDDYBZ; // 对帐单打印标志
    @ExcelProperty(index = 21)
    private String XJJYBZ; // 现金控制标志
    @ExcelProperty(index = 22)
    private String SFEIKZ; // 收费控制标志
    @ExcelProperty(index = 23)
    private String QDKZBZ; // 渠道控制标志
    @ExcelProperty(index = 24)
    private String RYKZBZ; // 冗余控制标志
    @ExcelProperty(index = 25)
    private String QNGSFS; // 清算方式
    @ExcelProperty(index = 26)
    private String KFJZBZ; // 可否记帐
    @ExcelProperty(index = 27)
    private String DFYWDH; // 对方业务代号


}
