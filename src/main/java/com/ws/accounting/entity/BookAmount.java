package com.ws.accounting.entity;

//子流水金额记账要素

import com.ws.accounting.base.EntityBase;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Data
@Table(name = "acc_book_amount")
public class BookAmount extends EntityBase {




    //标签
    private String bookTag;

    //值
    private String bookValue;


}
