package com.ws.accounting.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.ws.accounting.base.EntityBase;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "acc_subject_info")
public class SubjectInfo extends EntityBase {


    @ExcelProperty(index = 0)
    private String level1Subject;
    @ExcelProperty(index = 1)
    private String level1SubjectInfo;
    @ExcelProperty(index = 2)
    private String level2Subject;
    @ExcelProperty(index = 3)
    private String level2SubjectInfo;
    @ExcelProperty(index = 4)
    private String businessCode;
    @ExcelProperty(index = 5)
    private String glSubject;
    @ExcelProperty(index = 6)
    private String glSubjectInfo;
}
