package com.ws.accounting.entity;

import com.ws.accounting.base.EntityBase;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 交易主流水
 */
@Data
@Entity
@Table(name = "acc_ac_trn_info")
public class AcTrnInfo extends EntityBase {
   private String subbnkn;      // 法人行号
   private String datetrn;      // 交易日期
   private String datacc;       // 会计日期
   private String serseqn;      // 流水号
   private String brctran;      // 交易机构
   private String trancode;      // 交易码
   private String chnelno;      // 渠道
   private String frntdat;      // 发起方交易日期
   private String frntseq;      // 发起方流水号
   private String datoltr;      // 原交易日期
   private String seqoltr;      // 原交易流水号
   private String trandate;      // 主机交易日期
   private String transeq;      // 主机流水号
   private String module;       // 所属模块
   private String flgdeal;      // 处理标志
   private String flgtim;       // 定时/实时标志
   private String isclsal;      // 是否允许冲销
   private String stantyp;      // 交易类型
   private String flgcanl;      // 被冲销标志
   private String teltran;      // 交易柜员
  // private String ;           // 交易柜员流水号 TODO 字段名待确认
   private String telchk;       // 复核柜员
   private String brcmod;       // 维护机构
   private String datemod;      // 维护日期
   private String moddatetime;  // 维护时间
   private String updtime;      // 时间戳


}
