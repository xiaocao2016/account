package com.ws.accounting.entity;

import com.ws.accounting.base.EntityBase;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * 机构控制关系
 */
@Data
@Entity
@Table(name = "me_cr_brfd")
public class MeCrBrfd {

    /**
     * 编号
     */
    @Id
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @GeneratedValue(generator = "system-uuid")
    @Column(length = 50)
    private String id;


    private String brc;      // 机构编号
    private String lnktype;  // 机构关系种类
    private String brcadsc;  // 归属机构
    private String idxadsc;  // 归属机构备用账号索引
    private String idxbasc;  // 本机构备用账号索引
    private String remark;   // 备注
    private String stsrcd;   // 记录状态

}
