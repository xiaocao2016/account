package com.ws.accounting.entity;

import com.ws.accounting.base.EntityBase;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * ======================================================
 *
 * @System:[核算系统]
 * @Desc: [机构控制关系类/组件描述]
 * @author: [weibin.zhu]
 * @date: 2021-02-11 8:43
 * @copyright: [Pactera]
 * ========================================================
 */
@Data
@Entity
@Table(name = "brc_ctrl_ship")
public class BrcCtrlShip extends EntityBase {


    /**
     * 机构编号
     */
    private String brc;
    /**
     * 机构关系种类
     */
    private String lnktype;
    /**
     * 归属机构
     */
    private String brcadsc;
    /**
     * 归属机构备用账号索引
     */
    private String idxadsc;
    /**
     * 本机构备用账号索引
     */
    private String idxbasc;
    /**
     * 生效日期
     */
    private String dateval;
    /**
     * 备注
     */
    private String remark;
    /**
     * 记录状态
     */
    private String stsrcd;
    /**
     * 记录状态
     */
    private String strbak;









}
