package com.ws.accounting.entity;

import com.ws.accounting.base.EntityBase;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;


/**
 * 内部分户
 */
@Data
@Table(name = "ia_acc_info")
@Entity
public class IaAccInfo extends EntityBase {

    private String subbnkn;           // 法人行号
    private String acctype;           // 账别
    private String acctyp;           // 账户类型
    private String accsubtype;       // 账户子类型
    private String brc;              // 机构编号
    private String busnoa;           // 业务代号
    private String ccy;              // 币种
    private String sernob;           // 内部账顺序号
    private String accno;             // 账号
    private String accname;           // 户名
    private String flgsiso;           // 表内或表外标志
    private String isflgst;           // 是否为标准户
    private String iscashac;           // 是否头寸账户
    private String datlast;           // 上笔交易日
    private String sbal;             // 上笔余额
    private String sbald;             // 上笔余额方向
    private String balcur;           // 当前余额
    private String bald;             // 余额方向
    private String typubal;           // 余额修改方式
    private String amtmin;           // 最低金额
    private String amtmax;           // 最高金额
    private String amtcldr;          // 发生额控制方向
    private String balctrl;          // 余额控制方向
    private String isovd;            // 是否允许透支
    private String amtlmto;          // 透支限额
    private String intflgb;          // 计息标志
    private String rateint;          // 利率
    private String intbday;          // 计息天数方式
    private String perioda;          // 计息周期
    private String datintl;          // 上次计息日
    private String daynextint;       // 下次计息日
    private String flgpre;           // 计提标志
    private String percyc;           // 计提周期
    private String lastdate;         // 上次计提日
    private String nextaccrualdate;  // 下次计提日
    private String cyceaci;          // 结息周期
    private String datlsti;          // 上次结息日
    private String datnxti;          // 下次结息日
    private String receivableint;    // 应收利息
    private String amtints;          // 应付利息
    private String isalws;           // 是否允许上级机构操作
    private String flgrpc;           // 收付现标志
    private String flglist;          // 汇总入明细标志
    private String typctl;           // 控制方式
    private String balfry;           // 余额频度检查
    private String brcbld;           // 开户机构
    private String datebld;          // 开户日期
    private String telbld;           // 开户柜员
    private String brcclo;           // 销户机构
    private String dateclo;          // 销户日期
    private String telclo;           // 销户柜员
    private String brcprf;           // 核算机构
    private String datebr;           // 重启日期
    private String telbldr;          // 重启柜员
    private String remark;           // 备注
    private String stsacc;           // 账户状态

}
