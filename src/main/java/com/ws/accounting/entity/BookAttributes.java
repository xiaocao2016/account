package com.ws.accounting.entity;

import com.ws.accounting.base.EntityBase;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;


//子流水产品属性
@Data
@Entity
@Table(name = "acc_book_attributes")
public class BookAttributes extends EntityBase {


    //子流水ID
    private String  bookId;

    //标签
    private String bookTag;

    //值
    private String bookValue;
}
