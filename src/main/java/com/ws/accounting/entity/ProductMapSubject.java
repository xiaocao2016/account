package com.ws.accounting.entity;

import com.ws.accounting.base.EntityBase;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 核算产品科目映射表
 */
@Data
@Entity
@Table(name="acc_product_subject")
public class ProductMapSubject extends EntityBase {

    //核算产品号
    private String productCode;

    //核算产品属性编号
    private String elementCode;



    //记账要素标识
    private String bookElementTag;

    //记账要素名称
    private String bookElementName;

    //配置描述
    private String ProductDesc;

    //序号
    private String serial;

    //业务代号
    private String busnoa;


}
