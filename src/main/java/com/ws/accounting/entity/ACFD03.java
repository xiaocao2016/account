package com.ws.accounting.entity;


import com.ws.accounting.base.EntityBase;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
* @Desc 公共字典表 数据库对象<br>
* @author www.pactera-fintech.com
* @date 2021-02-20 08:38:47
*/
@Data
@Entity
@Table(name = "acfd03")
public class ACFD03 extends EntityBase {

private static final long serialVersionUID = 1L;

    /** 银行号 */
    private String bankno;
    /** 字典类型 */
    private String dicttyp;
    /** 所属子系统 */
    private String subsys;
    /** 字典代码 */
    private String dictcode;
    /** 字典中文名称 */
    private String dictchname;
    /** 保留字段1 */
    private String fldrsv1;
    /** 保留字段2 */
    private String fldrsv2;
    /** 保留字段3 */
    private String fldrsv3;
    /** 保留字段4 */
    private String fldrsv4;
    /** 保留字段5 */
    private String fldrsv5;
    /** 保留字段6 */
    private String fldrsv6;
    /** 记录状态 */
    private String stsrcd;

}
