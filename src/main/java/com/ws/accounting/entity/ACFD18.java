package com.ws.accounting.entity;


import com.ws.accounting.base.EntityBase;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
* @Desc 系统参数 数据库对象<br>
* @author www.pactera-fintech.com
* @date 2021-02-10 09:14:25
*/
@Data
@Entity
@Table(name = "acfd18")
public class ACFD18 extends EntityBase {



    /** 银行号 */
    private String bankid;
    /** 会计核算日期 */
    private String accdate;
    /** 上日会计日期 */
    private String lstaccdate;
    /** 下一会计日期 */
    private String nxtaccdate;
    /** 状态A */
    private String parmsts;
    /** 保留字段1 */
    private String fldrsv1;
    /** 保留字段2 */
    private String fldrsv2;
    /** 保留字段3 */
    private String fldrsv3;
    /** 保留字段4 */
    private String fldrsv4;
    /** 保留字段5 */
    private String fldrsv5;
    /** 保留字段6 */
    private String fldrsv6;
    /** 记录状态 */
    private String stsrcd;

}
