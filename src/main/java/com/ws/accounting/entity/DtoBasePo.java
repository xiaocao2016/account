package com.ws.accounting.entity;


import com.ws.accounting.base.EntityBase;

import java.sql.Timestamp;
import java.util.List;

/**
 * ======================================================
 *
 * @System:[核算系统]
 * @Desc: [基础实体类类/组件描述]
 * @author: [weibin.zhu]
 * @date: 2021-02-15 11:04
 * @copyright: [Pactera]
 * ========================================================
 */

public class DtoBasePo extends EntityBase {
    private static final long serialVersionUID = 1L;
    protected Timestamp crttime;
    protected Timestamp updtime;
    protected String uuid;
    protected String systemid;
    protected String modteller;
    protected String modbrc;
    protected String moddate;
    protected List<String> keys;

    public DtoBasePo() {
    }

    public Timestamp getCrttime() {
        return this.crttime;
    }

    public void setCrttime(Timestamp crttime) {
        this.crttime = crttime;
    }

    public Timestamp getUpdtime() {
        return this.updtime;
    }

    public void setUpdtime(Timestamp updtime) {
        this.updtime = updtime;
    }

    public String getUuid() {
        return this.uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getSystemid() {
        return this.systemid;
    }

    public void setSystemid(String systemid) {
        this.systemid = systemid;
    }

    public List<String> getKeys() {
        return this.keys;
    }

    public void setKeys(List<String> keys) {
        this.keys = keys;
    }

    public String getModteller() {
        return this.modteller;
    }

    public void setModteller(String modteller) {
        this.modteller = modteller;
    }

    public String getModbrc() {
        return this.modbrc;
    }

    public void setModbrc(String modbrc) {
        this.modbrc = modbrc;
    }

    public String getModdate() {
        return this.moddate;
    }

    public void setModdate(String moddate) {
        this.moddate = moddate;
    }
}

