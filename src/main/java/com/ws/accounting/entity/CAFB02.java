package com.ws.accounting.entity;

import com.ws.accounting.base.EntityBase;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
* @Desc 清算路径表 数据库对象<br>
* @author www.pactera-fintech.com
* @date 2021-02-04 17:25:13
*/
@Data
@Entity
@Table(name = "CAFB02")
public class CAFB02 extends EntityBase {


    /** 银行号 */
    private String bankid;
    /** 账务机构 */
    private String finorg;
    /** 币种 */
    private String ccy;
    /** 多级清算记账科目号 */
    private String mliqsub;
    /** 多级清算上级记账科目 */
    private String mliqsupsub;
    /** 0级清算记账科目号 */
    private String zliqsub;
    /** 0级清算上级记账科目 */
    private String zliqsupsub;
    /** 强拆记账科目 */
    private String splisub;
    /** 强拆上级科目 */
    private String splipsub;
    /** 保留字段1 */
    private String fldrsv1;
    /** 保留字段2 */
    private String fldrsv2;
    /** 保留字段3 */
    private String fldrsv3;
    /** 保留字段4 */
    private String fldrsv4;
    /** 保留字段5 */
    private String fldrsv5;
    /** 保留字段6 */
    private String fldrsv6;
    /** 记录状态 */
    private String rcdsts;


}
