package com.ws.accounting.entity;


import com.ws.accounting.base.EntityBase;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
* @Desc 外部产品与核算产品映射表 数据库对象<br>
* @author www.pactera-fintech.com
* @date 2021-02-19 17:40:33
*/
@Data
@Entity
@Table(name = "acfd05")
public class ACFD05 extends EntityBase {

private static final long serialVersionUID = 1L;

    /** 银行号 */
    private String bankno;
    /** 账薄类别 */
    private String accbooktyp;
    /** 外部产品编号1 */
    private String extprdno;
    /** 外部产品名称1 */
    private String extprdnm;
    /** 核算产品编号A */
    private String accprdno;
    /** 核算产品名称1 */
    private String accprdnm;
    /** 保留字段1 */
    private String fldrsv1;
    /** 保留字段2 */
    private String fldrsv2;
    /** 保留字段3 */
    private String fldrsv3;
    /** 保留字段4 */
    private String fldrsv4;
    /** 保留字段5 */
    private String fldrsv5;
    /** 保留字段6 */
    private String fldrsv6;
    /** 记录状态 */
    private String stsrcd;

}
