package com.ws.accounting.entity;
import com.ws.accounting.base.EntityBase;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;


/**
* @Desc 核心交易清算方式配置表 数据库对象<br>
* @author www.pactera-fintech.com
* @date 2021-02-22 09:52:22
*/
@Data
@Entity
@Table(name = "acfb08")
public class ACFB08 extends EntityBase {



    /** 银行号 */
    private String bankno;
    /** 交易码1 */
    private String trancode;
    /** 核心交易名称 */
    private String tranname;
    /** 交易清算标志 */
    private String trnclstflg;
    /** 生效状态 */
    private String stsval;

    /** 保留字段1 */
    private String fldrsv1;
    /** 保留字段2 */
    private String fldrsv2;
    /** 保留字段3 */
    private String fldrsv3;
    /** 保留字段4 */
    private String fldrsv4;
    /** 保留字段5 */
    private String fldrsv5;
    /** 保留字段6 */
    private String fldrsv6;
    /** 记录状态 */
    private String stsrcd;



}
