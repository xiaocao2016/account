package com.ws.accounting.entity;


import com.ws.accounting.base.EntityBase;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
* @Desc 核算产品与业务代号映射表 数据库对象<br>
* @author www.pactera-fintech.com
* @date 2021-02-19 17:03:33
*/
@Data
@Entity
@Table(name = "acfb07")
public class ACFB07 extends EntityBase {


    /** 银行号 */
    private String bankno;
    /** 账薄类别 */
    private String accbooktyp;
    /** 核算产品编号A */
    private String accprdno;
    /** 核算产品属性编号 */
    private String accpropno;
    /** 核算序号 */
    private String accseqno;
    /** 记账要素代码 */
    private String accfaccode;
    /** 记账要素中文名称 */
    private String accfacchnm;
    /** 说明 */
    private String des;
    /** 业务代号 */
    private String busno;
    /** 本金科目标志 */
    private String flgprinsub;
    /** 保留字段1 */
    private String fldrsv1;
    /** 保留字段2 */
    private String fldrsv2;
    /** 保留字段3 */
    private String fldrsv3;
    /** 保留字段4 */
    private String fldrsv4;
    /** 保留字段5 */
    private String fldrsv5;
    /** 保留字段6 */
    private String fldrsv6;
    /** 记录状态 */
    private String stsrcd;



}
