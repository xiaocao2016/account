package com.ws.accounting.entity;

import com.ws.accounting.base.EntityBase;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 核算产品映射表
 */
@Data
@Entity
@Table(name = "acc_prdrule_def")
public class PrdruleDef extends EntityBase {


    //外部产品编码
    private String productCode;

    //外部产品名称
    private String productName;

    //核算产品编码
    private String prdruleCode;

    //核算产品名称
    private String prdruleName;
}
