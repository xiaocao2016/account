package com.ws.accounting.entity;


    import com.ws.accounting.base.EntityBase;
    import lombok.Data;

    import javax.persistence.Entity;
    import javax.persistence.Table;
    import java.math.BigDecimal;

/**
* @Desc 会计主流水表 数据库对象<br>
* @author www.pactera-fintech.com
* @date 2021-02-23 10:43:05
*/
@Data
@Entity
@Table(name = "acfm15")
public class ACFM15 extends EntityBase {

private static final long serialVersionUID = 1L;

    /** 银行号 */
    private String bankno;
    /** 账薄类别 */
    private String accbooktyp;
    /** 核心交易代码 */
    private String kltrncode;
    /** 会计日期 */
    private String dtacc;
    /** 会计交易流水号 */
    private String actrnseqno;
    /** 会计交易日期 */
    private String dtacctrn;
    /** 核心交易日期 */
    private String dtkltrn;
    /** 核心交易流水号 */
    private String kltrnseqno;
    /** 渠道号 */
    private String channelno;
    /** 适用系统 */
    private String subsys;
    /** 发起方交易日期2 */
    private String dtfrnt;
    /** 发起方流水号A */
    private String frntseqno;
    /** 交易金额 */
    private BigDecimal amttran;
    /** 原核算会计日期 */
    private String dtoriacc;
    /** 原会计核算流水号 */
    private String oaccseqno;
    /** 交易接口类型 */
    private String trnintftyp;
    /** 交易方式 */
    private String tranmod;
    /** 核算处理标志 */
    private String flgaccdeal;
    /** 交易流水类型 */
    private String jouracctyp;
    /** 被冲销标志1 */
    private String flgbrevcls;
    /** 组合服务标志 */
    private String flgcomsrv;
    /** 交易时间 */
    private String trantime;
    /** 交易机构 */
    private String brc;
    /** 交易柜员1 */
    private String teltran;
    /** 复核员 */
    private String telchk;
    /** 授权柜员 */
    private String telauth;
    /** 记录状态 */
    private String stsrcd;
    /** 保留字段1 */
    private String fldrsv1;
    /** 保留字段2 */
    private String fldrsv2;
    /** 保留字段3 */
    private String fldrsv3;
    /** 保留字段4 */
    private String fldrsv4;
    /** 保留字段5 */
    private String fldrsv5;
    /** 保留字段6 */
    private String fldrsv6;



}
