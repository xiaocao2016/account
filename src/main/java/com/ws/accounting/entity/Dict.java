package com.ws.accounting.entity;

import com.ws.accounting.base.EntityBase;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.List;

@Data
@Entity
@Table(name = "human_dict")
public class Dict extends EntityBase implements Comparable<Dict> {


    /**
     * 前端树形节点使用
     */
    @Transient
    private String title;
    /**
     * 前端树形节点使用
     */
    @Transient
    private boolean expand;

    @Transient
    private String label;

    @Transient
    private String value;


    private int level;

    /**
     * 名称  本级不能重名
     */
    private String name;

    /**
     * 全称
     */
    @Column(length = 500)
    private String fullName;

    /**
     * 上级编号
     */
    @Column(length = 50)
    private String parentId;

    /**
     * 是否上级
     */
    private boolean isParent;

    /**
     * 自定义属性
     */
    private String attributes;

//    /**
//     * 下级
//     */
//    @Transient
//    private List<Dictionary> child;
//
//    @Transient
//    private List<Dictionary> children;
    /**
     * 排序号
     */
    @Column(name = "_rank")
    private int rank;

    /**
     * 顶级名称
     */
    private String topName;

    @Override
    public int compareTo(Dict o) {
        return Integer.compare(this.rank,o.rank);
    }
}
