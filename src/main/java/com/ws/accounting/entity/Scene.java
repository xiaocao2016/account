package com.ws.accounting.entity;

import com.ws.accounting.base.EntityBase;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "account_scene")
public class Scene extends EntityBase {

    //事件编码
    private String sceneCode;
    //事件描述
    private String sceneName;
}
