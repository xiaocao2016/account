package com.ws.accounting.entity;

import com.ws.accounting.base.EntityBase;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 传票
 */

@Data
@Entity
@Table(name = "acc_detail")
public class AcAccDetail extends EntityBase {

    private String subbnkn;   // 法人行号
    private String datacc;    // 会计日期
    private String serseqn;   // 流水号
    private String datetrn;   // 交易日期
    private String trancode;  // 交易码
    private String seq;       // 序号
    private String brctran;   // 交易机构
    private String brcprf;    // 核算机构
    private String acctype;   // 账别
    private String subno;     // 科目号
    private String busnoa;    // 业务代号
    private String sernob;    // 内部账顺序号
    private String accno;     // 账号
    private String custid;    // 客户号
    private String accsub;    // 产品账号
    private String acbnod;    // 账簿项编号
    private String scene;     // 业务场景
    private String subplat;   // 所属系统
    private String ccy;       // 币种
    private String flgdc;     // 借贷标志
    private String accenmd;   // 记账方式
    private String amttran;   // 交易金额
    private String prdcode;   // 产品代码
    private String flgcr;     // 钞汇标志
    private String flgsiso;   // 表内或表外标志
    private String typacc;    // 科目类别
    private String vouno;     // 凭证号
    private String voutyp;    // 凭证种类
    private String debtno;    // 借据编号
    private String cashid;    // 现金项目代码
    private String flgct;     // 现转标志
    private String frntdat;   // 发起方交易日期
    private String frntseq;   // 发起方流水号
    private String trncodb;   // 发起方交易码
    private String chanel;    // 发起方渠道代码
    private String isclsal;   // 是否允许冲销
    private String stantyp;   // 流水账类型
    private String flgcanl;   // 被冲销标志
    private String seqnor;    // 冲销流水号
    private String memo;      // 摘要
    private String memono;    // 摘要码
    private String teltran;   // 交易柜员
    private String telauth;   // 授权柜员
    private String flginac;   // 入账标志
    private String timtra;    // 交易时间
    private String accnoa;    // 对方账号
    private String accnama;   // 对方户名
    private String brcoter;   // 对方行号
    private String brcbcdb;   // 对方行名
    private String ccyopp;    // 对方币种
    private String flgcrt;    // 对方钞汇标识
    private String flgclrs;   // 特殊清算标志
    private String flgdeals;  // 特殊处理标志
    private String qscpbz;    // 清算传票标志
    private String trandate;  // 主机交易日期
    private String transeq;   // 主机流水号
    private String module;    // 所属模块

}
