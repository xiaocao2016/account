package com.ws.accounting.entity;

import com.ws.accounting.base.EntityBase;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Table(name = "ac_clear_path")
@Entity
public class AcClearPath extends EntityBase {

    /**
     * 金融许可证号码
     */
    private String finpeno;
    /**
     * 法人行号
     */
    private String subbnkn;
    /**
     * 财务机构：默认9999，代表全行
     */
    private String institution="9999";
    /**
     * 币种
     */
    private String ccy;
    /**
     * 多级清算-记账科目号
     */
    private String ml_account_no;
    /**
     * 多级清算-上级记账科目号
     */
    private String mlp_account_no;
    /**
     * 零级清算-记账科目号
     */
    private String zl_account_no;
    /**
     * 零级清算-上级记账科目号
     */
    private String zlp_account_no;
    /**
     * 强拆科目号
     */
    private String s_account_no;
    /**
     * 强拆上级科目号
     */
    private String sp_account_no;

    /**
     * 记录状态
     */
    private String stsrcd;

}
