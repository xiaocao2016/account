package com.ws.accounting.entity;


import com.ws.accounting.base.EntityBase;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
* @Desc 核算产品属性表 数据库对象<br>
* @author www.pactera-fintech.com
* @date 2021-02-19 17:21:34
*/
@Data
@Entity
@Table(name = "acfm08")
public class ACFM08 extends EntityBase {



    /** 银行号 */
    private String bankno;
    /** 账薄类别 */
    private String accbooktyp;
    /** 登记序号 */
    private String regseq;
    /** 核算产品编号A */
    private String accprdno;
    /** 产品属性代码 */
    private String prdpropcod;
    /** 产品属性中文名称 */
    private String prdpropdes;
    /** 保留字段1 */
    private String fldrsv1;
    /** 保留字段2 */
    private String fldrsv2;
    /** 保留字段3 */
    private String fldrsv3;
    /** 保留字段4 */
    private String fldrsv4;
    /** 保留字段5 */
    private String fldrsv5;
    /** 保留字段6 */
    private String fldrsv6;
    /** 记录状态 */
    private String stsrcd;


}
