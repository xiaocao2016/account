package com.ws.accounting.entity;

import com.ws.accounting.base.EntityBase;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
* @Desc 会计子流水辅助表 数据库对象<br>
* @author www.pactera-fintech.com
* @date 2021-02-23 10:34:05
*/
@Data
@Entity
@Table(name = "acfe17")
public class ACFE17 extends EntityBase {


    /** 银行号 */
    private String bankno;
    /** 账薄类别 */
    private String accbooktyp;
    /** 核心交易日期 */
    private String dtkltrn;
    /** 核心交易流水号 */
    private String kltrnseqno;
    /** 核心登记序号 */
    private String klregseq;
    /** 产品金额标志 */
    private String flgprdamt;
    /** 属性描述 */
    private String propdesc;
    /** 产品金额属性值 */
    private String propval;
    /** 记录状态 */
    private String stsrcd;
    /** 保留字段1 */
    private String fldrsv1;
    /** 保留字段2 */
    private String fldrsv2;
    /** 保留字段3 */
    private String fldrsv3;
    /** 保留字段4 */
    private String fldrsv4;
    /** 保留字段5 */
    private String fldrsv5;
    /** 保留字段6 */
    private String fldrsv6;

}
