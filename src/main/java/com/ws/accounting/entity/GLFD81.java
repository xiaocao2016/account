package com.ws.accounting.entity;


import com.ws.accounting.base.EntityBase;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

/**
* @Desc 会计科目定义 数据库对象<br>
* @author www.pactera-fintech.com
* @date 2021-01-27 20:55:07
*/
@Data
@Entity
@Table(name = "GLFD81")
public class GLFD81 extends EntityBase {

private static final long serialVersionUID = 1L;

    private String bankid       ;
    private String accbooktyp   ;
    private String subno        ;
    private String subnonm      ;
    private String busnoa       ;
    private String subnoa       ;
    private String sublvl       ;
    private String typacc       ;
    private String subnos       ;
    private String issubnc      ;
    private String flglstb      ;
    private String issubac      ;
    private String amtctld      ;
    private String sisoflg      ;
    private String balctld      ;
    private String relopenflg   ;
    private String oppbusno     ;
    private String reloseqflg   ;
    private String conadjvflg   ;
    private String badupdmod    ;
    private String appheadflg   ;
    private String appbrcflg    ;
    private String appsubflg    ;
    private String applocflg    ;
    private String dhalwflg     ;
    private String chalwflg     ;
    private String hopenflg     ;
    private String lmtchkflg    ;
    private String minamt       ;
    private String maxmat       ;
    private String isovdflg     ;
    private String ovdlmtamt    ;
    private String alwsupoflg   ;
    private String intflg       ;
    private String ratecode     ;
    private String intcyc       ;
    private String intdaymod    ;
    private String ymrateflg    ;
    private String accrualflg   ;
    private String accruedcyc   ;
    private String setintcyc    ;
    private String iscashdraw   ;
    private String autoclstyp   ;
    private Date valdate        ;
    private Date expidate       ;
    private String fldrsv1      ;
    private String fldrsv2      ;
    private String fldrsv3      ;
    private String fldrsv4      ;
    private String fldrsv5      ;
    private String fldrsv6      ;
    private String rcdsts       ;

}
