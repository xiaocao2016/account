package com.ws.accounting.entity;


import com.ws.accounting.base.EntityBase;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
* @Desc 传票解析规则配置 数据库对象<br>
* @author www.pactera-fintech.com
* @date 2021-02-23 20:25:41
*/
@Data
@Entity
@Table(name = "acfb06")
public class ACFB06 extends EntityBase {


    /** 银行号 */
    private String bankno;
    /** 账薄类别 */
    private String accbooktyp;
    /** 交易事件代码 */
    private String tranevntno;
    /** 交易事件名称 */
    private String tranevntnm;
    /** 核算产品编号A */
    private String accprdno;
    /** 记账要素代码 */
    private String accfaccode;
    /** 记账要素中文名称 */
    private String accfacchnm;
    /** 借贷标志 */
    private String flgdc;
    /** 核算记账方式 */
    private String accentmod;
    /** 科目来源1 */
    private String subsrctyp;
    /** 业务代号 */
    private String busno;
    /** 核算账号序号 */
    private String accnoseq;
    /** 规则序号 */
    private String ruleseq;
    /** 机构来源标志 */
    private String flgbrcsrc;
    /** 机构号标识1 */
    private String brciden;
    /** 核算账户机构 */
    private String brcacct;
    /** 金额表达式1 */
    private String amtexpform;
    /** 价税分离标志 */
    private String flgptaxsep;
    /** 记录状态 */
    private String stsrcd;
    /** 保留字段1 */
    private String fldrsv1;
    /** 保留字段2 */
    private String fldrsv2;
    /** 保留字段3 */
    private String fldrsv3;
    /** 保留字段4 */
    private String fldrsv4;
    /** 保留字段5 */
    private String fldrsv5;
    /** 保留字段6 */
    private String fldrsv6;


}
