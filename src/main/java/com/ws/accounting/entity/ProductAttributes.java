package com.ws.accounting.entity;

import com.ws.accounting.base.EntityBase;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 核算产品属性表
 */
@Data
@Entity
@Table(name = "acc_product_attributes")
public class ProductAttributes extends EntityBase {

    //核算产品码
    private String productCode;

    //序号
    private String productNumber;

    //说明
    private String productDesc;

    //核算规则标签
    private String productLabel;
}
