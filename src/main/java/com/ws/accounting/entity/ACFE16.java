package com.ws.accounting.entity;


import com.ws.accounting.base.EntityBase;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

/**
* @Desc 会计子流水 数据库对象<br>
* @author www.pactera-fintech.com
* @date 2021-02-23 10:28:05
*/
@Data
@Entity
@Table(name = "acfe16")
public class ACFE16 extends EntityBase {



    /** 银行号 */
    private String bankno;
    /** 账薄类别 */
    private String accbooktyp;
    /** 所属子系统 */
    private String subsys;
    /** 归属机构 */
    private String brcadsc;
    /** 会计日期 */
    private String dtacc;
    /** 核心交易日期 */
    private String dtkltrn;
    /** 会计交易流水号 */
    private String actrnseqno;
    /** 核心登记序号 */
    private String klregseq;
    /** 核心交易流水号 */
    private String kltrnseqno;
    /** 核算记分户标志 */
    private String flgentsub;
    /** 产品编号 */
    private String prdno;
    /** 交易事件代码 */
    private String tranevntno;
    /** 币种 */
    private String ccy;
    /** 钞汇标志 */
    private String flgcr;
    /** 核算现转标志 */
    private String flgct;
    /** 现金项目代码A */
    private String cashid;
    /** 交易金额 */
    private BigDecimal amttran;
    /** 客户号 */
    private String custno;
    /** 账号 */
    private String accno;
    /** 产品账号 */
    private String prdaccno;
    /** 产品账号户名 */
    private String prdaccname;
    /** 借据编号 */
    private String debitno;
    /** 核算账簿编号 */
    private String accbookno;
    /** 凭证种类 */
    private String voutyp;
    /** 凭证号 */
    private String vouno;
    /** 待销账编号 */
    private String acchngno;
    /** 待销账种类 */
    private String hngacctyp;
    /** 待销账标志 */
    private String flghngop;
    /** 挂账到期日 */
    private String dthngdue;
    /** 挂账金额 */
    private BigDecimal amthng;
    /** 销账金额 */
    private BigDecimal amtcan;
    /** 对方账号 */
    private String oppaccno;
    /** 对方户名 */
    private String oppaccnm;
    /** 对手行行号 */
    private String oppbankno;
    /** 对方行名 */
    private String oppbanknm;
    /** 对方币种 */
    private String oppccy;
    /** 对方钞汇标志 */
    private String flgoppcr;
    /** 执行利率 */
    private BigDecimal execrate;
    /** 利率 */
    private BigDecimal intrate;
    /** 特殊清算标志1 */
    private String flgspecls;
    /** 特殊处理标志 */
    private String flgspedeal;
    /** 小微企业标志1 */
    private String flgsme;
    /** 小额贷款标志1 */
    private String flgpettyln;
    /** 摘要码 */
    private String memono;
    /** 摘要 */
    private String memo;
    /** 备注 */
    private String remark;
    /** 记录状态 */
    private String stsrcd;
    /** 保留字段1 */
    private String fldrsv1;
    /** 保留字段2 */
    private String fldrsv2;
    /** 保留字段3 */
    private String fldrsv3;
    /** 保留字段4 */
    private String fldrsv4;
    /** 保留字段5 */
    private String fldrsv5;
    /** 保留字段6 */
    private String fldrsv6;



}
