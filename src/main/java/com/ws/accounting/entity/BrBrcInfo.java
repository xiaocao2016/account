package com.ws.accounting.entity;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 机构信息表
 */
@Data
@Entity
@Table(name = "br_brc_info")
public class BrBrcInfo  implements Serializable {

    /**
     * 编号
     */
    @Id
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @GeneratedValue(generator = "system-uuid")
    @Column(length = 50)
    private String id;

    @Column(length = 50)
    private String bankid;      // 银行号

    @Column(length = 50)
    private String brc;         // 机构编号

    @Column(length = 50)
    private String subbnkn;      // 法人行号

    @Column(length = 50)
    private String brcname;      // 机构名称

    @Column(length = 50)
    private String brcnams;      // 机构简称

    @Column(length = 50)
    private String brcen;       // 机构英文名称

    @Column(length = 50)
    private String ensname;      // 机构英文简称

    @Column(length = 50)
    private String brclvl;      // 机构级别

    @Column(length = 50)
    private String brcattr;      // 机构类型

    @Column(length = 50)
    private String brcnat;      // 机构属性

    @Column(length = 50)
    private String taxattr;      // 纳税属性

    @Column(length = 50)
    private String taxmod;      // 计税方法

    @Column(length = 50)
    private String isslbal;      // 是否独立核算

    @Column(length = 50)
    private String brcprf;      // 核算机构

    @Column(length = 50)
    private String datlbsn;    // 上次营业日期

    @Column(length = 50)
    private String stsbusi;    //当前营业状态

    @Column(length = 50)
    private String stsbrc;     //当前机构状态

    @Column(length = 50)
    private String isprtnt;    //是否打印票据

    @Column(length = 50)
    private String iscash;    //是否中心金库

    @Column(length = 50)
    private String isinvas;    //是否缴存现金

    @Column(length = 50)
    private String isdcblc;    //是否检查总账借贷平衡

    @Column(length = 50)
    private String isnblc;    //总分不平是否自动处理

    @Column(length = 50)
    private String ismics;    //中间业务对账是否完成

    @Column(length = 50)
    private String isrdrlv;    //是否调整存款变动利率

    @Column(length = 50)
    private String isadbrc;    //是否24小时机构

    @Column(length = 50)
    private String accsyid;    //账务体系代码

    @Column(length = 50)
    private String swiftid;    //SWIFT代码
    private String arecod;    //机构所属地区
    private String citycde;    //城市代码
    private String corname;    //地区名称
    private String addr;       //地址
    private String enaddr;     //英文详细地址
    private String postcod;    //邮政编码
    private String cnttel;     //联系电话
    private String aracode;    //地区码
    private String datesta;    //启用日期
    private String addrip;     //IP地址
    private String brcprfr;    //参考核算机构
    private String faxno;      //传真号码
    private String citynam;    //城市名称
    private String iscubak;    //村镇银行标志
    private String frntbrc;    //发起行机构编号
    private String finpeno;    //金融许可证号
    private String buslino;    //营业执照号码
    private String nxysyno;    //农信银系统行号
    private String modpyno;    //现代支付系统行号
    private String cntnam;     //联系人姓名
    private String stan1;      //备用1

    /**
     * 创建时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private String crttime;    //

    /**
     * 修改时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private String updtime;    //
    private String modbrc;      //
    private String moddate;     //
    private String modteller;    //

}
