package com.ws.accounting.entity;


import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
* @Desc 事件定义表 数据库对象<br>
* @author www.pactera-fintech.com
* @date 2021-02-20 08:58:48
*/
@Data
@Entity
@Table(name = "acfb19")
public class ACFB19 extends DtoBasePo {



    /** 银行号 */
    private String bankno;
    /** 交易事件码 */
    private String tranentno;
    /** 交易事件名称 */
    private String tranevntnm;
    /** 所属系统 */
    private String subplat;
    /** 保留字段1 */
    private String fldrsv1;
    /** 保留字段2 */
    private String fldrsv2;
    /** 保留字段3 */
    private String fldrsv3;
    /** 保留字段4 */
    private String fldrsv4;
    /** 保留字段5 */
    private String fldrsv5;
    /** 保留字段6 */
    private String fldrsv6;
    /** 记录状态 */
    private String stsrcd;


}
