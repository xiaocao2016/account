package com.ws.accounting.entity;

import com.ws.accounting.base.EntityBase;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

/**
* @Desc 传票表 数据库对象<br>
* @author www.pactera-fintech.com
* @date 2021-02-15 15:35:52
*/
@Data
@Entity
@Table(name = "ACFM18")
public class ACFM18 extends EntityBase {



    /** 银行号 */
    private String bankid;
    /** 会计日期 */
    private String accdate;
    /** 交易流水号/柜员流水号/主机流水号 */
    private String accseqno;
    /** 子流水序号 */
    private Integer subseqno;
    /** 传票序号 */
    private Integer bookseq;
    /** 交易码 */
    private String trancode;
    /** 交易码 */
    private String tranname;
    /** 交易机构 */
    private String brctran;
    /** 核算机构 */
    private String brcprf;
    /** 账别 */
    private String accbooktyp;
    /** 交易事件码 */
    private String tranentno;
    /** 交易事件名称 */
    private String tranevntnm;
    /** 科目类别 */
    private String subnotyp;
    /** */
    private String subno;
    /** */
    private String busnoa;
    /** */
    private String sisoflg;
    /** */
    private String issubac;
    /** */
    private String seqno;
    /** */
    private String accno;
    /** */
    private String custno;
    /** */
    private String prdcode;
    /** */
    private String prdaccno;
    /** */
    private String prdaccname;
    /** */
    private String accbookno;
    /** */
    private String subsys;
    /** */
    private String dcflg;
    /** */
    private String accentflg;
    /** */
    private String ccy;
    /** */
    private String crflg;
    /** */
    private String amttran;
    /** */
    private String vouno;
    /** */
    private String voutyp;
    /** */
    private String debitno;
    /** */
    private String cashid;
    /** */
    private String cashtrsflg;
    /** */
    private String revclsflg;
    /** */
    private String jouracctyp;
    /** */
    private String brevclsflg;
    /** */
    private String revclsseq;
    /** */
    private String memo;
    /** */
    private String memono;
    /** */
    private String teltran;
    /** */
    private String telauth;
    /** */
    private Date datetrn;
    /** */
    private String trantime;
    /** */
    private String accnoa;
    /** */
    private String accnama;
    /** */
    private String banknoa;
    /** */
    private String banknamea;
    /** */
    private String ccya;
    /** */
    private String crtflga;
    /** */
    private String speclsflg;
    /** */
    private String spedealflg;
    /** */
    private String clsbookflg;
    /** */
    private String fldrsv1;
    /** */
    private String fldrsv2;
    /** */
    private String fldrsv3;
    /** */
    private String fldrsv4;
    /** */
    private String fldrsv5;
    /** */
    private String fldrsv6;
    /** */
    private String rcdsts;


}
