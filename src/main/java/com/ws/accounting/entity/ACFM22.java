package com.ws.accounting.entity;

import com.ws.accounting.base.EntityBase;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
* @Desc 机构信息表 数据库对象<br>
* @author www.pactera-fintech.com
* @date 2021-02-15 15:19:52
*/
@Data
@Entity
@Table(name = "ACFM22")
public class ACFM22 extends EntityBase {


    /** 银行号 */
    private String bankid;
    /** 机构编号 */
    private String brc;
    /** 分行代码 */
    private String branchcode;
    /** 机构名称 */
    private String brcname;
    /** 机构简称 */
    private String brcnams;
    /** 机构级别 */
    private String brclvl;
    /** 机构类型 */
    private String brcattr;
    /** 机构属性 */
    private String brcnat;
    /** 核算机构 */
    private String brcprf;
    /** 账号类型 */
    private String accttyp;
    /** 当前机构状态 */
    private String stsbrc;



}
