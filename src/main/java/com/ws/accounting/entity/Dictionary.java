package com.ws.accounting.entity;

import com.ws.accounting.base.EntityBase;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.List;

/**
 * 数据字典
 */
@Entity
@Table(name = "system_dictionary")
public class Dictionary extends EntityBase implements Comparable<Dictionary> {

    /**
     * 前端树形节点使用
     */
    @Transient
    private String title;
    /**
     * 前端树形节点使用
     */
    @Transient
    private boolean expand;

    @Transient
    private String label;

    @Transient
    private String value;



    /**
     * 名称  本级不能重名
     */
    private String name;

    /**
     * 全称
     */
    private String fullName;

    /**
     * 上级编号
     */
    @Column(length = 50)
    private String parentId;

    /**
     * 是否上级
     */
    private boolean isParent;

    /**
     * 自定义属性
     */
    private String attributes;

    /**
     * 下级
     */
    @Transient
    private List<Dictionary> child;

    @Transient
    private List<Dictionary> children;
    /**
     * 排序号
     */
    @Column(name = "_rank")
    private int rank;

    /**
     * 顶级名称
     */
    private String topName;


    public String getTitle() {
        return this.name;
    }

    public boolean getExpand() {
        return true;
    }

    /**
     * 获取 名称  本级不能重名
     *
     * @return name 名称  本级不能重名
     */
    public String getName() {
        return this.name;
    }

    /**
     * 设置 名称  本级不能重名
     *
     * @param name 名称  本级不能重名
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取 全称
     *
     * @return fullName 全称
     */
    public String getFullName() {
        return this.fullName;
    }

    /**
     * 设置 全称
     *
     * @param fullName 全称
     */
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    /**
     * 获取 上级编号
     *
     * @return parentId 上级编号
     */
    public String getParentId() {
        return this.parentId;
    }

    /**
     * 设置 上级编号
     *
     * @param parentId 上级编号
     */
    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    /**
     * 获取 是否上级
     *
     * @return isParent 是否上级
     */
    public boolean getIsParent() {
        return this.isParent;
    }

    /**
     * 设置 是否上级
     *
     * @param isParent 是否上级
     */
    public void setIsParent(boolean isParent) {
        this.isParent = isParent;
    }

    /**
     * 获取 自定义属性
     *
     * @return attributes 自定义属性
     */
    public String getAttributes() {
        return this.attributes;
    }

    /**
     * 设置 自定义属性
     *
     * @param attributes 自定义属性
     */
    public void setAttributes(String attributes) {
        this.attributes = attributes;
    }

    /**
     * 获取 下级
     *
     * @return child 下级
     */
    public List<Dictionary> getChild() {
        return this.child;
    }

    public List<Dictionary> getChildren() {
        return this.child;
    }

    /**
     * 设置 下级
     *
     * @param child 下级
     */
    public void setChild(List<Dictionary> child) {
        this.child = child;
    }

    /**
     * Gets the value of rank. $bare_field_name
     *
     * @return the value of rank
     */
    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public String getTopName() {
        return topName;
    }

    public void setTopName(String topName) {
        this.topName = topName;
    }


    public String getLabel() {
        return this.name;
    }

    public String getValue() {
        return this.getId();
    }

    @Override
    public int compareTo(Dictionary o) {
        return Integer.compare(this.rank,o.rank);
    }
}

