package com.ws.accounting.entity;

import java.io.Serializable;

/**
 * ======================================================
 *
 * @System:[核算系统]
 * @Desc: [基础排序类类/组件描述]
 * @author: [weibin.zhu]
 * @date: 2021-02-15 11:08
 * @copyright: [Pactera]
 * ========================================================
 */

public class Order implements Serializable {
    private static final long serialVersionUID = -9052322409478952879L;
    private String propertyName;
    private String columnName;
    private String sort;

    public static Order asc(String propertyName) {
        return new Order(propertyName, " ASC");
    }

    public static Order desc(String propertyName) {
        return new Order(propertyName, " DESC");
    }

    private Order(String propertyName, String sort) {
        this.propertyName = propertyName;
        this.columnName = propertyName;
        this.sort = sort;
    }

    public String getPropertyName() {
        return this.propertyName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getColumnName() {
        return this.columnName;
    }

    public String getSort() {
        return this.sort;
    }
}

