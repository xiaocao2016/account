package com.ws.accounting.entity;


import com.ws.accounting.base.EntityBase;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
* @Desc 核算引擎错误登记簿 数据库对象<br>
* @author www.pactera-fintech.com
* @date 2021-02-18 10:08:25
*/
@Data
@Entity
@Table(name = "acfe09")
public class ACFE09 extends EntityBase {

private static final long serialVersionUID = 1L;

    /** 核算错误登记码 */
    private String caerrcode;
    /** 核算错误登记信息 */
    private String caerrmsg;
    /** 核算错误信息类型 */
    private String caerrtype;
    /** 交易流水 */
    private String seqtran;
    /** 交易请求报文 */
    private String request;



}
