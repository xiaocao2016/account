package com.ws.accounting.entity;


import com.ws.accounting.base.EntityBase;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 核算规则表
 */
@Data
@Entity
@Table(name = "acc_rules")
public class Rules extends EntityBase {

    //事件代码
    private String scenseq;

    //事件名称
    private String busse;

    //核算产品号
    private String productCode;


    //解析记账要素
    private String elements;


    //借贷方向
    private String flgdc;


    //记账方式
    private String accenmd;


    //科目来源
    private String subsrc;

    //业务代号
    private String busnoa;


    //序号
    private String sernob;

    //机构号来源
    private String orgSource;

    //机构号标识
    private String orgFlag;

    //机构号
    private String orgId;

    //金额表达式
    private String amountExpression;


}
