package com.ws.accounting.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ======================================================
 *
 * @System:[核算系统]
 * @Desc: [基础po类/组件描述]
 * @author: [weibin.zhu]
 * @date: 2021-02-15 11:06
 * @copyright: [Pactera]
 * ========================================================
 */

public abstract class BasePo implements Serializable {
    private static final long serialVersionUID = 1406072669738004984L;
    private Long tendId;
    private List<Order> orderBy;
    private Map<String, Object> additionalParameter;

    public BasePo() {
    }

    public Long getTendId() {
        return this.tendId;
    }

    public void setTendId(Long tendId) {
        this.tendId = tendId;
    }

    public final List<Order> getOrderBy() {
        return this.orderBy;
    }

    final void setOrderBy(List<Order> orderBy) {
        this.orderBy = orderBy;
    }

    public void addOrder(Order order) {
        if (this.orderBy == null) {
            this.orderBy = new ArrayList();
        }

        this.orderBy.add(order);
    }

    public Map<String, Object> getAdditionalParameter() {
        return this.additionalParameter;
    }

    public void putAdditionalParameter(String key, Object parameterValue) {
        if (this.additionalParameter == null) {
            this.additionalParameter = new HashMap(1);
        }

        this.additionalParameter.put(key, parameterValue);
    }
}

