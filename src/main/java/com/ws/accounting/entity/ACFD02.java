package com.ws.accounting.entity;


import com.ws.accounting.base.EntityBase;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
* @Desc 通用参数表 数据库对象<br>
* @author www.pactera-fintech.com
* @date 2021-02-20 08:08:47
*/
@Data
@Entity
@Table(name = "acfd02")
public class ACFD02 extends EntityBase {


    /** 银行号 */
    private String bankno;
    /** 参数类型1 */
    private String parmtype;
    /** 参数代码1 */
    private String parmcode;
    /** 参数描述1 */
    private String parmdesc;
    /** 参数值1 */
    private String parmval1;
    /** 参数值2 */
    private String parmval2;
    /** 参数值3 */
    private String parmval3;
    /** 参数值4 */
    private String parmval4;
    /** 参数值5 */
    private String parmval5;
    /** 参数值6 */
    private String parmval6;
    /** 参数值7 */
    private String parmval7;
    /** 参数值8 */
    private String parmval8;
    /** 参数值9 */
    private String parmval9;
    /** 参数属性值10 */
    private String parmval10;
    /** 生效日期 */
    private String dateval;
    /** 失效日期 */
    private String dateexpi;
    /** 记录状态 */
    private String stsrcd;

}
