package com.ws.accounting.sever;

import com.ws.accounting.entity.AcTrnInfo;
import com.ws.accounting.entity.AcTrnaccBook;
import com.ws.accounting.repository.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;


/**
 * @Author
 * @Description
 * @Date
 * @Param
 * @return
 * @module
 **/


//传票解析
@Service
public class AccAccDetailServer {


    public static final String SUCCESS = "传票解析成功";
    public static final String ERROR = "传票解析失败";
    public static final Logger log = LoggerFactory.getLogger(AccAccDetailServer.class);
    @Autowired
    private RulesRepository rulesRepository;
    @Autowired
    private ProductAttributesRepository attributesRepository;
    @Autowired
    private BookAttributesRepository bookAttributesRepository;
    @Autowired
    private BookAmountRepository bookAmountRepository;
    @Autowired
    private ProductMapSubjectRepository mapSubjectRepository;
    @Autowired
    private PrdruleDefRepository prdruleDefRepository;
    @Autowired
    private AcAccDetailRepository acAccDetailRepository;

    public static void main(String[] args) throws Exception {

        DateFormat sdf = new SimpleDateFormat("YYYYMMDD");
        Date date = sdf.parse("20210231");
        System.out.println(sdf.format(date));

        LocalDate localDate = LocalDate.of(2021, 2, 28);
        System.out.println(localDate);
    }

    /**
     * 1，根据产品码，事件码值查询传票解析规则表
     * 2，如果余额不为0，则继续解析
     * 3，查询产品属性配置表，获取产品属性编码，查询流水获取产品属性编码串
     * 4，根据产品属性编码串+金额记账要素查询业务代号
     * 5，生成传票分录
     *
     * @param acTrnaccBook
     * @return
     */

    public String analysis(AcTrnaccBook acTrnaccBook) {


//        if(acTrnaccBook==null){
//            return ERROR;
//        }
//        else {
//            List<AcAccDetail> acAccDetailList = new ArrayList<>();
//            //根据场景码查询传票解析规则表
//           List<Rules> rulesList = rulesRepository.findAllByBusse(acTrnaccBook.getScene());
//            if(rulesList.isEmpty()||rulesList.size()==0){
//                return ERROR;
//            }
//            log.info("匹配规则数据==="+rulesList.size());
//
//            for (Rules rules:rulesList
//                 ) {
//                // 判断科目来源 3，内部账号 2，科目 1 ，产品
//                if(rules.getSubsrc().equals("1")) {
//                    // 按产品 ,判断上送产品码是否为空，为空报错
//                    if(acTrnaccBook.getPrdcode().isEmpty()||acTrnaccBook.getPrdcode().equals("")){
//                        return ERROR;
//                    }
//
//                    //找到外部产品对应的核算产品
//                    log.info("子流水的外部产品是={}",acTrnaccBook.getPrdcode());
//                    PrdruleDef prdruleDef = prdruleDefRepository.findByProductCode(acTrnaccBook.getPrdcode());
//                    log.info("对应的核算产品是={}",prdruleDef.getPrdruleName());
//
//                    // 判断该流水中金额解析要素是否为0
//                    log.info("金额解析要素=={}",rules.getAmountExpression());
//                        List<BookAmount> bookAmountList = bookAmountRepository.findAllByBookId(acTrnaccBook.getId());
//                        String thisAmount = "";
//                    for (BookAmount amount:bookAmountList
//                         ) {
//                        if(amount.getBookTag().equals(rules.getAmountExpression())){
//                            log.info("解析出的金额是=={}",amount.getBookValue());
//                            thisAmount = amount.getBookValue();
//                        }
//                    }
//                    // 判断金额解析要素是否大于0，如果大于0，继续解析
//                    if(Double.valueOf(thisAmount)>0){
//                        //基本要素
//                        AcAccDetail acAccDetail = new AcAccDetail();
//                        acAccDetail.setAmttran(thisAmount);
//                        acAccDetail.setPrdcode(acTrnaccBook.getPrdcode());
//                        acAccDetail.setFlgdc(rules.getFlgdc());
//                        //科目信息
//                        // 拼接产品属性串
//                        List<ProductAttributes> productAttributesList=attributesRepository.findAllByProductCode(prdruleDef.getPrdruleCode());
//                        StringJoiner attributes = new StringJoiner("_");
//                        productAttributesList.forEach(productAttributes -> {
//
//                        BookAttributes bookAttributes = bookAttributesRepository.findByBookIdAndBookTag(acTrnaccBook.getId(),productAttributes.getProductLabel());
//                            System.out.println(bookAttributes.getBookValue());
//
//                        attributes.add(bookAttributes.getBookValue());
//                    });
//                       log.info("属性拼接字符串===={}",attributes);
//
//                        ProductMapSubject productMapSubject = mapSubjectRepository.findByProductCodeAndBookElementNameAndElementCode(prdruleDef.getPrdruleCode(),rules.getElements(),attributes.toString());
//                        log.info("在科目产品映射表中查到的科目是:{}",productMapSubject.getBusnoa());
//                        acAccDetail.setBusnoa(productMapSubject.getBusnoa());
//                        acAccDetailRepository.save(acAccDetail);
//                    }
//
//
//                }
//            }


        // return SUCCESS;
        //}
        return SUCCESS;

    }

    /**
     * 传票解析
     *
     * @param acTrnInfo
     */
    public void ac1008(AcTrnInfo acTrnInfo) {

    }
}
