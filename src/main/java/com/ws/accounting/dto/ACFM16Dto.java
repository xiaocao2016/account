package com.ws.accounting.dto;

import com.ws.accounting.entity.ACFE17;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * ======================================================
 *
 * @System:[核算系统]
 * @Desc: [dto]
 * @author: [曹国鹏]
 * @date: 2021/2/17 3:07 下午
 * @copyright: [Pactera]
 * ========================================================
 */
@Data
public class ACFM16Dto {

    /** 银行号 */
    private String bankid;
    /** 所属系统 */
    private String subsys;
    /** 会计核算日期 */
    private String accdate;
    /** 会计核算流水号 */
    private String accseqno;
    /** 核心交易日期 */
    private String kltrndate;
    /** 核心流水号 */
    private String kltrnseq;
    /** 核心流水号序号 */
    private String klseqno;
    /** 会计分户记账标志 */
    private String accsubflg;
    /** 交易金额 */
    private String tranamt;
    /** 产品代码 */
    private String prdcode;
    /** 事件代码 */
    private String entcode;
    /** 币种 */
    private String ccy;
    /** 客户号 */
    private String custno;
    /** 账号 */
    private String accno;
    /** 产品账号A */
    private String prdaccno;
    /** 借据编号 */
    private String debitno;
    /** 产品账号户名 */
    private String prdaccname;
    /** 预约编号 */
    private String bookno;
    /** 钞汇标志0 */
    private String crflg;
    /** 归属机构 */
    private String brcadsc;
    /** 凭证号 */
    private String vouno;
    /** 凭证种类 */
    private String voutyp;
    /** 摘要码 */
    private String memono;
    /** 摘要 */
    private String memo;
    /** 对方账号 */
    private String accnoa;
    /** 对方户名A */
    private String accnma;
    /** 对方行号A */
    private String banknoa;
    /** 对方行名A */
    private String banknma;
    /** 币种2 */
    private String ccya;
    /** 对方钞汇标识A */
    private String crflga;
    /** 待销账编号 */
    private String acchngno;
    /** 待销账种类 */
    private String hngacctyp;
    /** 待销账操作 */
    private String hngopflg;
    /** 挂账到期日A */
    private String hngduedate;
    /** 挂账金额A */
    private BigDecimal hngamt;
    /** 撤销金额 */
    private BigDecimal canamt;
    /** 特殊清算标志A */
    private String speclsflg;
    /** 特殊处理标志1 */
    private String spedealflg;
    /** 小微企业标志A */
    private String mmbflg;
    /** 小额贷款标志A */
    private String pettylnflg;
    /** 执行利率A */
    private String execrate;
    /** 利率 */

    private List<ACFE17> list;



}