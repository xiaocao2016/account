package com.ws.accounting.dto;

import lombok.Data;

import javax.persistence.Column;

@Data
public class AcClearPathDto {

    /**
     * 主键ID
     */
    private String id;
    /**
     * 清算模式,0--零级清算，1--多级清算
     */
    private String clearMode;

    /**
     * 记账科目号
     */
    private String accountNo;

    /**
     * 上级记账科目号
     */
    private String paccountNo;

    /**
     * 币种
     */
    private String ccy;

    /**
     * 金融许可证号
     */
    private String finpeno;

    /**
     * 法人行号
     */
    private String subbnkn;

    /**
     * 强拆科目号
     */
    private String saccountNo;
    /**
     * 强拆上级科目号
     */
    private String spaccountNo;
}
