package com.ws.accounting.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * ======================================================
 *
 * @System:[核算系统]
 * @Desc: [dto]
 * @author: [曹国鹏]
 * @date: 2021/2/17 3:07 下午
 * @copyright: [Pactera]
 * ========================================================
 */
@Data
public class ACFM15Dto {
    /** 银行号 */
    private String bankid;
    /** 主机交易日期 */
    private String trandate;
    /** 会计核算日期 */
    private String accdate;
    /** 会计核算流水号 */
    private String accseqno;
    /** 交易机构 */
    private String brctran;
    /** 交易码1 */
    private String trancode;
    /** 所属子系统 */
    private String subsys;
    /** 短信渠道标志 */
    private String channelno;
    /** 发起方交易日期1 */
    private String frntdate;
    /** 发起方流水号 */
    private String frntseq;
    /** 原会计核算日期 */
    private String oriaccdate;
    /** 原会计核算流水号 */
    private String oaccseqno;
    /** 核心交易日期 */
    private String kltrndate;
    /** 核心流水号 */
    private String kltrnseq;
    /** 交易金额0 */
    private BigDecimal tranamt;
    /** 会计核算流水处理标识 */
    private String acdealflg;
    /** 定时/实时标志 */
    private String flgtim;
    /** 是否允许冲销 */
    private String isclsal;
    /** 交易流水类型 */
    private String jouracctyp;
    /** 被冲销标志 */
    private String flgcanl;
    /** 组合服务标志 */
    private String comsrvflg;
    /** 复核柜员 */
    private String telchk;
    /** 记录状态 */
    private String stsrcd;

    private List<ACFM16Dto> list;

}