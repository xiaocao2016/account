package com.ws.accounting.dto;

import lombok.Data;

@Data
public class DictDto {
    private String text;
    private String value;
    private int level;
    private String fullName;
}
