package com.ws.accounting.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * ======================================================
 *
 * @System:[核算系统]
 * @Desc: [dto]
 * @author: [曹国鹏]
 * @date: 2021/2/17 3:07 下午
 * @copyright: [Pactera]
 * ========================================================
 */
@Data
public class ACFE17Dto {

    /** 银行号 */
    private String bankid;
    /** 核心交易日期 */
    private String kltrndate;
    /** 核心流水号序号 */
    private String klseqno;
    /** 核心流水号 */
    private String kltrnseq;
    /** 产品金额标志 */
    private String prdamtflg;
    /** 属性描述 */
    private String propdesc;
    /** 产品金额属性值 */
    private String propval;
    /** 记录状态 */
    private String rcdsts;



}