package com.ws.accounting.dto;

import lombok.Data;

@Data
public class BrcInfo {
    protected String bankID;
    protected String brc;
    protected String brcAttr;
    protected String brcNat;
    protected String taxMod;
    protected String brcPrf;
    protected String stsBusi;
    protected String stsBrc;
    protected String brcLvl;
}
