package com.ws.accounting.dto;

import com.ws.accounting.entity.ACFM15;
import com.ws.accounting.entity.ACFM16;
import com.ws.accounting.entity.AcTrnaccBook;
import lombok.Data;

import java.util.List;

@Data
public class JsonViewDto {

   private ACFM15 acfm15;
   private List<ACFM16> acfm16List;

}
