package com.ws.accounting.controller;

import com.ws.accounting.base.AjaxResult;
import com.ws.accounting.entity.ACFM15;
import com.ws.accounting.repository.ACFM15Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * ======================================================
 *
 * @System:[核算系统]
 * @Desc: [核算测试类/组件描述]
 * @author: [weibin.zhu]
 * @date: 2021-02-15
 * @copyright: [Pactera]
 * ========================================================
 */
@RestController
@RequestMapping("acfm15")
public class ACFM15Controller {

    @Autowired
    private ACFM15Repository repository;

    @GetMapping("list")
    @ResponseBody
    public AjaxResult list(){
        List<ACFM15> rulesList = (List<ACFM15>) repository.findAll();
        return AjaxResult.getInstance(rulesList);
    }

    @PostMapping
    @ResponseBody
    public AjaxResult save(ACFM15 ACFM15){
        repository.save(ACFM15);
        return AjaxResult.getInstance("新增成功");
    }

    @DeleteMapping("{id}")
    @ResponseBody
    public AjaxResult del(@PathVariable("id") String id){

        repository.deleteById(id);
        return AjaxResult.getInstance("删除成功");
    }

}
