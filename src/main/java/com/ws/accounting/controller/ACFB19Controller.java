package com.ws.accounting.controller;

import com.ws.accounting.base.AjaxResult;
import com.ws.accounting.entity.ACFB19;
import com.ws.accounting.repository.ACFB19Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * ======================================================
 *
 * @System:[核算系统]
 * @Desc: [核算测试类/组件描述]
 * @author: [weibin.zhu]
 * @date: 2021-02-15
 * @copyright: [Pactera]
 * ========================================================
 */
@RestController
@RequestMapping("acfb19")
public class ACFB19Controller {

    @Autowired
    private ACFB19Repository repository;

    @GetMapping("list")
    @ResponseBody
    public AjaxResult list(){
        List<ACFB19> rulesList = (List<ACFB19>) repository.findAll();
        return AjaxResult.getInstance(rulesList);
    }

    @PostMapping
    @ResponseBody
    public AjaxResult save(ACFB19 ACFB19){
        repository.save(ACFB19);
        return AjaxResult.getInstance("新增成功");
    }

    @DeleteMapping("{id}")
    @ResponseBody
    public AjaxResult del(@PathVariable("id") String id){

        repository.deleteById(id);
        return AjaxResult.getInstance("删除成功");
    }

}
