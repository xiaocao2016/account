package com.ws.accounting.controller;

import com.ws.accounting.base.AjaxResult;
import com.ws.accounting.entity.AcTrnInfo;
import com.ws.accounting.entity.BrBrcInfo;
import com.ws.accounting.repository.AcTrnInfoRepository;
import com.ws.accounting.repository.BrBrcInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("brcInfo")
public class BrcInfoController {

    @Autowired
    private BrBrcInfoRepository repository;

    @GetMapping("list")
    @ResponseBody
    public AjaxResult list(){
        List<BrBrcInfo> rulesList = (List<BrBrcInfo>) repository.findAll();
        return AjaxResult.getInstance(rulesList);
    }




    @PostMapping
    @ResponseBody
    public AjaxResult save(BrBrcInfo brBrcInfo){
        repository.save(brBrcInfo);
        return AjaxResult.getInstance("新增成功");
    }

    @DeleteMapping("{id}")
    @ResponseBody
    public AjaxResult del(@PathVariable("id") String id){

        repository.deleteById(id);
        return AjaxResult.getInstance("删除成功");
    }
}
