package com.ws.accounting.controller;

import com.ws.accounting.base.AjaxResult;
import com.ws.accounting.entity.AcTrnInfo;
import com.ws.accounting.entity.AcTrnaccBook;
import com.ws.accounting.repository.AcTrnInfoRepository;
import com.ws.accounting.repository.AcTrnaccBookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("actrnbook")
public class AcTrnaccBookController {

    @Autowired
    private AcTrnaccBookRepository repository;

    @GetMapping("list")
    @ResponseBody
    public AjaxResult list(){
        List<AcTrnaccBook> rulesList = (List<AcTrnaccBook>) repository.findAll();
        return AjaxResult.getInstance(rulesList);
    }




    @PostMapping
    @ResponseBody
    public AjaxResult save(AcTrnaccBook acTrnaccBook){
        repository.save(acTrnaccBook);
        return AjaxResult.getInstance("新增成功");
    }

    @DeleteMapping("{id}")
    @ResponseBody
    public AjaxResult del(@PathVariable("id") String id){

        repository.deleteById(id);
        return AjaxResult.getInstance("删除成功");
    }
}
