package com.ws.accounting.controller;


import com.ws.accounting.base.AjaxResult;
import com.ws.accounting.dto.DictDto;
import com.ws.accounting.entity.Dict;
import com.ws.accounting.repository.DictRepository;
import com.ws.accounting.utils.DictTreeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
@RequestMapping("dict")
public class DictController {

    @Autowired
    private DictRepository dictRepository;



    @GetMapping("tree")
    @ResponseBody
    public AjaxResult getDictTree(){
        List<Dict> dictionaryList = (List<Dict>)dictRepository.findAll();
        Map<String,Dict> map = new HashMap<>();
        dictionaryList.stream().forEach(index -> {
            map.put(index.getId(),index);
        });
        return AjaxResult.getInstance(DictTreeUtil.getNodeJson("9999",map));
    }


    @GetMapping("topName/{topName}")
    @ResponseBody
    public AjaxResult topName(@PathVariable("topName") String topName){
        List<Dict> dictionaryList = dictRepository.findByTopName(topName);
        return AjaxResult.getInstance(dictionaryList);
    }

    @GetMapping("level/{level}")
    @ResponseBody
    public AjaxResult topName(@PathVariable("level") int level){
        List<Dict> dictionaryList = dictRepository.findAllByLevel(level);
        return AjaxResult.getInstance(dictionaryList);
    }

    @GetMapping("list/{level}")
    @ResponseBody
    public AjaxResult list(@PathVariable("level") int level){
        List<Dict> dictList = (List<Dict>) dictRepository.findAll();
        dictList = dictList.stream().filter(dict -> dict.getLevel()<level).collect(Collectors.toList());
        List<DictDto> dictDtoList = new ArrayList<>();
        dictList.forEach(dict -> {
            DictDto dictDto = new DictDto();
            dictDto.setText(dict.getName());
            dictDto.setValue(dict.getId());
            dictDto.setLevel(dict.getLevel());
            dictDto.setFullName(dict.getFullName());
            dictDtoList.add(dictDto);
        });
        return AjaxResult.getInstance(dictDtoList);
    }

    @PostMapping
    @ResponseBody
    public AjaxResult save(Dict dict){
        dict.setTitle(dict.getName());
        System.out.println(dict);
        dictRepository.save(dict);
        updateChilid(dict);
        return AjaxResult.getInstance("保存成功");
    }


    @DeleteMapping("{id}")
    @ResponseBody
    public AjaxResult delDict(@PathVariable("id") String id){
        Dict dict = dictRepository.findById(id).get();
        dictRepository.delete(dict);
        deleteChilid(dict);
        return AjaxResult.getInstance("删除字典成功");
    }

    public  void updateChilid(Dict dict){
        List<Dict> dictList = (List<Dict>) dictRepository.findAll();
        dictList = dictList.stream().filter(dict1 -> dict1.getParentId().equals(dict.getId()))
                .collect(Collectors.toList());
        if(dictList.size()>0&&!dictList.isEmpty()){
            dictList.forEach(dict1 -> {
                dict1.setFullName(dict.getFullName()+"/"+dict1.getName());
                dict1.setLevel(dict.getLevel()+1);
                dictRepository.save(dict1);
                updateChilid(dict1);
            });
        }
    }

    public void  deleteChilid(Dict dict){
        List<Dict> dictList = (List<Dict>) dictRepository.findAll();
        dictList = dictList.stream().filter(dict1 -> dict1.getParentId().equals(dict.getId()))
                .collect(Collectors.toList());
        if(dictList.size()>0&&!dictList.isEmpty()){
            dictList.forEach(dict1 -> {
                dictRepository.delete(dict1);
                deleteChilid(dict1);
            });
        }
    }



}
