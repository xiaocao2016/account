package com.ws.accounting.controller;


import com.ws.accounting.base.AjaxResult;
import com.ws.accounting.dto.AcClearPathDto;
import com.ws.accounting.entity.AcClearPath;
import com.ws.accounting.repository.AcClearPathRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import java.awt.print.Pageable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

@RestController
@RequestMapping("acclear")
public class AcClearPathController {

    @Autowired
    private AcClearPathRepository repository;

    @GetMapping("list")
    @ResponseBody
    public AjaxResult list(){
        List<AcClearPath> rulesList = (List<AcClearPath>) repository.findAll();
        return AjaxResult.getInstance(rulesList);
    }




    @PostMapping
    @ResponseBody
    public AjaxResult save(AcClearPath acClearPath){
        repository.save(acClearPath);
        return AjaxResult.getInstance("新增成功");
    }

    @DeleteMapping("{id}")
    @ResponseBody
    public AjaxResult del(@PathVariable("id") String id){

        repository.deleteById(id);
        return AjaxResult.getInstance("删除成功");
    }
}
