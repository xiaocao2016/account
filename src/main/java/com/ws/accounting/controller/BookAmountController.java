package com.ws.accounting.controller;

import com.alibaba.fastjson.JSONObject;
import com.ws.accounting.base.AjaxResult;
import com.ws.accounting.entity.BookAmount;
import com.ws.accounting.entity.BookAttributes;
import com.ws.accounting.repository.BookAmountRepository;
import com.ws.accounting.repository.BookAttributesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("bookAmount")
public class BookAmountController {

    @Autowired
    private BookAmountRepository repository;

    @GetMapping("list")
    @ResponseBody
    public AjaxResult list(){
        List<BookAmount> rulesList = (List<BookAmount>) repository.findAll();
        return AjaxResult.getInstance(rulesList);
    }




    @PostMapping
    @ResponseBody
    public AjaxResult save(BookAmount bookAmount){
        repository.save(bookAmount);
        return AjaxResult.getInstance("新增成功");
    }

    @DeleteMapping("{id}")
    @ResponseBody
    public AjaxResult del(@PathVariable("id") String id){

        repository.deleteById(id);
        return AjaxResult.getInstance("删除成功");
    }
}
