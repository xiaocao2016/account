package com.ws.accounting.controller;

/**
 * ======================================================
 *
 * @System:[核算系统]
 * @Desc: [机构控制关系类/组件描述]
 * @author: [weibin.zhu]
 * @date: 2021-02-11 9:18
 * @copyright: [Pactera]
 * ========================================================
 */

import com.ws.accounting.base.AjaxResult;
import com.ws.accounting.entity.BrcCtrlShip;
import com.ws.accounting.repository.BrcCtrlShipRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
 * 机构控制关系
 */
@RestController
@RequestMapping("brcctrlship")
public class BrcCtrlShipController {


    @Autowired
    private BrcCtrlShipRepository repository;

    @GetMapping("list")
    @ResponseBody
    public AjaxResult list(){
        List<BrcCtrlShip> rulesList = (List<BrcCtrlShip>) repository.findAll();
        return AjaxResult.getInstance(rulesList);
    }

    @PostMapping
    @ResponseBody
    public AjaxResult save(BrcCtrlShip BrcCtrlShip){
        repository.save(BrcCtrlShip);
        return AjaxResult.getInstance("新增成功");
    }

    @DeleteMapping("{id}")
    @ResponseBody
    public AjaxResult del(@PathVariable("id") String id){

        repository.deleteById(id);
        return AjaxResult.getInstance("删除成功");
    }
    
}
