package com.ws.accounting.controller;

import com.alibaba.fastjson.JSONObject;
import com.ws.accounting.base.AjaxResult;
import com.ws.accounting.entity.AcTrnInfo;
import com.ws.accounting.entity.BookAttributes;
import com.ws.accounting.repository.BookAttributesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.awt.print.Book;
import java.util.List;

@RestController
@RequestMapping("bookAttributes")
public class BookAttributesController {

    @Autowired
    private BookAttributesRepository repository;

    @GetMapping("list")
    @ResponseBody
    public AjaxResult list(){
        List<BookAttributes> rulesList = (List<BookAttributes>) repository.findAll();
        return AjaxResult.getInstance(rulesList);
    }




    @PostMapping
    @ResponseBody
    public AjaxResult save(BookAttributes bookAttributes){
        repository.save(bookAttributes);
        return AjaxResult.getInstance("新增成功");
    }

    @PostMapping("all")
    @ResponseBody
    public AjaxResult saveAll(@RequestBody String params){

        JSONObject jsonObject = JSONObject.parseObject(params);
        List<BookAttributes> bookAttributesList =
                JSONObject.parseArray(jsonObject.getJSONArray("params").toJSONString(),BookAttributes.class);

        if(bookAttributesList.size()>0){
            repository.deleteAllByBookId(bookAttributesList.get(0).getBookId());
        }
        repository.saveAll(bookAttributesList);
        return AjaxResult.getInstance("新增成功");
    }

    @DeleteMapping("{id}")
    @ResponseBody
    public AjaxResult del(@PathVariable("id") String id){

        repository.deleteById(id);
        return AjaxResult.getInstance("删除成功");
    }
}
