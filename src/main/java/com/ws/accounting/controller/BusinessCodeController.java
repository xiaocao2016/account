package com.ws.accounting.controller;

import com.ws.accounting.base.AjaxResult;
import com.ws.accounting.entity.BusinessCode;
import com.ws.accounting.repository.BusinessCodeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("businessCode")
public class BusinessCodeController {

    @Autowired
    private BusinessCodeRepository repository;

    @GetMapping("{ywdh}")
    public AjaxResult findByCode(@PathVariable("ywdh") String ywdh){

        BusinessCode businessCode = repository.findByYEWUDH(ywdh);
        return AjaxResult.getInstance(businessCode);
    }
}
