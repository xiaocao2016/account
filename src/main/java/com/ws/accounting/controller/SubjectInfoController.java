package com.ws.accounting.controller;

import com.ws.accounting.base.AjaxResult;
import com.ws.accounting.entity.SubjectInfo;
import com.ws.accounting.repository.SubjectInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("subject")
public class SubjectInfoController {

    @Autowired
    private SubjectInfoRepository repository;

    @GetMapping("list")
    @ResponseBody
    public AjaxResult list(){
        List<SubjectInfo> subjectInfoList = (List<SubjectInfo>) repository.findAll();
        return AjaxResult.getInstance(subjectInfoList);
    }
}
