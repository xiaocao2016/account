package com.ws.accounting.controller;


import com.ws.accounting.base.AjaxResult;
import com.ws.accounting.entity.IaAccInfo;
import com.ws.accounting.repository.IaAccInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("iaaccinfo")
public class IaAccInfoController {

    @Autowired
    private IaAccInfoRepository repository;

    @GetMapping("list")
    @ResponseBody
    public AjaxResult list(){
        List<IaAccInfo> rulesList = (List<IaAccInfo>) repository.findAll();
        return AjaxResult.getInstance(rulesList);
    }




    @PostMapping
    @ResponseBody
    public AjaxResult save(IaAccInfo iaAccInfo){
        repository.save(iaAccInfo);
        return AjaxResult.getInstance("新增成功");
    }

    @DeleteMapping("{id}")
    @ResponseBody
    public AjaxResult del(@PathVariable("id") String id){

        repository.deleteById(id);
        return AjaxResult.getInstance("删除成功");
    }
}
