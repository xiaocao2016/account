package com.ws.accounting.controller;

import com.ws.accounting.base.AjaxResult;
import com.ws.accounting.entity.AcTrnInfo;
import com.ws.accounting.entity.PrdruleDef;
import com.ws.accounting.repository.AcTrnInfoRepository;
import com.ws.accounting.repository.PrdruleDefRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;

@RestController
@RequestMapping("actrninfo")
public class AcTrnInfoController {

    @Autowired
    private AcTrnInfoRepository repository;

    @GetMapping("list")
    @ResponseBody
    public AjaxResult list(){
        List<AcTrnInfo> rulesList = (List<AcTrnInfo>) repository.findAll();
        return AjaxResult.getInstance(rulesList);
    }




    @PostMapping
    @ResponseBody
    public AjaxResult save(AcTrnInfo acTrnInfo){
        repository.save(acTrnInfo);
        return AjaxResult.getInstance("新增成功");
    }

    @DeleteMapping("{id}")
    @ResponseBody
    public AjaxResult del(@PathVariable("id") String id){

        repository.deleteById(id);
        return AjaxResult.getInstance("删除成功");
    }
}
