package com.ws.accounting.controller;

import com.ws.accounting.base.AjaxResult;
import com.ws.accounting.entity.ACFM22;
import com.ws.accounting.repository.ACFM22Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * ======================================================
 *
 * @System:[核算系统]
 * @Desc: [核算测试类/组件描述]
 * @author: [weibin.zhu]
 * @date: 2021-02-15
 * @copyright: [Pactera]
 * ========================================================
 */
@RestController
@RequestMapping("acfm22")
public class ACFM22Controller {

    @Autowired
    private ACFM22Repository repository;

    @GetMapping("list")
    @ResponseBody
    public AjaxResult list(){
        List<ACFM22> rulesList = (List<ACFM22>) repository.findAll();
        return AjaxResult.getInstance(rulesList);
    }

    @PostMapping
    @ResponseBody
    public AjaxResult save(ACFM22 ACFM22){
        repository.save(ACFM22);
        return AjaxResult.getInstance("新增成功");
    }

    @DeleteMapping("{id}")
    @ResponseBody
    public AjaxResult del(@PathVariable("id") String id){

        repository.deleteById(id);
        return AjaxResult.getInstance("删除成功");
    }

}
