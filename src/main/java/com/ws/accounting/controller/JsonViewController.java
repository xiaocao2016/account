package com.ws.accounting.controller;

import com.ws.accounting.base.AjaxResult;
import com.ws.accounting.dto.ACFE17Dto;
import com.ws.accounting.dto.ACFM15Dto;
import com.ws.accounting.dto.ACFM16Dto;
import com.ws.accounting.dto.JsonViewDto;
import com.ws.accounting.entity.*;
import com.ws.accounting.repository.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController()
@RequestMapping("jsonView")
public class JsonViewController {


    @Autowired
    private ACFM15Repository acfm15Repository;

    @Autowired
    private ACFM16Repository acfm16Repository;

    @Autowired
    private ACFE17Repository acfe17Repository;

    @GetMapping("list/{seq}")
    public AjaxResult list(@PathVariable("seq") String seq){

        ACFM15 acfm15 = acfm15Repository.findByActrnseqno(seq);

        List<ACFM16> acfm16List = acfm16Repository.findAllByKltrnseqno(seq);


        ACFM15Dto acfm15Dto = new ACFM15Dto();
        List<ACFM16Dto> acfm16DtoList = new ArrayList<>();
        List<ACFE17Dto> acfe17DtoList = new ArrayList<>();


        acfm16List.forEach(acfm16 -> {
            ACFM16Dto acfm16Dto = new ACFM16Dto();
            BeanUtils.copyProperties(acfm16,acfm16Dto);
            List<ACFE17> acfe17List = acfe17Repository.findAllByKlregseq(acfm16.getKlregseq());
            //BeanUtils.copyProperties(acfe17List,acfe17DtoList);
            acfm16Dto.setList(acfe17List);
            acfm16DtoList.add(acfm16Dto);
        });

        BeanUtils.copyProperties(acfm15,acfm15Dto);
        acfm15Dto.setList(acfm16DtoList);
        return AjaxResult.getInstance(acfm15Dto);
    }

   
}
