package com.ws.accounting.controller;

import com.ws.accounting.base.AjaxResult;
import com.ws.accounting.entity.ProductMapSubject;
import com.ws.accounting.entity.Scene;
import com.ws.accounting.repository.ProductMapSubjectRepository;
import com.ws.accounting.repository.SceneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("scene")
public class SceneController {

    @Autowired
    private SceneRepository repository;

    @GetMapping("list")
    @ResponseBody
    public AjaxResult list(){
        List<Scene> rulesList = (List<Scene>) repository.findAll();
        return AjaxResult.getInstance(rulesList);
    }

    @PostMapping
    @ResponseBody
    public AjaxResult save(Scene scene){
        repository.save(scene);
        return AjaxResult.getInstance("新增成功");
    }

    @DeleteMapping("{id}")
    @ResponseBody
    public AjaxResult del(@PathVariable("id") String id){

        repository.deleteById(id);
        return AjaxResult.getInstance("删除成功");
    }
}
