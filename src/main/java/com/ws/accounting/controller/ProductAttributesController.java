package com.ws.accounting.controller;

import com.ws.accounting.base.AjaxResult;
import com.ws.accounting.entity.ProductAttributes;
import com.ws.accounting.entity.Rules;
import com.ws.accounting.repository.ProductAttributesRepository;
import com.ws.accounting.repository.RulesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("product")
public class ProductAttributesController {

    @Autowired
    private ProductAttributesRepository repository;

    @GetMapping("list")
    @ResponseBody
    public AjaxResult list(){
        List<ProductAttributes> rulesList = (List<ProductAttributes>) repository.findAll();
        return AjaxResult.getInstance(rulesList);
    }


    @GetMapping("list/{productCode}")
    @ResponseBody
    public AjaxResult listByCode(@PathVariable("productCode") String productCode){
        List<ProductAttributes> rulesList = (List<ProductAttributes>) repository.findAllByProductCode(productCode);
        return AjaxResult.getInstance(rulesList);
    }

    @PostMapping
    @ResponseBody
    public AjaxResult save(ProductAttributes productAttributes){
        repository.save(productAttributes);
        return AjaxResult.getInstance("新增成功");
    }

    @DeleteMapping("{id}")
    @ResponseBody
    public AjaxResult del(@PathVariable("id") String id){

        repository.deleteById(id);
        return AjaxResult.getInstance("删除成功");
    }
}
