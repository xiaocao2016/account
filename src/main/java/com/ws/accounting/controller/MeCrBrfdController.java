package com.ws.accounting.controller;

import com.ws.accounting.base.AjaxResult;
import com.ws.accounting.entity.BrBrcInfo;
import com.ws.accounting.entity.MeCrBrfd;
import com.ws.accounting.entity.PrdruleDef;
import com.ws.accounting.repository.MeCrBrfdRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("mecrbrfd")
public class MeCrBrfdController {

    @Autowired
    private MeCrBrfdRepository repository;

    @PostMapping
    @ResponseBody
    public AjaxResult save(MeCrBrfd meCrBrfd){

        if(meCrBrfd!=null){
            repository.save(meCrBrfd);
            return AjaxResult.getInstance("新增成功");
        }
        return AjaxResult.getInstance("输入数据为空，新增失败");
    }

    @DeleteMapping("{id}")
    @ResponseBody
    public AjaxResult del(@PathVariable("id") String id){
        repository.deleteById(id);
        return AjaxResult.getInstance("删除成功");
    }

    @GetMapping("list")
    @ResponseBody
    public AjaxResult list(){
        List<MeCrBrfd> rulesList = (List<MeCrBrfd>) repository.findAll();
        return AjaxResult.getInstance(rulesList);
    }

}
