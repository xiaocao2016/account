package com.ws.accounting.controller;

import com.ws.accounting.base.AjaxResult;
import com.ws.accounting.entity.AcAccDetail;
import com.ws.accounting.repository.AcAccDetailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 传票
 */
@RestController
@RequestMapping("acaccdetail")
public class AcAccDetailController {
    @Autowired
    private AcAccDetailRepository repository;

    @GetMapping("list")
    @ResponseBody
    public AjaxResult list(){
        List<AcAccDetail> rulesList = (List<AcAccDetail>) repository.findAll();
        return AjaxResult.getInstance(rulesList);
    }


    @PostMapping
    @ResponseBody
    public AjaxResult save(AcAccDetail acAccDetail){
        repository.save(acAccDetail);
        return AjaxResult.getInstance("新增成功");
    }

    @DeleteMapping("{id}")
    @ResponseBody
    public AjaxResult del(@PathVariable("id") String id){

        repository.deleteById(id);
        return AjaxResult.getInstance("删除成功");
    }

    public static void main(String[] args) {
        System.out.println(System.getProperty("user.home"));
    }
}
