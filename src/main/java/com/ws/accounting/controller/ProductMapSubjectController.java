package com.ws.accounting.controller;

import com.ws.accounting.base.AjaxResult;
import com.ws.accounting.entity.ProductMapSubject;
import com.ws.accounting.repository.ProductMapSubjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("productSubject")
public class ProductMapSubjectController {

    @Autowired
    private ProductMapSubjectRepository repository;

    @GetMapping("list")
    @ResponseBody
    public AjaxResult list(){
        List<ProductMapSubject> rulesList = (List<ProductMapSubject>) repository.findAll();
        return AjaxResult.getInstance(rulesList);
    }

    @PostMapping
    @ResponseBody
    public AjaxResult save(ProductMapSubject productMapSubject){
        repository.save(productMapSubject);
        return AjaxResult.getInstance("新增成功");
    }

    @DeleteMapping("{id}")
    @ResponseBody
    public AjaxResult del(@PathVariable("id") String id){

        repository.deleteById(id);
        return AjaxResult.getInstance("删除成功");
    }
}
