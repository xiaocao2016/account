package com.ws.accounting.controller;

import com.ws.accounting.base.AjaxResult;
import com.ws.accounting.entity.ACFM08;
import com.ws.accounting.repository.ACFM08Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * ======================================================
 *
 * @System:[核算系统]
 * @Desc: [核算测试类/组件描述]
 * @author: [weibin.zhu]
 * @date: 2021-02-15
 * @copyright: [Pactera]
 * ========================================================
 */
@RestController
@RequestMapping("acfm08")
public class ACFM08Controller {

    @Autowired
    private ACFM08Repository repository;

    @GetMapping("list")
    @ResponseBody
    public AjaxResult list(){
        List<ACFM08> rulesList = (List<ACFM08>) repository.findAll();
        return AjaxResult.getInstance(rulesList);
    }

    @PostMapping
    @ResponseBody
    public AjaxResult save(ACFM08 ACFM08){
        repository.save(ACFM08);
        return AjaxResult.getInstance("新增成功");
    }

    @DeleteMapping("{id}")
    @ResponseBody
    public AjaxResult del(@PathVariable("id") String id){
        repository.deleteById(id);
        return AjaxResult.getInstance("删除成功");
    }
    
}
