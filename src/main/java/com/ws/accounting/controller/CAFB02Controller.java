package com.ws.accounting.controller;

import com.ws.accounting.base.AjaxResult;
import com.ws.accounting.entity.CAFB02;
import com.ws.accounting.repository.CAFB02Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * ======================================================
 *
 * @System:[核算系统]
 * @Desc: [核算测试类/组件描述]
 * @author: [weibin.zhu]
 * @date: 2021-02-15
 * @copyright: [Pactera]
 * ========================================================
 */
@RestController
@RequestMapping("cafb02")
public class CAFB02Controller {

    @Autowired
    private CAFB02Repository repository;

    @GetMapping("list")
    @ResponseBody
    public AjaxResult list(){
        List<CAFB02> rulesList = (List<CAFB02>) repository.findAll();
        return AjaxResult.getInstance(rulesList);
    }

    @PostMapping
    @ResponseBody
    public AjaxResult save(CAFB02 CAFB02){
        repository.save(CAFB02);
        return AjaxResult.getInstance("新增成功");
    }

    @DeleteMapping("{id}")
    @ResponseBody
    public AjaxResult del(@PathVariable("id") String id){

        repository.deleteById(id);
        return AjaxResult.getInstance("删除成功");
    }
}
