package com.ws.accounting.controller;

import com.ws.accounting.base.AjaxResult;
import com.ws.accounting.entity.PrdruleDef;
import com.ws.accounting.entity.ProductMapSubject;
import com.ws.accounting.repository.PrdruleDefRepository;
import com.ws.accounting.repository.ProductMapSubjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;

@RestController
@RequestMapping("prdrule")
public class PrdruleDefController {

    @Autowired
    private PrdruleDefRepository repository;

    @GetMapping("list")
    @ResponseBody
    public AjaxResult list(){
        List<PrdruleDef> rulesList = (List<PrdruleDef>) repository.findAll();
        return AjaxResult.getInstance(rulesList);
    }


    @GetMapping("listDistinct")
    @ResponseBody
    public AjaxResult listDistinct(){
        List<PrdruleDef> rulesList = (List<PrdruleDef>) repository.findAll();
        rulesList = rulesList.stream().collect(Collectors.collectingAndThen(
                Collectors.toCollection(()->new TreeSet<>(Comparator.comparing(PrdruleDef::getPrdruleCode))), ArrayList::new));
        return AjaxResult.getInstance(rulesList);
    }

    @PostMapping
    @ResponseBody
    public AjaxResult save(PrdruleDef prdruleDef){
        repository.save(prdruleDef);
        return AjaxResult.getInstance("新增成功");
    }

    @DeleteMapping("{id}")
    @ResponseBody
    public AjaxResult del(@PathVariable("id") String id){

        repository.deleteById(id);
        return AjaxResult.getInstance("删除成功");
    }
}
