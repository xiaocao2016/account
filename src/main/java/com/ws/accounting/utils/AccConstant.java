package com.ws.accounting.utils;


/**
 * @author : MaHuan
 * @program : accounting
 * @description : 常量值
 * @date : 2020-10-16 15:15
 **/
public class AccConstant {


    /**
     *  字典类型：01 记账要素字典
     */
    public static final String DIC_TYPE_1 = "01";
    /**
     *  字典类型：02 产品属性字典
     */
    public static final String DIC_TYPE_2 = "02";
    /**
     *  字典类型：03 内部场景字典
     */
    public static final String DIC_TYPE_3 = "03";
    /**
     *  字典类型：04 内部产品字典
     */
    public static final String DIC_TYPE_4 = "04";
    /**
     *  字典类型：05 记账机构类型
     */
    public static final String DIC_TYPE_5 = "05";

    /**
     * 记录状态：0是删除
     */
    public static final String RECORD_STATUS_NO = "0";

    /**
     * 记录状态：1是正常
     */
    public static final String RECORD_STATUS_YES = "1";


    /**余额控制方向
     * 余额控制方向，0借方
     */
    public static final String BALCTRL_DE = "0";
    /**
     * 余额控制方向，1贷方
     */
    public static final String BALCTRL_CR = "1";
    /**
     * 余额控制方向，2轧差
     */
    public static final String BALCTRL_ZC = "2";
    /**
     * 余额控制方向，3双向
     */
    public static final String BALCTRL_B = "3";

    /**
     * 是否存在下级科目，0否，1是
     */
    public static final String EXIST_SUB_SBJNUM_YES = "1";
    /**
     * 是否存在下级科目，0否，1是
     */
    public static final String EXIST_SUB_SBJNUM_NO = "0";

    /**
     * 是否使用计价中心
     * 0-不使用 1使用
     */
    public static final String USE_VALUATION_YES = "1";

    /**
     * 是否使用计价中心
     * 0-不使用 1使用
     */
    public static final String USE_VALUATION_NO = "0";

    /**
     * 币种：999 - 通用
     */
    public static final String CCY_ALL = "999";

    /**
     * 币种：036 - 澳元
     */
    public static final String CCY_AUD = "036";
    /**
     * 币种：124 - 加元
     */
    public static final String CCY_CAD = "124";
    /**
     * 币种：156 - 人民币
     */
    public static final String CCY_CNY = "156";
    /**
     * 币种：344 - 港币
     */
    public static final String CCY_HKD = "344";
    /**
     * 币种：392 - 日元
     */
    public static final String CCY_JPY = "392";
    /**
     * 币种：410 - 韩元
     */
    public static final String CCY_KRW = "410";
    /**
     * 币种：702 - 新元
     */
    public static final String CCY_SGD = "702";
    /**
     * 币种：756 - 瑞士法郎
     */
    public static final String CCY_CHF = "756";
    /**
     * 币种：764 - 泰铢
     */
    public static final String CCY_THB = "764";
    /**
     * 币种：826 - 英镑
     */
    public static final String CCY_GBP = "826";
    /**
     * 币种：840 - 美元
     */
    public static final String CCY_USD = "840";
    /**
     * 币种：978 - 欧元
     */
    public static final String CCY_EUR = "978";

    /**
     * 本币结售汇 澳元 BBJSHAUD
     */
    public static final String BBJSH_AUD = "BBJSHAUD";

    /**
     * 借贷标志: "D"
     */
    public static final String FLGDC_DEBIT = "0";

    /**
     * 借贷标志: "C"
     */
    public static final String FLGDC_CREDIT = "1";

    /**
     * 借贷标志: "X" - 通用
     */
    public static final String FLGDC_FLAG_X = "X";

    /**
     * 借贷标志: "B" - 双向
     */
    public static final String FLGDC_FLAG_B = "B";

    /**
     * 生效日期: "2000-01-01"
     */
    public static final String COMMENCEMENT_DATE = "2000-01-01";

    /**
     * 定时/实时标志: "1" - 定时
     */
    public static final String FLGTIM_FLAG_1 = "1";

    /**
     * 定时/实时标志: "2" - 实时
     */
    public static final String FLGTIM_FLAG_2 = "2";

    /**
     * 科目类别: "5" - 表外类科目
     */
    public static final String TYPACC_FLAG_5 = "5";

    /**
     * 表内或表外标志: "1" - 表内
     */
    public static final String FLGSISO_FLAG_1 = "1";

    /**
     * 表内或表外标志: "2" - 表外
     */
    public static final String FLGSISO_FLAG_2 = "2";

    /**
     * 记账方式: "1" - 内部账记账
     */
    public static final String ACCENMD_FLAG_1 = "1";

    /**
     * 记账方式: "2" - 表外记账
     */
    public static final String ACCENMD_FLAG_2 = "2";

    /**
     * 是否手工记账: "0" - 否
     */
    public static final String ISHACC_FLAG_0 = "0";

    /**
     * 是否手工记账: "1" - 是
     */
    public static final String ISHACC_FLAG_1 = "1";

    /**
     * 是否记传票: "0" - 否
     */
    public static final String ISWRTVC_FLAG_0 = "0";

    /**
     * 是否记传票: "1" - 是
     */
    public static final String ISWRTVC_FLAG_1 = "1";

    /**
     * 是否允许冲销: "0" - 否
     */
    public static final String ISCLSAL_FLAG_0 = "0";

    /**
     * 是否允许冲销: "1" - 是
     */
    public static final String ISCLSAL_FLAG_1 = "1";

    /**
     * 流水账类型: "0" - 正常
     */
    public static final String STANTYP_FLAG_0 = "0";

    /**
     * 流水账类型: "1" - 冲销
     */
    public static final String STANTYP_FLAG_1 = "1";

    /**
     * 流水账类型: "2" - 补正
     */
    public static final String STANTYP_FLAG_2 = "2";

    /**
     * 被冲销标志："0" - 未被冲销
     */
    public static final String FLGCANL_FLAG_0 = "0";

    /**
     * 被冲销标志: "1" - 被冲销
     */
    public static final String FLGCANL_FLAG_1 = "1";

    /**
     * 入账标志: "0" - 未入账
     */
    public static final String FLGINAC_FLAG_0 = "0";

    /**
     * 入账标志: "1" - 已入账
     */
    public static final String FLGINAC_FLAG_1 = "1";

    /**
     * 是否免税: "1" - 是
     */
    public static final String ISDUTFR_YES = "1";

    /**
     * 是否免税： "0" - 否
     */
    public static final String ISDUTFR_NO = "0";

    /**
     * 是否优惠： "1" - 是
     */
    public static final String ISALFAV_YES = "1";

    /**
     * 是否优惠： "0" - 否
     */
    public static final String ISALFAV_NO = "0";

    /**
     * 是否小微企业: "0" - 否
     */
    public static final String FLGSMMI_NO = "0";

    /**
     * 是否小微企业: "1" - 是
     */
    public static final String FLGSMMI_YES = "1";

    /**
     * 是否小微企业: "2" - 都可以
     */
    public static final String FLGSMMI_WHATEVER = "2";

    /**
     * 是否小额贷款: "3" - 否
     */
    public static final String FLGPLAN_NO = "0";

    /**
     * 是否小额贷款: "1" - 是
     */
    public static final String FLGPLAN_YES = "1";

    /**
     * 是否小额贷款: "2" - 都可以
     */
    public static final String FLGPLAN_WHATEVER = "2";

    /**
     * 是否记账： "0" - 否
     */
    public static final String ISACCT_NO = "0";

    /**
     * 是否记账： "1" - 是
     */
    public static final String ISACCT_YES = "1";

    /**
     * 是否记账： "2" - 营改增记账
     */
    public static final String ISACCT_VAT = "2";

    /**
     * 是否检查上下级关系: "0" - 否
     */
    public static final String FLGLINK_NO = "0";

    /**
     * 是否检查上下级关系: "1" - 是
     */
    public static final String FLGLINK_YES = "1";

    /**
     * 是否检查余额为零: "0" - 否
     */
    public static final String ISBLDZ_NO = "0";

    /**
     * 是否检查余额为零: "1" - 是
     */
    public static final String ISBLDZ_YES = "1";

    /**
     * 账户状态: "0" - 正常
     */
    public static final String STSACC_NORMAL_0 = "0";

    /**
     * 账户状态: "1" - 销户
     */
    public static final String STSACC_CANCELLATION_1 = "1";

    /**
     * 账户状态: "2" - 控制
     */
    public static final String STSACC_CONTROL_2 = "2";

    /**
     * 状态: "9" - 无此账号
     */
    public static final String STSACC_NOACCNO_9 = "9";

    /**
     * 特殊清算标志: "0" - 正常
     */

    public static final String FLGCLRS_NORMAL_0 = "0";

    /**
     * 特殊清算标志: "1" - 特殊不清算
     */
    public static final String FLGCLRS_SPECIAL_1 = "1";
    /**
     * 借据状态: "0" - 正常
     */
    public static final String DEBTSTS_NORMAL_0 = "0";
    /**
     * 借据状态: "1" - 提前还清
     */
    public static final String DEBTSTS_ADVANCE_1 = "1";

    /**
     * 摊销类型："1" - 平均摊销
     */
    public static final String AMOTTYP_AVERAGE_1 = "1";
    /**
     * 摊销类型："2" - 按比例摊销
     */
    public static final String AMOTTYP_PROPORTION_2 = "2";

    /**
     * 摊销标志: "0" - 未摊完
     */
    public static final String AMOTFLG_NOT_0 = "0";
    /**
     * 摊销标志: "1" - 已摊完
     */
    public static final String AMOTFLG_ALREADY_1 = "1";

    /**
     * 摊销金额类型: "1" - 贷款手续费
     */
    public static final String AMTTYPE_SERVICE_HARGE_1 = "1";
    /**
     * 摊销金额类型: "2" - 贷款利息
     */
    public static final String AMTTYPE_INTEREST_2 = "2";

    /**
     * 摊销周期："1D" - 每天
     */
    public static final String AMOTCYC_DAY_1D = "1D";
    /**
     * 摊销周期："1M20" - 每月20号
     */
    public static final String AMOTCYC_MONTH_1M20 = "1M20";
    /**
     * 摊销周期："1Y1120" - 每年11月20日
     */
    public static final String AMOTCYC_YEAR_1Y1120 = "1Y1120";

    /**
     * 文件传输标志: "2" - 下传
     */
    public static final String FILEFLAG_DOWN_2 = "2";

    /**
     * 状态："A" - 申请
     */
    public static final String FGSTS_APPLY_A = "A";
    /**
     * 状态："B" - 拒绝
     */
    public static final String FGSTS_REFUSE_B = "B";
    /**
     * 状态："C" - 执行
     */
    public static final String FGSTS_EXECUTE_C = "C";
    /**
     * 状态："0" - 确认
     */
    public static final String FGSTS_AFFIRM_0 = "0";

    /**
     * 迁移方式："1" - 撤并迁移
     */
    public static final String MOVMOD_REMOVAL_1 = "1";
    /**
     * 迁移方式："2" - 负债迁移
     */
    public static final String MOVMOD_DEBTS_2 = "2";
    /**
     * 迁移方式："3" - 产品迁移
     */
    public static final String MOVMOD_PRODUCT_3 = "3";
    /**
     * 迁移方式："4" - 账号迁移
     */
    public static final String MOVMOD_ACCNO_4 = "4";
    /**
     * 迁移方式："5" - 账号列表迁移6
     */
    public static final String MOVMOD_LIST_5 = "5";
    /**
     * 迁移方式："6" - 再次划归迁移
     */
    public static final String MOVMOD_AGAIN_6 = "6";

    /**
     * 机构级别："0" - 省联社
     */
    public static final String BRCLVL_PROVINCE_0 = "0";
    /**
     * 机构级别："3" - 法人行
     */
    public static final String BRCLVL_LEGAL_3 = "3";
    /**
     * 机构级别："4" - 分行（事业部）
     */
    public static final String BRCLVL_BRANCH_4 = "4";
    /**
     * 机构级别："5" - 支行
     */
    public static final String BRCLVL_SUBBRANCH_5 = "5";
    /**
     * 机构级别："6" - 分理处
     */
    public static final String BRCLVL_OFFICE_6 = "6";

    /**
     * 机构类型："0" - 管理机构
     */
    public static final String BRCATTR_MANAGEMENt_0 = "0";
    /**
     * 机构类型："1" - 营业机构
     */
    public static final String BRCATTR_BUSINESS_1 = "1";
    /**
     * 机构类型："2" - 内部账务机构
     */
    public static final String BRCATTR_INTERNAL_2 = "2";
    /**
     * 机构类型："3" - 业务办理机构
     */
    public static final String BRCATTR_HANDLING_3 = "3";

    /**
     * 机构属性："100" - 省联社财务部
     */
    public static final String BRCNAT_100 = "100";
    /**
     * 机构属性："101" - 法人行财务部
     */
    public static final String BRCNAT_101 = "101";
    /**
     * 机构属性："103" - 支行财务部
     */
    public static final String BRCNAT_103 = "103";
    /**
     * 机构属性："200" - 省联社清算中心
     */
    public static final String BRCNAT_200 = "200";
    /**
     * 机构属性："201" - 法人行清算中心
     */
    public static final String BRCNAT_201 = "201";
    /**
     * 机构属性："300" - 省联社营业网点
     */
    public static final String BRCNAT_300 = "300";

    /**
     * 机构属性："301" - 法人行营业部
     */
    public static final String BRCNAT_301 = "301";
    /**
     * 机构属性："302" - 分行营业网点
     */
    public static final String BRCNAT_302 = "302";
    /**
     * 机构属性："303" - 支行营业网点
     */
    public static final String BRCNAT_303 = "303";
    /**
     * 机构属性："304" - 分理处营业网点
     */
    public static final String BRCNAT_304 = "304";
    /**
     * 机构属性："401" - 法人行事业部
     */
    public static final String BRCNAT_401 = "401";
    /**
     * 机构属性："402" - 分行事业部
     */
    public static final String BRCNAT_402 = "402";
    /**
     * 机构属性："403" - 支行事业部
     */
    public static final String BRCNAT_403 = "403";
    /**
     * 机构属性："500" - 省联社管理部门
     */
    public static final String BRCNAT_500 = "500";
    /**
     * 机构属性："501" - 法人行管理部门
     */
    public static final String BRCNAT_501 = "501";
    /**
     * 机构属性："502" - 分行管理部门
     */
    public static final String BRCNAT_502 = "502";
    /**
     * 机构属性："503" - 支行管理部门
     */
    public static final String BRCNAT_503 = "503";
    /**
     * 机构属性："1" - 财务部
     */
    public static final String BRCNAT_ACC_1 = "1";
    /**
     * 机构属性："2" - 清算中心
     */
    public static final String BRCNAT_LIQUIDATION_2 = "2";
    /**
     * 机构属性："3" - 营业网点
     */
    public static final String BRCNAT_BUSINESS_3 = "3";
    /**
     * 机构属性："4" - 事业部
     */
    public static final String BRCNAT_ENTERPRISE_4 = "4";
    /**
     * 机构属性："5" - 管理部门
     */
    public static final String BRCNAT_MANAGEMENT_5 = "5";

    /**
     * 当前营业状态: "0" - 未签到
     */
    public static final String STSBUSI_NOSIGN_0 = "0";
    /**
     * 当前营业状态: "1" - 签到准备
     */
    public static final String STSBUSI_NOSIGNIN_1 = "1";
    /**
     * 当前营业状态: "2" - 签到
     */
    public static final String STSBUSI_SIGN_2 = "2";
    /**
     * 当前营业状态: "3" - 轧账
     */
    public static final String STSBUSI_ROLLINGACC_3 = "3";
    /**
     * 当前营业状态: "4" - 签退
     */
    public static final String STSBUSI_SIGNOUT_4 = "4";
    /**
     * 当前营业状态: "5" - 重新签到
     */
    public static final String STSBUSI_SIGNAGAIN_5 = "5";

    /**
     * 当前机构状态: "0" - 未上线
     */
    public static final String STSBRC_NOTONLINE_0 = "0";
    /**
     * 当前机构状态: "1" - 已上线
     */
    public static final String STSBRC_ONLINE_1 = "1";
    /**
     * 当前机构状态: "2" - 已撤并
     */
    public static final String STSBRC_REMOVAL_2 = "2";
    /**
     * 当前机构状态: "3" - 已删除
     */
    public static final String STSBRC_DELETED_3 = "3";

    /**
     * 账别："0" - 普通机构
     */
    public static final String ACCTYPE_ORDINARY_0 = "0";
    /**
     * 账别："1" - 村镇银行
     */
    public static final String ACCTYPE_VILLAGE_1 = "1";
    /**
     * 账别："2" - 自贸区机构
     */
    public static final String ACCTYPE_FREE_2 = "2";
    /**
     * 账别："3" - 离岸机构
     */
    public static final String ACCTYPE_OFFSHORE_3 = "3";

    /**
     * 余额汇总方式："1" - 轧差汇总
     */
    public static final String FLGLSTB_OFFSET_1 = "1";
    /**
     * 余额汇总方式："2" - 借贷并列汇总
     */
    public static final String FLGLSTB_DCSUM_2 = "2";
    /**
     * 余额汇总方式："D" - 借
     */
    public static final String FLGLSTB_DEBIT = "D";
    /**
     * 余额汇总方式："C" - 贷
     */
    public static final String FLGLSTB_CREDIT = "C";
    /**
     * 余额汇总方式："A" - 借贷并列
     */
    public static final String FLGLSTB_DC_PARALLELING_A = "A";
    /**
     * 余额汇总方式："B" - 双向
     */
    public static final String FLGLSTB_BOTHWAY_B = "B";

    /**
     * 是否记分户账："0" - 不记分户账
     */
    public static final String ISSUBAC_NO = "0";
    /**
     * 是否记分户账："1" - 记分户账
     */
    public static final String ISSUBAC_YES = "1";

    /**
     * 渠道："004" - 柜面
     */
    public static final String REQCHNLID_COUNTER_004 = "004";

    /**
     * 是否允许上级机构操作："0" - 不允许
     */
    public static final String ISALWS_NO = "0";
    /**
     * 是否允许上级机构操作："1" - 允许上级机构操作
     */
    public static final String ISALWS_YES = "1";
    /**
     * 是否允许上级机构操作："2" - 允许跨机构操作
     */
    public static final String ISALWS_ALL = "2";

    /**
     * 控制标准："0" - 不控制
     */
    public static final String CONTROL_NO = "0";

    /**
     * 控制标准："1" - 控制
     */
    public static final String CONTROL_YES = "1";

    /**
     * 控制方式："1" - 只进不出
     */
    public static final String TYPCTL_IN_1 = "1";
    /**
     * 控制方式："2" - 只出不进
     */
    public static final String TYPCTL_OUT_2 = "2";
    /**
     * 控制方式："3" - 不进不出
     */
    public static final String TYPCTL_NO_3 = "3";
    /**
     * 控制方式："4" - 正常
     */
    public static final String TYPCTL_NORMAL_4 = "4";

    /**
     * 利率浮动方向："01" - 利率上浮
     */
    public static final String FLGFINT_UP_01 = "01";
    /**
     * 利率浮动方向："02" - 利率下浮
     */
    public static final String FLGFINT_DOWN_02 = "02";

    /**
     * 利率浮动方式: "01" - 不浮动
     */
    public static final String FLGFLT_NO_01 = "01";
    /**
     * 利率浮动方式: "02" - 点数浮动
     */
    public static final String FLGFLT_COUNT_02 = "02";
    /**
     * 利率浮动方式: "03" -比例浮动
     */
    public static final String FLGFLT_RATIO_03 = "03";

    /**
     * 利率浮动方式: "01" - 不浮动
     */
    public static final String WAYFINT_NO_01 = "01";
    /**
     * 利率浮动方式: "02" - 点数浮动
     */
    public static final String WAYFINT_COUNT_02 = "02";
    /**
     * 利率浮动方式: "03" -比例浮动
     */
    public static final String WAYFINT_RATIO_03 = "03";

    /**
     * 是否计息: "0" - 不计息
     */
    public static final String ISINT_NO_0 = "0";
    /**
     * 是否计息: "1" - 计息
     */
    public static final String ISINT_YES_1 = "1";
    /**
     * 是否计息: "2" - 试算不计息
     */
    public static final String ISINT_TRIAL_2 = "2";
    /**
     * 是否计息: "3" - 特殊计息
     */
    public static final String ISINT_SPECIAL_3 = "3";

    /**
     * 发生额控制方向: "D" - 借
     */
    public static final String AMTCLDR_DEBIT = "D";

    /**
     * 发生额控制方向: "C" - 贷
     */
    public static final String AMTCLDR_CREDIT = "C";
    /**
     * 发生额控制方向: "B" - 双向
     */
    public static final String AMTCLDR_BOTH = "B";
    /**
     * 余额控制方向: "D" - 借
     */
    public static final String BALCTRL_DEBIT = "D";
    /**
     * 余额控制方向: "C" - 贷
     */
    public static final String BALCTRL_CREDIT = "C";
    /**
     * 余额控制方向: "B" - 双向
     */
    public static final String BALCTRL_BOTH = "B";

    /**
     * 是否允许透支: "0" - 不允许
     */
    public static final String ISOVD_NO = "0";

    /**
     * 是否允许透支: "1" - 允许
     */
    public static final String ISOVD_YES = "1";

    /**
     * 是否标准户: "0" - 不是标准户
     */
    public static final String ISFLGST_NO = "0";

    /**
     * 是否标准户: "1" - 是标准户
     */
    public static final String ISFLGST_YES = "1";

    /**
     * 开销户标志："1" - 开户
     */
    public static final String FLGCHA_OPEN_1 = "1";
    /**
     * 开销户标志："2" - 销户
     */
    public static final String FLGCHA_CLOSE_2 = "2";
    /**
     * 开销户标志："3" - 重启
     */
    public static final String FLGCHA_RESET_3 = "3";

    /**
     * 操作类型："0" - 开户
     */
    public static final String KINDOPR_OPEN_0 = "0";
    /**
     * 操作类型："1" - 销户
     */
    public static final String KINDOPR_CLOSE_1 = "1";
    /**
     * 操作类型："2" - 重启
     */
    public static final String KINDOPR_RESET_2 = "2";
    /**
     * 操作类型："3" - 修改
     */
    public static final String KINDOPR_ALTER_3 = "3";
    /**
     * 操作类型："4" - 控制
     */
    public static final String KINDOPR_CONTROL_4 = "4";
    /**
     * 操作类型："5" - 解控
     */
    public static final String KINDOPR_RELIEVE_5 = "5";
    /**
     * 操作类型："6" - 应提利息维护
     */
    public static final String KINDOPR_INTEREST_6 = "6";

    /**
     * 是否开票："0" - 否
     */
    public static final String ISBIL_NO = "0";
    /**
     * 是否开票："1" - 是
     */
    public static final String ISBIL_YES = "1";

    /**
     * 申报类型: "0" - 已申报
     */
    public static final String KINDDCL_HAVE_0 = "0";
    /**
     * 申报类型: "1" - 不申报
     */
    public static final String KINDDCL_NO_1 = "1";
    /**
     * 申报类型: "2" - 正常申报
     */
    public static final String KINDDCL_NORMAL_2 = "2";

    /**
     * 日终相关标志: "0" - 与日终没有关系
     */
    public static final String ENDFLG_NO = "0";
    /**
     * 日终相关标志: "1" - 与日终有关系
     */
    public static final String ENDFLG_YES = "1";

    /**
     * 文件处理标志: "0" - 未处理
     */
    public static final String DEALFLG_UNTREATED_0 = "0";
    /**
     * 文件处理标志: "1" - 正在处理
     */
    public static final String DEALFLG_PROCESSING_1 = "1";
    /**
     * 文件处理标志: "2" - 处理成功
     */
    public static final String DEALFLG_SUCCESSFUL_2 = "2";
    /**
     * 文件处理标志: "3" - 处理错误
     */
    public static final String DEALFLG_ERROR_3 = "3";

    /**
     * 利率类型: "Y" - 年利率
     */
    public static final String RATTYPF_YEAR = "Y";
    /**
     * 利率类型: "M" - 月利率
     */
    public static final String RATTYPF_MONTH = "M";
    /**
     * 利率类型: "D" - 日利率
     */
    public static final String RATTYPF_DAY = "D";

    /**
     * 销户重启标志: "0" - 销户
     */
    public static final String FLGOPR_CLOSE_0 = "0";
    /**
     * 销户重启标志: "1" - 重启
     */
    public static final String FLGOPR_RESET_1 = "1";

    /**
     * 划转标志: "0" - 已划转
     */
    public static final String FLGCHA_TRANSFER_0 = "0";
    /**
     * 划转标志: "1" - 已划回
     */
    public static final String FLGCHA_BACK_1 = "1";
    /**
     * 划转标志: "2" - 划转失败
     */
    public static final String FLGCHA_TRANSFER_FAIL_2 = "2";
    /**
     * 划转标志: "3" - 划回失败
     */
    public static final String FLGCHA_BACK_FAIL_3 = "3";

    /**
     * 记录状态: "0" - 无效
     */
    public static final String STSRCD_INVALID_0 = "0";
    /**
     * 记录状态: "1" - 有效
     */
    public static final String STSRCD_VALID_1 = "1";

    /**
     * 机构关系种类: "00" - 清算
     */
    public static final String LNKTYPE_LIQUIDATION_00 = "00";
    /**
     * 机构关系种类: "01" - 账务
     */
    public static final String LNKTYPE_FINANCE_01 = "01";
    /**
     * 机构关系种类: "02" - 总账
     */
    public static final String LNKTYPE_LEDGER_02 = "02";
    /**
     * 机构关系种类: "03" - 现金
     */
    public static final String LNKTYPE_CASH_03 = "03";
    /**
     * 机构关系种类: "04" - 凭证
     */
    public static final String LNKTYPE_VOUCHER_04 = "04";
    /**
     * 机构关系种类: "05" - 卡库
     */
    public static final String LNKTYPE_CARD_WAREHOUSE_05 = "05";
    /**
     * 机构关系种类: "06" - 管理
     */
    public static final String LNKTYPE_MANAGEMENT_06 = "06";
    /**
     * 机构关系种类: "07" - 拆借
     */
    public static final String LNKTYPE_LENDING_07 = "07";
    /**
     * 机构关系种类: "08" - 平盘
     */
    public static final String LNKTYPE_FLAT_08 = "08";
    /**
     * 机构关系种类: "09" - 单证
     */
    public static final String LNKTYPE_DOCUMENT_09 = "09";
    /**
     * 机构关系种类: "1" - 作业
     */
    public static final String LNKTYPE_TASK_10 = "10";
    /**
     * 机构关系种类: "2" - 国债
     */
    public static final String LNKTYPE_DEBT_11 = "11";
    /**
     * 机构关系种类: "3" - 制卡
     */
    public static final String LNKTYPE_MAKECARD_12 = "12";
    /**
     * 机构关系种类: "4" - 签退
     */
    public static final String LNKTYPE_SIGNOUT_13 = "13";

    /**
     * 批量返回信息: "2" - 成功
     */
    public static final String RTNCD_SUCCESS_2 = "2";
    /**
     * 批量返回信息: "3" - 警告
     */
    public static final String RTNCD_WARN_3 = "3";
    /**
     * 批量返回信息: "5" - 初始化
     */
    public static final String RTNCD_INITIALIZE_5 = "5";
    /**
     * 批量返回信息: "8" - 失败
     */
    public static final String RTNCD_FAIL_8 = "8";
    /**
     * 分类提交描述: "0" - 日间批量
     */
    public static final String SBMSQL_DAYTIME_0 = "0";
    /**
     * 分类提交描述: "1" - 日终批量
     */
    public static final String SBMSQL_DAYEND_1 = "1";
    /**
     * 科目类别: "0" - 资产类科目
     */
    public static final String TYPACC_ASSET_0 = "0";
    /**
     * 科目类别: "1" - 负债类科目
     */
    public static final String TYPACC_LIABILITIES_1 = "1";
    /**
     * 科目类别: "2" - 资产负债共同类科目
     */
    public static final String TYPACC_ASSET_LIABILITIES_2 = "2";
    /**
     * 科目类别: "3" - 所有者权益类科目
     */
    public static final String TYPACC_EQUITY_3 = "3";
    /**
     * 科目类别: "4" - 或有资产负债类
     */
    public static final String TYPACC_CONTINGENT_ASSETS_4 = "4";
    /**
     * 科目类别: "5" - 表外类科目
     */
    public static final String TYPACC_OFF_BALANCE_5 = "5";
    /**
     * 科目类别: "6" - 损益类支出性科目
     */
    public static final String TYPACC_EXPENSES_6 = "6";
    /**
     * 科目类别: "7" - 损益类收入性科目
     */
    public static final String TYPACC_INCOME_7 = "7";

    /**
     * 文件类型: "1" - 会计流水文件
     */
    public static final String FILTYPE_ACCOUNTING_1 = "1";
    /**
     * 文件类型: "2" - 并账文件
     */
    public static final String FILTYPE_CONSOLIDATION_2 = "2";
    /**
     * 文件类型: "3" - 交易流水
     */
    public static final String FILTYPE_TRANSACTION_3 = "3";

    /**
     * 科目是否需要转换标志位: "1" - 需要
     */
    public static final String CONVFLG_YES = "1";
    /**
     * 科目是否需要转换标志位: "2" - 不需要
     */
    public static final String CONVFLG_NO = "2";

    /**
     * 余额修改方式: "0" - 实时修改
     */
    public static final String TYPUBAL_REALTIME_0 = "0";
    /**
     * 余额修改方式: "1" - 准实时修改
     */
    public static final String TYPUBAL_NO_REALTIME_1 = "1";
    /**
     * 余额修改方式: "2" - 日终修改
     */
    public static final String TYPUBAL_DAY_END_2 = "2";

    /**
     * 是否结息: "0" - 利息查询
     */
    public static final String ISINTSG_QUERY_0 = "0";
    /**
     * 是否结息: "1" - 结息
     */
    public static final String ISINTSG_YES_1 = "1";
    /**
     * 是否结息: "2" - 应提利息计算
     */
    public static final String ISINTSG_COMPUTE_2 = "2";

    /**
     * 计税方法: "1" - 含税处理
     */
    public static final String TAXMOD_INCLUSIVE_1 = "1";
    /**
     * 计税方法: "2" - 不含税处理
     */
    public static final String TAXMOD_EXCLUSIVE_2 = "2";

    /**
     * 机构类别: "1" - 交易机构
     */
    public static final String BRCTYPE_TRANSACTION_1 = "1";
    /**
     * 机构类别: "2" - 归属机构
     */
    public static final String BRCTYPE_BELONG_2 = "2";
    /**
     * 机构类别: "3" - 对方机构
     */
    public static final String BRCTYPE_OPPOSITE_3 = "3";
    /**
     * 机构类别: "4" - 清算中心
     */
    public static final String BRCTYPE_LIQUIDATION_4 = "4";
    /**
     * 机构类别: "5" - 系统内平盘上级机构
     */
    public static final String BRCTYPE_FLAT_5 = "5";

    /**
     * 特殊处理标志: "0" -未处理
     */
    public static final String FLGDEALS_UNTREATED_0 = "0";
    /**
     * 特殊处理标志: "1" -处理中
     */
    public static final String FLGDEALS_PROCESSING_1 = "1";
    /**
     * 特殊处理标志: "2" - 处理完成
     */
    public static final String FLGDEALS_SUCCESSFUL_2 = "2";

    /**
     * 清算处理标志: "0" - 未拆分
     */
    public static final String FLGDEAL_NOT_SPLIT_0 = "0";
    /**
     * 清算处理标志: "1" - 拆分中
     */
    public static final String FLGDEAL_IN_SPLIT_1 = "1";
    /**
     * 清算处理标志: "2" - 拆分完成
     */
    public static final String FLGDEAL_SUCCESS_SPLIT_2 = "2";
    /**
     * 清算处理标志: "3" - 拆分失败
     */
    public static final String FLGDEAL_FAIL_SPLIT_3 = "3";
    /**
     * 清算处理标志: "4" - 清算中
     */
    public static final String FLGDEAL_IN_LIQUIDATION_4 = "4";
    /**
     * 清算处理标志: "5" - 清算完成
     */
    public static final String FLGDEAL_SUCCESS_LIQUIDATION_5 = "5";
    /**
     * 清算处理标志: "6" - 清算失败
     */
    public static final String FLGDEAL_FAIL_LIQUIDATION_6 = "6";
    /**
     * 清算处理标志: "7" - 核算完成
     */
    public static final String FLGDEAL_SUCCESS_ACCOUNTING_7 = "7";
    /**
     * 处理标志: "0" - 未处理
     */
    public static final String FLGDEAL_UNTREATED_0 = "0";
    /**
     * 处理标志: "1" - 处理中
     */
    public static final String FLGDEAL_PROCESSING_1 = "1";
    /**
     * 处理标志: "2" - 已处理（成功）
     */
    public static final String FLGDEAL_PROCESSED_SUCCESS_2 = "2";
    /**
     * 处理标志: "3" - 已处理（出错）
     */
    public static final String FLGDEAL_PROCESSED_ERROR_3 = "3";
    /**
     * 处理标志: "0" - 正常
     */
    public static final String FLGDEAL_NORMAL_0 = "0";
    /**
     * 处理标志: "1" - 特殊
     */
    public static final String FLGDEAL_SPECIAL_1 = "1";

    /**
     * 处理标志: "1" - 计提
     */
    public static final String FLGDEAL_ACCRUED_1 = "1";
    /**
     * 处理标志: "2" - 补提或反提
     */
    public static final String FLGDEAL_SUPPLEMENT_OPPOSITE_2 = "2";

    /**
     * 操作标志: "0" - 查询
     */
    public static final String FLGOPR_query_0 = "0";
    /**
     * 操作标志: "1" - 新增
     */
    public static final String FLGOPR_ADD_1 = "1";
    /**
     * 操作标志: "2" - 修改
     */
    public static final String FLGOPR_UPDATE_2 = "2";
    /**
     * 操作标志: "3" - 删除
     */
    public static final String FLGOPR_DELETE_3 = "3";
    /**
     * 操作标志: "A" - 增加
     */
    public static final String FLGOPR_ADD_A = "A";
    /**
     * 操作标志: "D" - 删除
     */
    public static final String FLGOPR_DELETE_D = "D";
    /**
     * 操作标志: "U" - 更新
     */
    public static final String FLGOPR_UPDATE_U = "U";
    /**
     * 操作标志: "Q" - 查询
     */
    public static final String FLGOPR_QUERY_Q = "Q";
    /**
     * 操作标志: "2" - 提前还款
     */
    public static final String FLGOPR_ADVANCE_2 = "2";
    /**
     * 操作标志: "2" - 删除
     */
    public static final String FLGOPR_DELETE_2 = "2";
    /**
     * 操作标志: "3" - 修改
     */
    public static final String FLGOPR_UPDATE_3 = "3";
    /**
     * 操作标志: "4" - 查询
     */
    public static final String FLGOPR_QUERY_4 = "4";
    /**
     * 操作标志: "1" - 交易流水查询
     */
    public static final String FLGOPR_TRANSACTION_1 = "1";
    /**
     * 操作标志: "2" - 会计流水
     */
    public static final String FLGOPR_ACCOUNTING_2 = "2";

    /**
     * BankID：100 - 通用
     */
    public static final String BANKID = "100";

    /**
     * YEAE：YYYY - 通用
     */
    public static final String YEAR_ALL = "YYYY";

    /**
     * 场景代码：WBJSH001 - 外币结售汇场景代码
     */
    public static final String SCESPCL_WBJSH001 = "WBJSH001";

    /**
     * 场景代码：BBJSHUSD - 美元结售汇场景代码
     */
    public static final String SCESPCL_BBJSHUSD = "BBJSHUSD";

    /**
     * 场景代码：BBJSHEUR - 欧元结售汇场景代码
     */
    public static final String SCESPCL_BBJSHEUR = "BBJSHEUR";

    /**
     * 场景代码：BBJSHGBP - 英镑结售汇场景代码
     */
    public static final String SCESPCL_BBJSHGBP = "BBJSHGBP";

    /**
     * 场景代码：BBJSHHKD - 港币结售汇场景代码
     */
    public static final String SCESPCL_BBJSHHKD = "BBJSHHKD";

    /**
     * 场景代码：BBJSHCHF - 瑞士法郎结售汇场景代码
     */
    public static final String SCESPCL_BBJSHCHF = "BBJSHCHF";

    /**
     * 场景代码：BBJSHJPY - 日元结售汇场景代码
     */
    public static final String SCESPCL_BBJSHJPY = "BBJSHJPY";

    /**
     * 场景代码：BBJSHAUD - 澳元结售汇场景代码
     */
    public static final String SCESPCL_BBJSHAUD = "BBJSHAUD";

    /**
     * 场景代码：BBJSHSGD - 新元结售汇场景代码
     */
    public static final String SCESPCL_BBJSHSGD = "BBJSHSGD";

    /**
     * 场景代码：BBJSHCAD - 加元结售汇场景代码
     */
    public static final String SCESPCL_BBJSHCAD = "BBJSHCAD";

    /**
     * 场景代码：BBJSHTHB - 泰铢结售汇场景代码
     */
    public static final String SCESPCL_BBJSHTHB = "BBJSHTHB";

    /**
     * 场景代码：BBJSHKRW - 韩元结售汇场景代码
     */
    public static final String SCESPCL_BBJSHKRW = "BBJSHKRW";

    /**
     * 科目清单：WBJSH001 - 外币结售汇科目清单
     */
    public static final String SUBLCOD_WBJSH001 = "WBJSH001";

    /**
     * 科目清单：PPSUBBAL - 配平科目清单
     */
    public static final String SUBLCOD_PPSUBBAL = "PPSUBBAL";

    /**
     * 核算种类代码：15001 - 挂销账核算种类代码
     */
    public static final String PRFCODE_15001 = "15001";

    /**
     * 对账数据源标志：0 - 正常
     */
    public static final String FLGDATA_NOR_0 = "0";

    /**
     * 对账数据源标志：1 - 重发
     */
    public static final String FLGDATA_REP_1 = "1";

    /**
     * 数据提供标志：0 - 对账文件提供
     */
    public static final String FLGSUPP_FILE_0 = "0";

    /**
     * 数据提供标志：1 - 核算提供
     */
    public static final String FLGSUPP_ACC_1 = "1";

    /**
     * 状态标志：0 - 登记
     */
    public static final String FLGSTS_REG_0 = "0";

    /**
     * 状态标志：1 - 已核对
     */
    public static final String FLGSTS_CHK_1 = "1";

    /**
     * 状态标志：2 - 待调账
     */
    public static final String FLGSTS_NAC_2 = "2";

    /**
     * 状态标志：3 - 已完成
     */
    public static final String FLGSTS_END_3 = "3";

    /**
     * 差异标志：0 - 登记
     */
    public static final String FLGDIFF_REG_0 = "0";

    /**
     * 差异标志：1 - 核算缺少
     */
    public static final String FLGDIFF_ACCL_1 = "1";

    /**
     * 差异标志：2 - 外围缺少
     */
    public static final String FLGDIFF_BUSL_2 = "2";

    /**
     * 差异标志：3 - 差异记录
     */
    public static final String FLGDIFF_DIFF_3 = "3";

    /**
     * 处理标志：0 - 登记
     */
    public static final String FLGDEAL_REG_0 = "0";

    /**
     * 处理标志：1 - 核算补录
     */
    public static final String FLGDEAL_ACR_1 = "1";

    /**
     * 处理标志：2 - 核算补录且补账
     */
    public static final String FLGDEAL_ACRA_2 = "2";

    /**
     * 处理标志：3 - 核算修改记录
     */
    public static final String FLGDEAL_ACM_3 = "3";

    /**
     * 处理标志：4 - 核算修改且调账
     */
    public static final String FLGDEAL_ACMA_4 = "4";

    /**
     * 处理标志：5 - 核算记录作废
     */
    public static final String FLGDEAL_IVAL_5 = "5";

    /**
     * 处理标志：6 - 核算记录作废且冲账
     */
    public static final String FLGDEAL_IVALR_6 = "6";

    /**
     * 处理标志：7 - 不处理
     */
    public static final String FLGDEAL_NOPR_7 = "7";

    /**
     * 批次：97 - 外围整个批次记录
     */
    public static final Integer BATID_CTL_97 = 97;

    /**
     * 批次：98 - 核算抽取汇总信息
     */
    public static final Integer BATID_ACC_98 = 98;

    /**
     * 批次：99 - 外围整个批次记录
     */
    public static final Integer BATID_BUS_99 = 99;

    /**
     * 文件处理标志：0 - 登记
     */
    public static final String FLGFILE_REG_0 = "0";

    /**
     * 文件处理标志：1 - 已抽取完明细文件
     */
    public static final String FLGFILE_END_1 = "1";

    /**
     * 汇总标志：0 - 存在差异
     */
    public static final String FLGSUM_DIFF_0 = "0";

    /**
     * 汇总标志：1 - 汇总相等
     */
    public static final String FLGSUM_EQU_1 = "1";

    /**
     * 方向标志：0 - 外围汇总
     */
    public static final String FLGWAY_BUS_0 = "0";

    /**
     * 方向标志：1 - 核算汇总
     */
    public static final String FLGWAY_ACC_1 = "1";

    /**
     * 方向标志：2 - 汇总控制
     */
    public static final String FLGWAY_CTL_2 = "2";

    /**
     * 核算规则代码：99 - 核算规则代码默认值
     */
    public static final String PRFRNO_99 = "99";

    /**
     * 对账交易码：999999 - 对账核算交易码
     */
    public static final String TRANCODE_DZ = "999999";

    /**
     * 对账交易标识：DZ999999 - 对账核算交易码
     */
    public static final String TRANCODE_DZ1 = "DZ999999";

    /**
     * 对账文件分隔符：DZ_SEPCHAR - "|"
     */
    public static final String DZ_SEPCHAR = "|";

    /**
     * 对账文件结束符：DZ_ENDCHAR - "end"
     */
    public static final String DZ_ENDCHAR = "end";

    /**
     * 对账汇总文件标识：DZ_SUMID - "dzsum"
     */
    public static final String DZ_SUMID = "dzsum";

    /**
     * 对账明细文件标识：DZ_LSTID - "dzlst"
     */
    public static final String DZ_LSTID = "dzlst";
    /**
     * 业务类型：01 - 外币损益重估
     */
    public static final String AFRTYP_01 = "01";
    /**
     * 业务类型：03 - 日终平盘处理
     */
    public static final String AFRTYP_03 = "03";
    /**
     * 业务类型：11 - 税金上划
     */
    public static final String AFRTYP_11 = "11";
    /**
     * 业务类型：21 - 损益上划
     */
    public static final String AFRTYP_21 = "21";
    /**
     * 业务类型：22 - 外币损益上划
     */
    public static final String AFRTYP_22 = "22";
    /**
     * 业务类型：23 - 投资买卖价差销项税
     */
    public static final String AFRTYP_23 = "23";
    /**
     * 业务类型：24 - 增值税上划
     */
    public static final String AFRTYP_24 = "24";
    /**
     * 业务类型：31 - 人民币年终结转至本年利润
     */
    public static final String AFRTYP_31 = "31";
    /**
     * 业务类型：32 - 外币年终结转至本年利润
     */
    public static final String AFRTYP_32 = "32";
    /**
     * 业务类型：33 - 往来账本年转历年
     */
    public static final String AFRTYP_33 = "33";
    /**
     * 业务类型：34 - 外币年终结转未分配利润清零
     */
    public static final String AFRTYP_34 = "34";
    /**
     * 业务类型：35 - 外币年终结转至未分配利润
     */
    public static final String AFRTYP_35 = "35";
    /**
     * 业务类型：36 - 外币年终结转平盘处理
     */
    public static final String AFRTYP_36 = "36";
    /**
     * 业务类型：91 - 往来账本年转历年转回
     */
    public static final String AFRTYP_91 = "91";
    /**
     * 是否可以手工记账：0 - 否
     */
    public static final String ISFLGSC_NO = "0";
    /**
     * 是否可以手工记账：1 - 是
     */
    public static final String ISFLGSC_YES = "1";
    /**
     * 利率类型: "0" - 年利率
     */
    public static final String RATTYPF_YEAR_0 = "0";
    /**
     * 利率类型: "1" - 月利率
     */
    public static final String RATTYPF_MONTH_1 = "1";
    /**
     * 利率类型: "2" - 日利率
     */
    public static final String RATTYPF_DAY_2 = "2";
    /**
     * 资金来源和去向: "1" - 现金
     */
    public static final String FNDTYP_CASH = "1";
    /**
     * 资金来源和去向: "2" - 待销账
     */
    public static final String FNDTYP_CHARGE = "2";
    /**
     * 资金来源和去向: "3" - 内部账一般户
     */
    public static final String FNDTYP_BASIC = "3";
    /**
     * 交易操作方式: "0" - 表内记账
     */
    public static final String TRANTYP_ACC = "0";
    /**
     * 交易操作方式: "1" - 红蓝字记账
     */
    public static final String TRANTYP_RB = "1";
    /**
     * 交易操作方式: "2" - 表外记账
     */
    public static final String TRANTYP_OUT = "2";
    /**
     * 表内记账方式: "0" - 一借一贷
     */
    public static final String TRANTYP_ACCOPT_0 = "0";
    /**
     * 表内记账方式: "1" - 一借多贷
     */
    public static final String TRANTYP_ACCOPT_1 = "1";
    /**
     * 表内记账方式: "2" - 多贷一借
     */
    public static final String TRANTYP_ACCOPT_2 = "2";
    /**
     * 表内记账方式: "3" - 多贷多借
     */
    public static final String TRANTYP_ACCOPT_3 = "3";
    /**
     * 红蓝字记账方式: "0" - 两蓝字
     */
    public static final String TRANTYP_RBOPT_0 = "0";
    /**
     * 红蓝字记账方式: "1" - 两红字
     */
    public static final String TRANTYP_RBOPT_1 = "1";
    /**
     * 红蓝字记账方式: "2" - 借红借蓝
     */
    public static final String TRANTYP_RBOPT_2 = "2";
    /**
     * 红蓝字记账方式: "3" - 贷红贷蓝
     */
    public static final String TRANTYP_RBOPT_3 = "3";
    /**
     * 查询方式: 1-正常查询
     */
    public static final String FLGQRY_1 = "1";
    /**
     * 查询方式: 2-新增时查询
     */
    public static final String FLGQRY_2 = "2";
    /**
     * 查询方式: 3-修改时查询
     */
    public static final String FLGQRY_3 = "3";
    /**
     * 查询方式: 4-停用启用时查询
     */
    public static final String FLGQRY_4 = "4";
    /**
     * 科目级别: "0" - 一级
     */
    public static final String SUBLVL_0 = "0";
    /**
     * 科目级别: "1" - 二级
     */
    public static final String SUBLVL_1 = "1";
    /**
     * 科目级别: "2" - 三级
     */
    public static final String SUBLVL_2 = "2";
    /**
     * 科目级别: "3" - 四级
     */
    public static final String SUBLVL_3 = "3";
    /**
     * 是否头寸户: "0" - 否
     */
    public static final String ISCASHAC_NO = "0";
    /**
     * 是否头寸户: "1" - 是
     */
    public static final String ISCASHAC_YES = "1";
    /**
     * 汇总入明细标志: "0" - 日间单笔
     */
    public static final String FLGLIST_0 = "0";
    /**
     * 汇总入明细标志: "1" - 日终单笔
     */
    public static final String FLGLIST_1 = "1";
    /**
     * 汇总入明细标志: "2" - 日终汇总
     */
    public static final String FLGLIST_2 = "2";

    /**
     * 状态："0" - 申请
     */
    public static final String FGSTS_APPLY_0 = "0";

    /**
     * 状态："1" - 拒绝
     */
    public static final String FGSTS_REFUSE_1 = "1";

    /**
     * 状态："2" - 执行
     */
    public static final String FGSTS_EXECUTE_2 = "2";

    /**
     * 状态："4" - 确认
     */
    public static final String FGSTS_AFFIRM_4 = "4";

    /**
     * 迁出处理标志: "1" - 成功
     */
    public static final String EMIG_FLGDEAL_SUCC_1 = "1";


    /**
     * 迁出处理标志: "9" - 失败
     */
    public static final String EMIG_FLGDEAL_FAIL_2 = "2";

    /**
     * 余额方向："D" - 借
     */
    public static final String BALD_DEBIT_D = "D";

    /**
     * 余额方向："C" - 贷
     */
    public static final String BALD_CREDIT_C = "C";

    /**
     * 发生额方向: "D" - 借
     */
    public static final String AMTD_DEBIT_D = "D";

    /**
     * 发生额控制方向: "C" - 贷
     */
    public static final String AMTD_CREDIT_C = "C";

    /**
     * 预警阈值: "0" - 级别0
     */
    public static final String WALVL_0 = "0";

    /**
     * 预警阈值: "1" - 级别1
     */
    public static final String WALVL_1 = "1";

    /**
     * 预警阈值: "2" - 级别2
     */
    public static final String WALVL_2 = "2";

    /**
     * 预警阈值: "3" - 级别3
     */
    public static final String WALVL_3 = "3";

    /**
     * 文件错误标志: "0" - 成功
     */
    public static final String FLGFILE_SUCC_0 = "0";

    /**
     * 文件错误标志: "1" - 失败
     */
    public static final String FLGFILE_FAIL_1 = "1";

    /**
     * 记录标志: "0" - 成功
     */
    public static final String FLGREC_SUCC_0 = "0";

    /**
     * 记录标志: "1" - 失败
     */
    public static final String FLGREC_FAIL_1 = "1";

    /**
     * SQL错误标志: "0" - 成功
     */
    public static final String FLGSQL_SUCC_0 = "0";

    /**
     * SQL错误标志: "1" - 失败
     */
    public static final String FLGSQL_FAIL_1 = "1";

    /**
     * 流水错误标志: "0" - 成功
     */
    public static final String FLGSEQ_SUCC_0 = "0";

    /**
     * 流水错误标志: "1" - 失败
     */
    public static final String FLGSEQ_FAIL_1 = "1";

    /**
     * 表内外标志: "BN" - 表内
     */
    public static final String FLGBNW_BN = "BN";

    /**
     * 表内外标志: "BW" - 表外
     */
    public static final String FLGBNW_BW = "BW";

    /**
     * 是否手工记账: "0" - 否
     */
    public static final String ISSHACC_NO_0 = "0";

    /**
     * 是否手工记账: "1" - 是
     */
    public static final String ISSHACC_YES_1 = "1";

    /**
     * 是否记传票: "0" - 否
     */
    public static final String ISWRTVC_NO_0 = "0";

    /**
     * 是否记传票: "1" - 是
     */
    public static final String ISWRTVC_YES_1 = "1";

    /**
     * 产品属性默认值: "99" - PRFRNO99 = "99"
     */
    public static final String PRFRNO_DEFAULT_99 = "99";
    /**
     * 操作方向：0 - 增加
     */
    public static final String FLGWAYOPT_ADD_0 = "0";

    /**
     * 操作方向：1 - 减少
     */
    public static final String FLGWAYOPT_SUB_1 = "1";
    /**
     * 多笔输出方式：0 - 无
     */
    public static final String MULTFLAG_NO_0 = "0";
    /**
     * 多笔输出方式：1 - 表单
     */
    public static final String MULTFLAG_FORM_1 = "1";
    /**
     * 多笔输出方式：2 - 文件
     */
    public static final String MULTFLAG_FILE_2 = "2";
    /**
     * 标准户处理标志：N - 不可以操作标准户
     */
    public static final String FLGCLO_ST_N = "N";
    /**
     * 标准户处理标志：Y - 可以操作标准户
     */
    public static final String FLGCLO_ST_Y = "Y";

    /**
     * 结息日：0320
     */
    public static final String MONDAY_0320 = "0320";

    /**
     * 结息日：0620
     */
    public static final String MONDAY_0620 = "0620";

    /**
     * 结息日：0920
     */
    public static final String MONDAY_0920 = "0920";

    /**
     * 结息日：1220
     */
    public static final String MONDAY_1220 = "1220";

    /**
     * 结息日：1231
     */
    public static final String MONDAY_1231 = "1231";

    /**
     * 结息日：-03-20
     */
    public static final String MONDAY_03_20 = "-03-20";

    /**
     * 结息日：-06-20
     */
    public static final String MONDAY_06_20 = "-06-20";

    /**
     * 结息日：-09-20
     */
    public static final String MONDAY_09_20 = "-09-20";

    /**
     * 结息日：-12-20
     */
    public static final String MONDAY_12_20 = "-12-20";

    /**
     * 日期格式 yyyy-MM-dd
     */
    public static final String PATTERN_YYYY_MM_DD = "yyyy-MM-dd";

    /**
     * 日期格式 yyyy-MM-dd
     */
    public static final String FORMAT_YYYY = "yyyy";

    /**
     * 初始日期 0000-00-00
     */
    public static final String DATE_ZERO = "0000-00-00";

    /**
     * 账号上级与本机构的关系："0" - 成立
     */
    public static final int BRCCHK_YES_0 = 0;

    /**
     * 账号上级与本机构的关系："1" - 不成立
     */
    public static final int BRCCHK_NO_1 = 1;

    /**
     * 标志 用于开户修改。判断则为 1
     */
    public static final String FLGCHA_JUDGE_1 = "1";

    /**
     * 标志 用于开户修改。为空时则为 0
     */
    public static final String FLGCHA_NULL_0 = "0";

    /**
     * 是否满足迁出状态: 1 - 满足
     */
    public static final String TRBKCF_SATISFY_1 = "1";

    /**
     * 是否满足迁出状态: 0 - 不满足
     */
    public static final String TRBKCF_DISSATISFY_0 = "0";

    /**
     * 是否满足迁出状态: 000 - 全部不满足
     */
    public static final String TRBKCF_AllSATISFY_111 = "111";

    /**
     * 是否满足迁出状态: 000 - 全部不满足
     */
    public static final String TRBKCF_AllDISSATISFY_000 = "000";

    /**
     * 现转标志 1-现金 2-转账
     */
    public static final String FLGCT_CASH_1 = "1";

    /**
     * 现转标志  2-转账
     */
    public static final String FLGCT_TRANSFER_2 = "2";

    /**
     * 是否年初： 1 - 是
     */
    public static final String ISYRBF_YES = "1";

    /**
     * 是否年末： 1 - 是
     */
    public static final String ISYREF_YES = "1";

    /**
     * 是否半年初： 1 - 是
     */
    public static final String ISHYBF_YES = "1";

    /**
     * 是否半年末： 1 - 是
     */
    public static final String ISHYEF_YES = "1";

    /**
     * 是否季初： 1 - 是
     */
    public static final String ISQTBF_YES = "1";

    /**
     * 是否季末： 1 - 是
     */
    public static final String ISQTEF_YES = "1";

    /**
     * 是否月初： 1 - 是
     */
    public static final String ISMNBF_YES = "1";

    /**
     * 是否月末： 1 - 是
     */
    public static final String ISMNEF_YES = "1";

    /**
     * 是否旬初： 1 - 是
     */
    public static final String ISTEBF_YES = "1";

    /**
     * 是否旬未： 1 - 是
     */
    public static final String ISTEEF_YES = "1";

    /**
     * 是否周末： 1 - 是
     */
    public static final String ISWKEF_YES = "1";

    /**
     * 是否周初： 1 - 是
     */
    public static final String ISWKBF_YES = "1";

    /**
     * 是否21日： 1 - 是
     */
    public static final String ISTWOF_YES = "1";

    /**
     * 是否5日： 1 - 是
     */
    public static final String ISFIVF_YES = "1";


    /**
     * 是否年初： 0 - 否
     */
    public static final String ISYRBF_NO = "0";

    /**
     * 是否年末： 0 - 否
     */
    public static final String ISYREF_NO = "0";

    /**
     * 是否半年初： 0 - 否
     */
    public static final String ISHYBF_NO = "0";

    /**
     * 是否半年末： 0 - 否
     */
    public static final String ISHYEF_NO = "0";

    /**
     * 是否季初： 0 - 否
     */
    public static final String ISQTBF_NO = "0";

    /**
     * 是否季末： 0 - 否
     */
    public static final String ISQTEF_NO = "0";

    /**
     * 是否月初： 0 - 否
     */
    public static final String ISMNBF_NO = "0";

    /**
     * 是否月末： 0 - 否
     */
    public static final String ISMNEF_NO = "0";

    /**
     * 是否旬初： 0 - 否
     */
    public static final String ISTEBF_NO = "0";

    /**
     * 是否旬未： 0 - 否
     */
    public static final String ISTEEF_NO = "0";

    /**
     * 是否周末： 0 - 否
     */
    public static final String ISWKEF_NO = "0";

    /**
     * 是否周初： 0 - 否
     */
    public static final String ISWKBF_NO = "0";

    /**
     * 是否21日： 0 - 否
     */
    public static final String ISTWOF_NO = "0";

    /**
     * 是否5日： 0 - 否
     */
    public static final String ISFIVF_NO = "0";


    /**
     * 周期标志："1" - 上旬
     */
    public static final String FLGCYC_TOPMONTH_1 = "1";

    /**
     * 周期标志："2" - 中旬
     */
    public static final String FLGCYC_MIDMONTH_2 = "2";

    /**
     * 周期标志："3" - 下旬
     */
    public static final String FLGCYC_DOWNMONTh_3 = "3";

    /**
     * 周期标志："4" - 月
     */
    public static final String FLGCYC_MONTH_4 = "4";

    /**
     * 周期标志："5" - 季
     */
    public static final String FLGCYC_QTR_5 = "5";

    /**
     * 周期标志："6" - 半年
     */
    public static final String FLGCYC_HALFYEAR_6 = "6";

    /**
     * 周期标志："7" - 年
     */
    public static final String FLGCYC_YEAR_7 = "7";

    /**
     * 所属系统： IA
     */
    public static final String SUBPLAT_IA = "IA";

    /**
     * 所属系统： DP
     */
    public static final String SUBPLAT_DP = "DP";

    /**
     * 不平标志：1 - 不平
     */
    public static final String ERROR_UNBALANCE_1 = "1";

    /**
     * 年初日期
     */
    public static final String DATE_01_01 = "01-01";

    /**
     * 年末日期
     */
    public static final String DATE_12_31 = "12-31";

    /**
     * 法人行 340000
     */
    public static final String SUBBNKN_340000 = "340000";

    /**
     * 拼接顺序号 000
     */
    public static final String SUBNO_000 = "000";

    /**
     * 拼接顺序号 000XXXX
     */
    public static final String SUBNO_000XXXX = "000XXXX";

    /**
     * 拼接顺序号 XXXX
     */
    public static final String SUBNO_XXXX = "XXXX";


    /**
     * 清算头寸透支拆借处理标志："1" - 借款
     */
    public static final String FLGDEAL_BORROW_1 = "1";

    /**
     * 清算头寸透支拆借处理标志："2" - 还款
     */
    public static final String FLGDEAL_REPAY_2 = "2";

    /**
     * 清算行：1 - 法人行补清算
     */
    public static final String FLGCLR_SUBBNKN_1 = "1";

    /**
     * 清算行：2 - 分行补清算
     */
    public static final String FLGCLR_SUBBNKN_2 = "2";

    /**
     * 法人行清算
     */
    public static final String CLRLEV_SUBBNKN_1 = "1";

    /**
     * 法人行内部清算
     */
    public static final String CLRLEV_SUBBNKN_2 = "2";

    /**
     * 特殊清算标志 0 - 零级清算
     */
    public static final String FLGCLRS_ZERO_0 = "0";

    /**
     * 特殊清算标志 1 - 逐级清算
     */
    public static final String FLGCLRS_STEP_1 = "1";

    /**
     * 扣划标志: 0 - 不执行
     */
    public static final String FLGPUN_NO_0 = "0";

    /**
     * 扣划标志: 1 - 执行
     */
    public static final String FLGPUN_YES_1 = "1";

    /**
     * 判断是否从 批量日期表里面取日期: 0 - 否
     */
    public static final String FLAG_NO_0 = "0";

    /**
     * 判断是否从 批量日期表里面取日期: 1 - 是
     */
    public static final String FLAG_YES_1 = "1";

    /**
     * 系统状态 N - 正常（日间）
     */
    public static final String SYSSTS_NORMAL_N = "N";

    /**
     * 系统状态 C - 日切（日切窗口）
     */
    public static final String SYSSTS_CUT_C = "C";


    /**
     * 系统状态 C - 日切（准备日切）
     */
    public static final String SYSSTS_CUT_D = "C";


    /**
     * 系统状态 C - 日终处理中
     */
    public static final String SYSSTS_CUT_E = "C";


    /**
     * 系统状态 C - 停止服务
     */
    public static final String SYSSTS_CUT_F = "C";



    /**
     * 报文流水处理状态 0 - 未处理
     */
    public static final String ACDEAL_FLG_0 = "0";

    /**
     * 报文流水处理状态 0 - 处理中
     */
    public static final String ACDEAL_FLG_1 = "1";

    /**
     * 报文流水处理状态 0 - 解析完成
     */
    public static final String ACDEAL_FLG_9 = "9";


    /**
     * 传票解析规则科目来源 1 - 产品
     */
    public static final String SUB_NO_SOURCE_1="1";

    /**
     * 传票解析规则科目来源 2 - 固定科目
     */
    public static final String SUB_NO_SOURCE_2="2";

    /**
     * 传票解析规则科目来源 3 - 内部账号
     */
    public static final String SUB_NO_SOURCE_3="3";


    /**
     * 传票解析规则机构来源 1 - 交易中获取
     */
    public static final String BRC_NO_SOURCE_1="1";

    /**
     * 传票解析规则机构来源 2 - 固定机构号
     */
    public static final String BRC_NO_SOURCE_2="2";

    /**
     * 传票解析规则机构来源 3 - 平盘机构
     */
    public static final String BRC_NO_SOURCE_3="3";

    // ============================================= //
    //TODO 补充枚举
    /**
     * 属性类别标志 1 - 产品属性
     */
    public static final String PRD_PROP_TYPE_1="1";

    /**
     * 属性类别标志 2 - 金额属性
     */
    public static final String PRD_PROP_TYPE_2="2";


    /**
     * 传票记账状态 1 - 未入账
     */
    public static final String ACC_ENT_FLAG_1="1";

    /**
     * 传票记账状态 2 - 已入账
     */
    public static final String ACC_ENT_FLAG_2="2";


    /**
     * 传票种类: "0" - 记账传票
     */

    public static final String FLGCLRS_TYPE_0 = "0";

    /**
     * 传票种类: "1" - 清算传票
     */
    public static final String FLGCLRS_TYPE_1 = "1";



    /**
     * 核算记账方式 0 - 正常
     */
    public static final String ACC_ENT_MOD_0="0";

    /**
     * 核算记账方式 1 - 红字
     */

    public static final String ACC_ENT_MOD_1 = "1";

    /**
     * 核算记账方式 2 - 蓝字
     */
    public static final String ACC_ENT_MOD_2 = "2";

    /**
     * 参数类型 0 - 系统参数
     */
    public static final String PARM_TYPE_0="0";

    /**
     * 参数类型 1 - 业务参数
     */

    public static final String PARM_TYPE_1 = "1";


    /**
     * 参数类型 实时清算补平科目 - 参数代码
     */
    public static final String PARM_COD_0="ACCCLEARSUB";


    /**
     * 参数类型 待销账业务代号 - 参数代码
     */
    public static final String PARM_COD_1="ACCHNGSUB";

    /**
     * 参数类型 暂收暂付业务代号 - 参数代码
     */
    public static final String PARM_COD_2="TMPRPAYSUB";


    /** 手工开户受理模式 0-不允许 */
    public final static String FLGHOPEN_MOD_0 = "0";
    /** 手工开户受理模式 1-自动开 */
    public final static String FLGHOPEN_MOD_1 = "1";
    /** 手工开户受理模式 2-自行开立 */
    public final static String FLGHOPEN_MOD_2 = "2";

    // ============================================= //


}
