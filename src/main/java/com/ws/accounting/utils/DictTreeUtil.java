package com.ws.accounting.utils;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ws.accounting.entity.Dict;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DictTreeUtil {

    public static List<Dict> getChildrenDict(String nodeKey, Map<String, Dict> nodes){

        List<Dict> customerList = new ArrayList<>();

        for (String key: nodes.keySet()
        ) {

            if(nodes.get(key).getParentId().equals(nodeKey)) {
                customerList.add(nodes.get(key));
            }


        }

        return customerList;
    }


    public static JSONArray getNodeJson(String nodeKey, Map<String,Dict> nodes){

        //当前层级node对象

        //Dictionary dictionary = nodes.get(nodeKey);

        //当前层级下的所有节点

        List<Dict> childList = getChildrenDict(nodeKey,nodes);

        JSONArray childTree = new JSONArray();

        for (Dict node:childList
        ) {
            JSONObject o = new JSONObject();
            o.put("name",node.getName());
            o.put("nodeKey",node.getId());
            o.put("id",node.getId());
            o.put("parentId",node.getParentId());
            o.put("topName",node.getTopName());
            o.put("rank",node.getRank());
            o.put("attributes",node.getAttributes());
            o.put("level",node.getLevel());
            o.put("fullName",node.getFullName());





            JSONArray childs = getNodeJson(node.getId(),nodes);
            if(!childs.isEmpty()){
                o.put("children",childs);
            }
            childTree.fluentAdd(o);
        }
        return childTree;
    }
}
