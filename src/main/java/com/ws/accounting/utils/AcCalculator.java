package com.ws.accounting.utils;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Stack;

/**
 * ======================================================
 *
 * @System:[核算系统]
 * @Desc: [字符串数学表达式解析求值工具类]
 * @author: [曹国鹏]
 * @date: 2021/2/8 2:37 下午
 * @copyright: [Pactera]
 * ========================================================
 */
public class AcCalculator {
    // 后缀式栈
    private Stack<String> postfixStack = new Stack<String>();
    // 运算符栈
    private Stack<Character> opStack = new Stack<Character>();
    // 运用运算符ASCII码-40做索引的运算符优先级
    private int[] operatPriority = new int[] { 0, 3, 2, 1, -1, 1, 0, 2 };


    /**
     * @Author [曹国鹏]
     * @Description 计算数学表达式结果
     * @Date  2021/2/8 2:37 下午
     * @Param expression 计算表达式
     * @return 计算结果
     * @module [核算系统]
     *
     **/
    public static BigDecimal conversion(String expression) {

        BigDecimal result =  new BigDecimal("0.0000");
        AcCalculator cal = new AcCalculator();
        try {
            expression = transform(expression);
            result = cal.calculate(expression);
        } catch (Exception e) {
            // e.printStackTrace();
            // 运算错误返回NaN
            return new BigDecimal("0.00");
        }
        // return new String().valueOf(result);
        return result;
    }


    /**
     * @Author [曹国鹏]
     * @Description 将表达式中负数的符号更改,例如-2+-1*(-3E-2)-(-1) 被转为 ~2+~1*(~3E~2)-(~1)
     * @Date  2021/2/8 2:37 下午
     * @Param expression 计算表达式
     * @return String
     * @module [核算系统]
     *
     **/
    private static String transform(String expression) {
        char[] arr = expression.toCharArray();
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == '-') {
                if (i == 0) {
                    arr[i] = '~';
                } else {
                    char c = arr[i - 1];
                    if (c == '+' || c == '-' || c == '*' || c == '/' || c == '(' || c == 'E' || c == 'e') {
                        arr[i] = '~';
                    }
                }
            }
        }
        if(arr[0]=='~'||arr[1]=='('){
            arr[0]='-';
            return "0"+new String(arr);
        }else{
            return new String(arr);
        }
    }


    /**
     * @Author [曹国鹏]
     * @Description 按照给定的表达式计算，要计算的表达式例如:5+12*(3+5)/7
     * @Date  2021/2/8 2:37 下午
     * @Param expression 计算表达式
     * @return 计算结果
     * @module [核算系统]
     *
     **/
    public BigDecimal calculate(String expression) {
        Stack<String> resultStack = new Stack<String>();
        prepare(expression);
        // 将后缀式栈反转
        Collections.reverse(postfixStack);
        // 参与计算的第一个值，第二个值和算术运算符
        String firstValue, secondValue, currentValue;
        while (!postfixStack.isEmpty()) {
            currentValue = postfixStack.pop();
            // 如果不是运算符则存入操作数栈中
            if (!isOperator(currentValue.charAt(0))) {
                currentValue = currentValue.replace("~", "-");
                resultStack.push(currentValue);
                // 如果是运算符则从操作数栈中取两个值和该数值一起参与运算
            } else {
                secondValue = resultStack.pop();
                firstValue = resultStack.pop();

                // 将负数标记符改为负号
                firstValue = firstValue.replace("~", "-");
                secondValue = secondValue.replace("~", "-");

                String tempResult = calculate(firstValue, secondValue, currentValue.charAt(0));
                resultStack.push(tempResult);
            }
        }

        return new BigDecimal(resultStack.pop());
    }


    /**
     * @Author [曹国鹏]
     * @Description 数据准备阶段将表达式转换成为后缀式栈
     * @Date  2021/2/8 2:37 下午
     * @Param expression 计算表达式
     * @return
     * @module [核算系统]
     *
     **/
    private void prepare(String expression) {
        //运算符放入栈底元素逗号，此符号优先级最低
        opStack.push(',');

        char[] arr = expression.toCharArray();

        // 当前字符的位置
        int currentIndex = 0;

        // 上次算术运算符到本次算术运算符的字符的长度便于或者之间的数值
        int count = 0;

        // 当前操作符和栈顶操作符
        char currentOp, peekOp;


        for (int i = 0; i < arr.length; i++) {
            currentOp = arr[i];
            // 如果当前字符是运算符
            if (isOperator(currentOp)) {
                if (count > 0) {
                    // 取两个运算符之间的数字
                    postfixStack.push(new String(arr, currentIndex, count));
                }
                peekOp = opStack.peek();

                // 遇到反括号则将运算符栈中的元素移除到后缀式栈中直到遇到左括号
                if (currentOp == ')') {
                    while (opStack.peek() != '(') {
                        postfixStack.push(String.valueOf(opStack.pop()));
                    }
                    opStack.pop();
                } else {
                    while (currentOp != '(' && peekOp != ',' && compare(currentOp, peekOp)) {
                        postfixStack.push(String.valueOf(opStack.pop()));
                        peekOp = opStack.peek();
                    }
                    opStack.push(currentOp);
                }
                count = 0;
                currentIndex = i + 1;
            } else {
                count++;
            }
        }
        // 最后一个字符不是括号或者其他运算符的则加入后缀式栈中
        if (count > 1 || (count == 1 && !isOperator(arr[currentIndex]))) {
            postfixStack.push(new String(arr, currentIndex, count));
        }
        // 将操作符栈中的剩余的元素添加到后缀式栈中
        while (opStack.peek() != ',') {
            postfixStack.push(String.valueOf(opStack.pop()));
        }
    }


    /**
     * @Author [曹国鹏]
     * @Description 判断是否为算术符号
     * @Date  2021/2/8 2:37 下午
     * @Param char（符号）
     * @return boolean
     * @module [核算系统]
     *
     **/
    private boolean isOperator(char c) {
        return c == '+' || c == '-' || c == '*' || c == '/' || c == '(' || c == ')';
    }


    /**
     * @Author [曹国鹏]
     * @Description 利用ASCII码-40做下标去计算算术符号优先级
     * @Date  2021/2/8 2:37 下午
     * @Param char（符号）如果是peek优先级高于cur，返回true，默认都是peek优先级要低
     * @return boolean
     * @module [核算系统]
     *
     **/
    public boolean compare(char cur, char peek) {
        boolean result = false;
        if (operatPriority[(peek) - 40] >= operatPriority[(cur) - 40]) {
            result = true;
        }
        return result;
    }

    /**
     * @Author [曹国鹏]
     * @Description 按照给定的算术运算符做计算
     * @Date  2021/2/8 2:37 下午
     * @param firstValue
     * @param secondValue
     * @param currentOp
     * @return 计算结果字符串
     * @module [核算系统]
     *
     *
     **/
    private String calculate(String firstValue, String secondValue, char currentOp) {
        String result = "";
        switch (currentOp) {
            case '+':
                result = String.valueOf(AcArithHelper.add(firstValue, secondValue));
                break;
            case '-':
                result = String.valueOf(AcArithHelper.sub(firstValue, secondValue));
                break;
            case '*':
                result = String.valueOf(AcArithHelper.mul(firstValue, secondValue));
                break;
            case '/':
                result = String.valueOf(AcArithHelper.div(firstValue, secondValue));
                break;
        }
        return result;
    }
}