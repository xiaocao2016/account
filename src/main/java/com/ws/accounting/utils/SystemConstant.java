package com.ws.accounting.utils;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * ======================================================
 *
 * @System:[核算系统]
 * @Desc: [canshu]
 * @author: [曹国鹏]
 * @date: 2021/2/25 4:22 下午
 * @copyright: [Pactera]
 * ========================================================
 */
public class SystemConstant {
    public static final String OFF = "0";
    public static final String ON = "1";
    public static final String POC_SWITCH = "0";
    public static final boolean UNIT_SUCCESS = false;
    public static final boolean UNIT_FAIL = true;
    public static final String BLANKS = "";
    public static final Integer ZERO = 0;
    public static final BigDecimal ZERO$;
    public static final BigDecimal ZERO1;
    public static final BigDecimal ZERO2;
    public static final BigDecimal ZERO3;
    public static final BigDecimal ZERO4;
    public static final BigDecimal ZERO5;
    public static final BigDecimal ZERO6;
    public static final BigDecimal ZERO7;
    public static final BigDecimal ZERO8;
    public static final BigDecimal ZERO14;
    public static final BigInteger ZEROBI;
    public static final BigInteger BIGONE;
    public static final String SEP = "|";
    public static final char INIT_CHAR = ' ';
    public static final String RSPCODE = "AAAAAAA";
    public static final Integer DIVIDE;
    public static final Integer AMT;
    public static final Integer RATE;
    public static final Integer EXCHANGE;
    public static final String YES = "1";
    public static final String NO = "0";
    public static final Integer ERROR;
    public static final Integer SUCCESS;
    public static final Integer EXIT;
    public static final Integer PAGEINDEX;
    public static final String SQL_NULL;
    public static final Integer BATMAXNUM;
    public static final String PUBCOM_PRE = "PUBCOM_TRANSEQ_";
    public static final String PUBCOM_JSON = "PUBCOM_JSON";
    public static final String REST_GET = "GET";
    public static final String REST_POST = "POST";
    public static final String ECAS_SERVICE_AC = "ecas-ac-service";
    public static final String ECAS_SERVICE_DP = "ecas-dp-service";
    public static final String ECAS_SERVICE_LN = "ecas-ln-service";
    public static final String ECAS_SERVICE_PB = "ecas-pb-service";
    public static final String FIRST_SERVICE = "S814001";
    public static final String TAIL_SERVICE = "S814002";
    public static final String PLATFORM_SEQUENCE = "PlatFormSeq";

    public SystemConstant() {
    }

    public static void main(String[] args) {
        System.out.println((new BigDecimal("5.2")).divideAndRemainder(new BigDecimal("2.6"))[0]);
        System.out.println((new BigDecimal("5.2")).divideAndRemainder(new BigDecimal("2.6"))[1]);
        System.out.println((new BigDecimal("5.3")).divideAndRemainder(new BigDecimal("2.6"))[0]);
        System.out.println((new BigDecimal("5.3")).divideAndRemainder(new BigDecimal("2.6"))[1]);
    }

    static {
        ZERO$ = BigDecimal.ZERO;
        ZERO1 = new BigDecimal("0.0");
        ZERO2 = new BigDecimal("0.00");
        ZERO3 = new BigDecimal("0.000");
        ZERO4 = new BigDecimal("0.0000");
        ZERO5 = new BigDecimal("0.00000");
        ZERO6 = new BigDecimal("0.000000");
        ZERO7 = new BigDecimal("0.0000000");
        ZERO8 = new BigDecimal("0.00000000");
        ZERO14 = new BigDecimal("0.00000000000000");
        ZEROBI = new BigInteger("0");
        BIGONE = new BigInteger("1");
        DIVIDE = 10;
        AMT = 2;
        RATE = 7;
        EXCHANGE = 7;
        ERROR = 1;
        SUCCESS = 0;
        EXIT = -1;
        PAGEINDEX = 1;
        SQL_NULL = null;
        BATMAXNUM = 1000;
    }
}