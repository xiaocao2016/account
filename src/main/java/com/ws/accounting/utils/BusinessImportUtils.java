package com.ws.accounting.utils;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.fastjson.JSON;
import com.ws.accounting.entity.BusinessCode;
import com.ws.accounting.entity.SubjectInfo;
import com.ws.accounting.repository.BusinessCodeRepository;
import com.ws.accounting.repository.SubjectInfoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class BusinessImportUtils extends AnalysisEventListener<BusinessCode> {
    private static final Logger LOGGER = LoggerFactory.getLogger(BusinessImportUtils.class);
    private static final int BATCH_COUNT = 5;
    List<BusinessCode> businessCodeList = new ArrayList<BusinessCode>();

    private BusinessCodeRepository repository;

    /**
     * 如果使用了spring,请使用这个构造方法。每次创建Listener的时候需要把spring管理的类传进来
     *
     * @param repository
     */
    public BusinessImportUtils(BusinessCodeRepository repository) {
        this.repository = repository;
    }



    @Override
    public void invoke(BusinessCode businessCode, AnalysisContext analysisContext) {
        LOGGER.info("解析到一条数据:{}", JSON.toJSONString(businessCode));
        businessCodeList.add(businessCode);
        if (businessCodeList.size() >= BATCH_COUNT) {
            saveData();
            // 存储完成清理 list
            businessCodeList.clear();
        }
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
        // 这里也要保存数据，确保最后遗留的数据也存储到数据库
        saveData();
        LOGGER.info("所有数据解析完成！");
    }




    /**
     * 加上存储数据库
     */
    private void saveData() {
        LOGGER.info("{}条数据，开始存储数据库！", businessCodeList.size());
        repository.saveAll(businessCodeList);
        LOGGER.info("存储数据库成功！");
    }
}
